


var AceNewSurveyDialog = function () {
    BaseAceSurveyWidget.call(this);
    this.listener = {
        onSaved: null
    }
    this.appendTo(document.body);
}


// Inherit all BaseAceSurveyWidget method 
AceNewSurveyDialog.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceNewSurveyDialog.prototype.constructor = AceNewSurveyDialog;

AceNewSurveyDialog.prototype.template = function () {
    var self = this;

    var dialog_holder = new AceSurveyPanel(),
        wrapper_content = new AceSurveyPanel(),
        scroller = new AceSurveyPanel(),
        row_1 = new AceSurveyPanel(),
        row_2 = new AceSurveyPanel(),
        row_3 = new AceSurveyPanel(),
        row_4 = new AceSurveyPanel(),
        row_5 = new AceSurveyPanel(),
        row_6 = new AceSurveyPanel(),
        row_7 = new AceSurveyPanel(),
        lb_header = new AceSurveyLabel(ACE_SURVEY_STRINGS.dialog_header_new_survey),
        lb_survey_title = new AceSurveyLabel(ACE_SURVEY_STRINGS.hint_survey_name),
        lb_survey_description = new AceSurveyLabel(ACE_SURVEY_STRINGS.hint_survey_description),
        lb_multiple_response = new AceSurveyLabel("Allow multiple responses"),
        lb_show_user = new AceSurveyLabel("Show user names in survey results"),
        lb_read_permission = new AceSurveyLabel("<b>Reader</b>"),
        lb_contribute_permission = new AceSurveyLabel("<b>Contribute</b>"),
        lb_admin_permission = new AceSurveyLabel("<b>Full Control</b>"),
        txt_survey_title = new AceSurveyEdittext(),
        txt_survey_desc = new AceSurveyTextArea(),
        cb_multi_response = new AceSurveyCheckbox(),
        cb_show_user = new AceSurveyCheckbox(),
        // Currently, Not addd user picker box into dialog
        // picker_user_read = new AceSurveyUserPicker(),
        // picker_user_contribute = new AceSurveyUserPicker(),
        // picker_user_admin = new AceSurveyUserPicker(),
        btn_save = new AceSurveyButton("Create"),
        btn_cancel = new AceSurveyButton('Cancel');

    dialog_holder.addClass("ace-dialog-holder");
    wrapper_content.addClass("ace-dialog-new-survey");
    scroller.addClass('pn-scroller')

    lb_header.addClass('lb-header');
    txt_survey_title.addClass('txt-sv-title');
    //  txt_survey_title.setAttribute('placeholder', ACE_SURVEY_STRINGS.hint_survey_name);
    txt_survey_desc.addClass('txt-sv-desc');
    //   txt_survey_desc.setAttribute('placeholder', ACE_SURVEY_STRINGS.hint_survey_description);

    btn_save.addClass("ace-btn-ok");
    btn_cancel.addClass("ace-btn-cancel");

    wrapper_content.appendChild(lb_header);
    row_1.appendChild(lb_survey_title);
    row_1.appendChild(txt_survey_title);

    row_2.appendChild(lb_survey_description);
    row_2.appendChild(txt_survey_desc);

    row_3.appendChild(lb_multiple_response);
    row_3.appendChild(cb_multi_response);

    row_4.appendChild(lb_show_user);
    row_4.appendChild(cb_show_user);

    // row_5.appendChild(lb_admin_permission);
    // row_5.appendChild(picker_user_admin);

    // row_6.appendChild(lb_contribute_permission);
    // row_6.appendChild(picker_user_contribute);

    // row_7.appendChild(lb_read_permission);
    // row_7.appendChild(picker_user_read);

    scroller.appendChild(row_1);
    scroller.appendChild(row_2);
    scroller.appendChild(row_3);
    scroller.appendChild(row_4);
    // scroller.appendChild(row_5);
    // scroller.appendChild(row_6);
    // scroller.appendChild(row_7);
    wrapper_content.appendChild(scroller)
    wrapper_content.appendChild(btn_save);
    wrapper_content.appendChild(btn_cancel);

    dialog_holder.appendChild(wrapper_content);

    btn_save.addEventListener("click", function () {
        wrapper_content.showLoading();
        if (txt_survey_title.getValue().length == 0) {
            toastr.error(ACE_SURVEY_STRINGS.error_not_fill_survey_title, { timeOut: 2000 });
        }
        var is_allow_show_user = cb_show_user.isChecked(),
            is_allow_multiple_response = cb_multi_response.isChecked();
        var user_read, user_contribute, user_admin;
        //  user_read = picker_user_read.getUserInfos().map(function (item) {
        //     return item.Key;
        // }).join(';');

        //  user_contribute = picker_user_contribute.getUserInfos().map(function (item) {
        //     return item.Key;
        // }).join(';');

        //  user_admin = picker_user_admin.getUserInfos().map(function (item) {
        //     return item.Key;
        // }).join(';');

        if (self.survey_id == null) {
            AceSVDbInstance.createNewSurvey(
                txt_survey_title.getValue(),
                txt_survey_desc.getValue(),
                is_allow_multiple_response,
                is_allow_show_user,
                user_admin,
                user_contribute,
                user_read
            ).onSuccess(function (new_item) {
                window.location.href = _spPageContextInfo.webAbsoluteUrl + "/SitePages/ManageQuestions.aspx?listid={" + new_item.ListId + "}";
            }).onFailed(function (message) {
                alert(message);
            }).onDone(function () {
                wrapper_content.hideLoading();
            });

        } else {
            AceSVDbInstance.updateSurvey(
                self.survey_id,
                txt_survey_title.getValue(),
                txt_survey_desc.getValue(),
                is_allow_multiple_response,
                is_allow_show_user,
                user_admin,
                user_contribute,
                user_read
            ).onSuccess(function (new_item) {
                toastr.success(ACE_SURVEY_STRINGS.update_success);
                if (self.listener.onSaved) {
                    self.listener.onSaved(self.survey_id, txt_survey_title.getValue(), txt_survey_desc.getValue());
                }
                self.destroy();
            }).onFailed(function (message) {
                alert(message);
            }).onDone(function () {
                wrapper_content.hideLoading();
            });

        }

        return false;
    });

    btn_cancel.addEventListener("click", function () {
        self.destroy();
        return false;
    });

    this.__renderSurveyInfo = function () {
        wrapper_content.showLoading();
        lb_header.setText(ACE_SURVEY_STRINGS.dialog_header_update_survey);
        btn_save.setText(ACE_SURVEY_STRINGS.save);
        AceSVDbInstance
            .getSurveyListBasicInfo(self.survey_id)
            .onSuccess(function (info) {
                txt_survey_title.setValue(info.Title);
                txt_survey_desc.setValue(info.Description);
                cb_multi_response.setIsChecked(info.AllowMultiResponses);
                cb_show_user.setIsChecked(info.ShowUser);

                // Currently, Not addd user picker box into dialog
                // info.Admin.forEach(function (item) {
                //     picker_user_admin.addUser(item.LoginName);
                // });
                // info.Contribute.forEach(function (item) {
                //     picker_user_contribute.addUser(item.LoginName);
                // });
                // info.Reader.forEach(function (item) {
                //     picker_user_read.addUser(item.LoginName);
                // });
            }).onDone(function () {
                wrapper_content.hideLoading();
            });
    }

    return dialog_holder;
}

AceNewSurveyDialog.prototype.setSurveyId = function (survey_id) {
    this.survey_id = survey_id;
    this.__renderSurveyInfo(survey_id);
}

AceNewSurveyDialog.prototype.setOnSaved = function (callback) {
    this.listener.onSaved = callback;
}