
var AceReOrderQuestionDialog = function (list_question_model) {
    this.list_question_model = list_question_model || [];
    BaseAceSurveyWidget.call(this);
    this.listener = {
        onOrderred: null
    }
    this.appendTo(document.body);
}


// Inherit all BaseAceSurveyWidget method 
AceReOrderQuestionDialog.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceReOrderQuestionDialog.prototype.constructor = AceReOrderQuestionDialog;

AceReOrderQuestionDialog.prototype.template = function () {
    var self = this;
    var dialog_holder = new AceSurveyPanel(),
        wrapper_content = new AceSurveyPanel(),
        btn_up = new AceSurveyMaterialButton("keyboard_arrow_up"),
        btn_down = new AceSurveyMaterialButton("keyboard_arrow_down"),
        pn_list_queston = new AceSurveyPanel(),
        btn_ok = new AceSurveyButton(ACE_SURVEY_STRINGS.save),
        btn_cancel = new AceSurveyButton(ACE_SURVEY_STRINGS.cancel);

    dialog_holder.addClass("ace-dialog-holder");
    wrapper_content.addClass("ace-dialog-reorder-question");
    pn_list_queston.addClass("ace-panel-lst-question");
    btn_up.addClass('ace-btn-reorder-question-up');
    btn_down.addClass('ace-btn-reorder-question-down');
    btn_ok.addClass("ace-btn-ok");
    btn_cancel.addClass("ace-btn-cancel");

    var question_model_selecting = null, row_selecting;

    var array_row_question = [], array_question_model_orderred = [];

    this.list_question_model.forEach(function (question_model, index) {
        var row_question_title = new AceSurveyPanel();
        row_question_title.setHtml(question_model.title);
        pn_list_queston.appendChild(row_question_title);
        row_question_title.addEventListener('click', function (e) {
            if (row_selecting) {
                row_selecting.removeClass('ace-selecting');
            }
            row_selecting = row_question_title;
            row_selecting.addClass('ace-selecting');
            question_model_selecting = question_model;
        });
        array_row_question.push(row_question_title);
        array_question_model_orderred.push(question_model);
    });


    wrapper_content.appendChild(btn_up);
    wrapper_content.appendChild(btn_down);
    wrapper_content.appendChild(pn_list_queston);
    wrapper_content.appendChild(btn_ok);
    wrapper_content.appendChild(btn_cancel);

    dialog_holder.appendChild(wrapper_content);

    function doSwapQuestionItem(is_down) {
        if (row_selecting == null) {
            return;
        }
        var row_index = array_row_question.indexOf(row_selecting);
        console.log(row_index, array_row_question.length);
        if (is_down == true && row_index < array_row_question.length - 2) {
            pn_list_queston.element.removeChild(row_selecting.element);
            // allow move down
            var next_row = array_row_question[row_index + 2];

            array_row_question.splice(row_index, 1);

            array_row_question.splice(row_index + 1, 0, row_selecting);

            var temp = array_question_model_orderred.splice(row_index, 1)[0];
            array_question_model_orderred.splice(row_index + 1, 0, temp);

            pn_list_queston.element.insertBefore(row_selecting.element, next_row.element);
        }
        else if (is_down == true && row_index < array_row_question.length - 1) {
            pn_list_queston.element.removeChild(row_selecting.element);
            // allow move down
            var next_row = array_row_question[array_row_question.length - 1];

            array_row_question.splice(row_index, 1);

            array_row_question.push(row_selecting);

            var temp = array_question_model_orderred.splice(row_index, 1)[0];
            array_question_model_orderred.push(temp);

            pn_list_queston.element.appendChild(row_selecting.element);

        }
        else if (is_down == false && row_index > 0) {
            pn_list_queston.element.removeChild(row_selecting.element);
            // allow move up
            var prev_row = array_row_question[row_index - 1];

            array_row_question.splice(row_index, 1);

            array_row_question.splice(row_index - 1, 0, row_selecting);

            var temp = array_question_model_orderred.splice(row_index, 1)[0];
            array_question_model_orderred.splice(row_index - 1, 0, temp);

            pn_list_queston.element.insertBefore(row_selecting.element, prev_row.element);
        }

    }

    btn_ok.addEventListener("click", function () {
        self.destroy('ace-sv-anim-scale-out');
        if (self.listener.onOrderred && array_question_model_orderred.length > 0) {
            self.listener.onOrderred(array_question_model_orderred);
        }
        return false;
    });

    btn_cancel.addEventListener("click", function () {
        self.destroy('ace-sv-anim-scale-out');
        return false;
    });

    btn_up.addEventListener("click", function () {
        doSwapQuestionItem(false);
        return false;
    });

    btn_down.addEventListener("click", function () {
        doSwapQuestionItem(true);
        return false;
    });
    dialog_holder.addAnimationClass('ace-sv-anim-scale-in');
    return dialog_holder;
}

AceReOrderQuestionDialog.prototype.onOrderred = function (callback) {
    this.listener.onOrderred = callback;
}


