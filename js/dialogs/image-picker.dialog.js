


var AceImagePickerDialog = function (image_url) {
    this.image_url = image_url;
    BaseAceSurveyWidget.call(this);
    this.listener = {
        onImagePicked: null
    }
    this.appendTo(document.body);
}

// Inherit all BaseAceSurveyWidget method 
AceImagePickerDialog.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceImagePickerDialog.prototype.constructor = AceImagePickerDialog;

AceImagePickerDialog.prototype.template = function () {
    var self = this;
    var dialog_holder = new AceSurveyPanel(),
        wrapper_content = new AceSurveyPanel(),
        txt_image_url = new AceSurveyEdittext(),
        btn_choose_image = new AceSurveyButton(ACE_SURVEY_STRINGS.choose_image),
        imv_preview = new AceSurveyPanel(),
        btn_ok = new AceSurveyButton(ACE_SURVEY_STRINGS.save),
        btn_cancel = new AceSurveyButton(ACE_SURVEY_STRINGS.cancel);

    
    var image_base64 = this.image_url;

    var file_picker = document.createElement("input");
    file_picker.type = 'file';
    file_picker.accept = '.png,.jpg,.jpeg,.gif';
    $(file_picker).css({
        'position': 'absolute',
        'top': 0,
        'bottom': 0,
        'z-index': -1,
        'opacity': 0
    });

    dialog_holder.element.appendChild(file_picker);

    txt_image_url.setAttribute('placeholder', ACE_SURVEY_STRINGS.hint_text_enter_image_url);

    dialog_holder.addClass("ace-dialog-holder");
   
    btn_choose_image.addClass('btn-choose-image');
    btn_ok.addClass("ace-btn-ok");
    btn_cancel.addClass("ace-btn-cancel");
    wrapper_content.addClass("ace-dialog-image-picker")
    txt_image_url.addClass("txt-image-url");
    imv_preview.addClass("imv-preview");

    if (typeof this.image_url === 'string' && this.image_url.length > 0) {
        imv_preview.addCss({
            'background-image': 'url("' + this.image_url + '")'
        });
        // txt_image_url.setValue(this.image_url);
    }

    // wrapper_content.appendChild(txt_image_url);
    wrapper_content.appendChild(btn_choose_image);
    wrapper_content.appendChild(imv_preview);
    wrapper_content.appendChild(btn_cancel);
    wrapper_content.appendChild(btn_ok);

    dialog_holder.appendChild(wrapper_content);

    txt_image_url.addOnTextChanged(function (e) {
        // imv_preview.setImageUrl(txt_image_url.getValue());
        imv_preview.addCss({
            'background-image': 'url("' + txt_image_url.getValue() + '")'
        })
    });

    file_picker.onchange = function (e) {
        if (Object.keys(e.target.files).length > 0) {
            AceSurveyUtility
                .imageFile2Base64(e.target.files[0], 0.6)
                .onLoaded(function (base64) {
                    image_base64 = base64;
                    imv_preview.addCss({
                        'background-image': 'url("' + base64 + '")'
                    })
                });
        }
    }

    btn_choose_image.addEventListener('click', function () {
        file_picker.click();
        return false;
    })

    // txt_image_url.addEventListener('keyup', function (e) {
    //     if (e.keyCode == 13 && txt_image_url.getValue().length > 0) {
    //         if (self.listener.onImagePicked) {
    //             self.listener.onImagePicked(txt_image_url.getValue());
    //         }
    //         self.destroy('ace-sv-anim-scale-out');
    //     }
    // })



    btn_ok.addEventListener("click", function () {
        self.destroy('ace-sv-anim-scale-out');
        if (self.listener.onImagePicked && image_base64.length > 0) {
            self.listener.onImagePicked(image_base64);
        }
        return false;
    });

    btn_cancel.addEventListener("click", function () {
        self.destroy('ace-sv-anim-scale-out');
        return false;
    });


    dialog_holder.addAnimationClass('ace-sv-anim-scale-in');
    return dialog_holder;
}

AceImagePickerDialog.prototype.onImagePicker = function (callback) {
    this.listener.onImagePicked = callback;
}