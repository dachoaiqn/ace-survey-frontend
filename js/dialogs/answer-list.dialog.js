


var AceAnswerListDialog = function (question_model) {
    this.question_model = question_model;
    BaseAceSurveyWidget.call(this);
    this.listener = {
        onImagePicked: null
    }
    this.appendTo(document.body);
}



// Inherit all BaseAceSurveyWidget method 
AceAnswerListDialog.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceAnswerListDialog.prototype.constructor = AceAnswerListDialog;

AceAnswerListDialog.prototype.template = function () {
    var self = this;

    var dialog_holder = new AceSurveyPanel(),
        wrapper_content = new AceSurveyPanel(),
        table_container = new AceSurveyPanel(),
        lb_question_title = new AceSurveyLabel(self.question_model.title),
        btn_close = new AceSurveyMaterialButton('clear');


    dialog_holder.addClass("ace-dialog-holder");
    wrapper_content.addClass('ace-dialog-user-poll');

    table_container.addClass('tbl-container');
    lb_question_title.addClass('lb-question');
    btn_close.addClass('btn-close');


    wrapper_content.appendChild(table_container);
    wrapper_content.appendChild(btn_close);
    wrapper_content.appendChild(lb_question_title);


    dialog_holder.appendChild(wrapper_content);
    dialog_holder.addAnimationClass('ace-sv-anim-scale-in');

    setTimeout(function () {
        // Render answer of per user
        var html_table_result = "<table class='ace-tbl-user-poll' >";
        var user_polls = self.question_model.toUserPollList();
        if (ACE_SV_GLOBAL_VALUE.survey_basic_info.ShowUser) {
            html_table_result = user_polls.reduce(function (output, item) {
                var html_answer = AceSurveyUtility.convertUserAnswerToHtml(self.question_model, item.answer, ACE_SURVEY_STRINGS.empty_answer);
                output += '<tr><td>' + item.user.full_name + '</td><td>' + html_answer + '</td></tr>';
                return output;
            }, html_table_result);
        } else {
            html_table_result = user_polls.reduce(function (output, item) {
                var html_answer = AceSurveyUtility.convertUserAnswerToHtml(self.question_model, item.answer, ACE_SURVEY_STRINGS.empty_answer);
                output += '<tr><td>' + html_answer + '</td></tr>';
                return output;
            }, html_table_result);
        }
        html_table_result += '</table>';
        table_container.setHtml(html_table_result);
    }, 200);

    btn_close.addEventListener('click', function () {
        self.destroy('ace-sv-anim-scale-out');
    });

    return dialog_holder;
}
