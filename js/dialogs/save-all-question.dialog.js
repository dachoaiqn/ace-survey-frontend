
var AceSaveAllQuestionDialog = function (survey_list_title, list_question_model) {
    this.list_question_model = list_question_model || [];
    this.survey_list_title = survey_list_title;
    BaseAceSurveyWidget.call(this);
    this.listener = {
        onOrderred: null
    }
    this.appendTo(document.body);
}


// Inherit all BaseAceSurveyWidget method 
AceSaveAllQuestionDialog.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSaveAllQuestionDialog.prototype.constructor = AceSaveAllQuestionDialog;

AceSaveAllQuestionDialog.prototype.template = function () {
    var self = this;
    var dialog_holder = new AceSurveyPanel(),
        wrapper_content = new AceSurveyPanel(),
        pn_list_queston = new AceSurveyPanel(),
        lb_count = new AceSurveyLabel(ACE_SURVEY_STRINGS.save_question_progressing_has_saved + ' 0/' + self.list_question_model.length),
        btn_cancel = new AceSurveyButton(ACE_SURVEY_STRINGS.close);

    lb_count.addClass('lb-count-success');

    btn_cancel.addClass("ace-btn-cancel");
    btn_cancel.hide();


    dialog_holder.addClass("ace-dialog-holder");
    wrapper_content.addClass("ace-dialog-save-question");
    pn_list_queston.addClass("ace-panel-lst-question");

    var array_pn_question = [];

    this.list_question_model.forEach(function (question_model, index) {
        var row_question_title = new AceSurveyPanel();
        row_question_title.setHtml(question_model.title);
        pn_list_queston.appendChild(row_question_title);
        array_pn_question.push(row_question_title);
    });

    var count_success = 0;
    // Submit sequence question - use recursive
    var recusive_submit = function (index) {
        if (index >= self.list_question_model.length) {
            btn_cancel.show();
            wrapper_content.hideLoading();
            return;
        }
        // get UI - row question title  
        var row_question = array_pn_question[index];
        wrapper_content.showLoading();
        setTimeout(function () {
            var q = self.list_question_model[index];
            if (q.isNewModel()) {
                AceSVDbInstance.createSurveyQuestion(self.survey_list_title, q)
                    .onSuccess(function () {
                        row_question.addClass("success");
                        count_success++;
                        lb_count.setText(ACE_SURVEY_STRINGS.save_question_progressing_has_saved + " " + count_success + '/' + self.list_question_model.length);
                    })
                    .onFailed(function (message) {
                        row_question.addClass("failed");

                    }).onDone(function () {
                        row_question.hideLoading();
                        recusive_submit(index + 1);
                    });
            } else {
                AceSVDbInstance.updateSurveyQuestion(self.survey_list_title, q)
                    .onSuccess(function () {
                        row_question.addClass("success");
                        count_success++;
                        lb_count.setText(ACE_SURVEY_STRINGS.save_question_progressing_has_saved + " " + count_success + '/' + self.list_question_model.length);
                    })
                    .onFailed(function (message) {
                        row_question.addClass("failed");
                    }).onDone(function () {
                        recusive_submit(index + 1);
                    });
            }
        }, 2000);
    }

    recusive_submit(0);


    wrapper_content.appendChild(pn_list_queston);
    wrapper_content.appendChild(lb_count);
    wrapper_content.appendChild(btn_cancel);


    dialog_holder.appendChild(wrapper_content);

    dialog_holder.addAnimationClass('ace-sv-anim-scale-in');

    btn_cancel.addEventListener("click", function () {
        self.destroy('ace-sv-anim-scale-out');
        return false;
    });
    return dialog_holder;
}

AceSaveAllQuestionDialog.prototype.onOrderred = function (callback) {
    this.listener.onOrderred = callback;
}


