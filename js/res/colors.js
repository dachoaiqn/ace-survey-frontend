var ACE_CHART_COLOR = {
    backgroundColor: [
        'rgba(255, 99, 132, 0.4)',
        'rgba(54, 162, 235, 0.4)',
        'rgba(255, 206, 86, 0.4)',
        'rgba(75, 192, 192, 0.4)',
        'rgba(153, 102, 255, 0.4)',
        'rgba(255, 159, 64, 0.4)',
        'rgba(106, 0, 255,0.4)',
        'rgba(89, 236, 255,0.4)',
        'rgba(208, 255, 89,0.4)',
        'rgba(255, 132, 0,0.4)',
        'rgba(3, 150, 163,0.4)',
        'rgba(163, 3, 27,0.4)',
        'rgba(255, 132, 0,0.4)',
        'rgba(3, 51, 163,0.4)',
        'rgba(212, 0, 255,0.4)',
        'rgba(18, 237, 2,0.4)',
        'rgba(135, 213, 255,0.4)',
        'rgba(6, 38, 56,0.4)',
        'rgba(120, 205, 255,0.4)',
        'rgba(241, 163, 255,0.4)'

        // 'rgba(255, 99, 132, 0.5)',
        // 'rgba(54, 162, 235, 0.5)',
        // 'rgba(255, 206, 86, 0.5)',
        // 'rgba(75, 192, 192, 0.5)',
        // 'rgba(153, 102, 255, 0.5)',
        // 'rgba(255, 159, 64, 0.5)',
    ],
    borderColor: [
        'rgba(255, 99, 132, 0.8)',
        'rgba(54, 162, 235, 0.8)',
        'rgba(255, 206, 86, 0.8)',
        'rgba(75, 192, 192, 0.8)',
        'rgba(153, 102, 255, 0.8)',
        'rgba(255, 159, 64, 0.8)',
        'rgba(106, 0, 255,0.8)',
        'rgba(89, 236, 255,0.8)',
        'rgba(208, 255, 89,0.8)',
        'rgba(255, 132, 0,0.8)',
        'rgba(3, 150, 163,0.8)',
        'rgba(163, 3, 27,0.8)',
        'rgba(255, 132, 0,0.8)',
        'rgba(3, 51, 163,0.8)',
        'rgba(212, 0, 255,0.8)',
        'rgba(18, 237, 2,0.8)',
        'rgba(135, 213, 255,0.8)',
        'rgba(6, 38, 56,0.8)',
        'rgba(120, 205, 255,0.8)',
        'rgba(241, 163, 255,0.8)'
    ],
}



var dynamicColors = function () {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return ["rgba(" + r + "," + g + "," + b + ", 0.4)", "rgba(" + r + "," + g + "," + b + ", 0.8)"]
};

for (var i = 0; i < 50; i++) {
    var color = dynamicColors();
    ACE_CHART_COLOR.backgroundColor.push(color[0]);
    ACE_CHART_COLOR.borderColor.push(color[1]);
}