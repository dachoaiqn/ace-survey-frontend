document.addEventListener("click", function () {
    if (_q_type_spinner_showing != null) {
        _q_type_spinner_showing.destroy('ace-sv-anim-scale-out');
        _q_type_spinner_showing = null;
    }
});

var SV_QUESTION_SOURCES = [
    {
        name: "Single line",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT,
        icon: 'short_text'
    },
    {
        name: "Multiple line",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT,
        icon: 'note'
    },
    {
        name: "Number",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER,
        icon: 'looks_one'
    },
    {
        name: "Datetime",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME,
        icon: 'date_range'
    },
    {
        name: "Radio",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE,
        icon: 'radio_button_checked'
    },
    {
        name: "Drop-down",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN,
        icon: 'arrow_drop_down_circle'
    },
    {
        name: "Yes/No",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO,
        icon: 'done'
    },
    {
        name: "Multiple choice",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE,
        icon: 'check_box'
    },
    {
        name: "Matrix",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX,
        icon: 'apps'
    },
    {
        name: "Matrix text",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT,
        icon: 'featured_play_list'
    },
    {
        name: "Matrix Choice Text",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT,
        icon: 'format_list_numbered'
    },
    {
        name: "Matrix Multiple Choice ",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE,
        icon: 'done_all'
    },
    // {
    //     name: "Matrix Choice Text",
    //     key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT,
    //     icon: 'format_list_numbered'
    // },
    {
        name: "Currency",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY,
        icon: 'attach_money'
    },
    {
        name: "Peole only",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER,
        icon: 'account_circle'
    },
    {
        name: "People and group",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP,
        icon: 'group'
    },
    {
        name: "Page Separator",
        key: ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR,
        icon: 'description'
    }
]

var _q_type_spinner_showing = null;

var AceSurveyQuestionTypePicker = function (selected_key) {
    this.selected_index = 0;
    var self = this;
    if (selected_key != undefined) {
        this.question_type_selected = SV_QUESTION_SOURCES.filter(function (item, index) {
            if (item.key == selected_key) {
                self.selected_index = index;
                return true;
            } else {
                return false;
            }
        })[0];
    } else {
        this.question_type_selected = SV_QUESTION_SOURCES[0];
    }
    this.listeners = {
        onTypeSelected: null
    }
    BaseAceSurveyWidget.call(this);
    this.addEventListener('click', function (e) {
        e.stopPropagation();
    });
}

AceSurveyQuestionTypePicker.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyQuestionTypePicker.prototype.constructor = AceSurveyQuestionTypePicker;

AceSurveyQuestionTypePicker.prototype.template = function () {
    var DEFAULT_ITEM_HEIGHT = 36; // px
    var self = this;
    var wrapper = new AceSurveyPanel();
    wrapper.addClass('ace-question-type-picker-holder');

    var wrapper_choosen = new AceSurveyPanel();
    wrapper_choosen.addClass('ace-question-row-type');

    // Set default selected
    if (this.question_type_selected) {
        wrapper_choosen.setHtml('<i class="material-icons">' + this.question_type_selected.icon + '</i><span>' + this.question_type_selected.name + '</span>');
    }

    wrapper_choosen.addEventListener('click', function () {
        if (_q_type_spinner_showing != null) {
            _q_type_spinner_showing.destroy('ace-sv-anim-scale-out');
            _q_type_spinner_showing = null;
        }
        var picker_spinner = new AceSurveyPanel();

        _q_type_spinner_showing = picker_spinner;
        // Add click to prevent click on body
        picker_spinner.addEventListener('click', function (e) {
            e.stopPropagation();
        });
        picker_spinner.addClass('ace-question-type-spinner');
        wrapper.appendChild(picker_spinner);

        // Add type item to Spinner
        SV_QUESTION_SOURCES.forEach(function (item, index) {
            var row_question_and_type = new AceSurveyPanel();
            row_question_and_type.addClass('ace-question-row-type');
            row_question_and_type.setHtml('<i class="material-icons">' + item.icon + '</i><span>' + item.name + '</span>');
            if (item.key == self.question_type_selected.key) {
                // For Animation
                // var postion_top = DEFAULT_ITEM_HEIGHT * index;
                // picker_spinner.addCss({
                //     'top': -postion_top + "px",
                //     'transform-origin': 'center ' + postion_top + 'px'
                // });
                row_question_and_type.addClass('type-focusing')
            }

            row_question_and_type.addEventListener('click', function () {

                self.selected_index = index;
                picker_spinner.removeClass('ace-sv-anim-scale-in');
                picker_spinner.destroy('ace-sv-anim-scale-out');
                if (self.question_type_selected.key == item.key) {
                    return;
                }
                self.question_type_selected = item;
                wrapper_choosen.setHtml(row_question_and_type.getHtml());
                _q_type_spinner_showing = null;
                if (self.listeners.onTypeSelected) {
                    self.listeners.onTypeSelected.call(self, self.question_type_selected);
                }
            });
            picker_spinner.appendChild(row_question_and_type);
        });
    });
    wrapper.appendChild(wrapper_choosen);

    return wrapper;
}


AceSurveyQuestionTypePicker.prototype.setSelectedByQuestionKey = function (selected_key) {
    this.question_type_selected = SV_QUESTION_SOURCES.filter(function (item) {
        return item.key == selected_key;
    })[0];
    this.element.querySelector('.ace-question-row-type').innerHTML = '<i class="material-icons">' + this.question_type_selected.icon + '</i><span>' + this.question_type_selected.name + '</span>';
}

AceSurveyQuestionTypePicker.prototype.getTypeSelected = function () {
    return this.question_type_selected;
}

AceSurveyQuestionTypePicker.prototype.setOnTypeSelected = function (callback) {
    this.listeners.onTypeSelected = callback;
}

