

var AceUserAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AceUserAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceUserAnswer.prototype.constructor = AceUserAnswer;

AceUserAnswer.prototype.templateEditMode = function () {
    var self = this,
        holder = new AceSurveyPanel();

    return holder;
}

AceUserAnswer.prototype.templateViewMode = function () {
    var cache_user_ids = {};
    var self = this,
        holder = new AceSurveyPanel(),
        txt_answer = new AceSurveyEdittext();
    var holder_id = "ace-people-holder-" + self.question_model.id;
    holder.addClass("ace-user-answer-holder");
    holder.setElementId(holder_id);

    ['sp.core.js', 'sp.runtime.js', 'sp.js', 'autofill.js', 'clientpeoplepicker.js', 'clientforms.js', 'clienttemplates.js'].forEach(function (val, idx) {
        if (!_v_dictSod[val]) RegisterSod(val, "\u002f_layouts\u002f15\u002f" + val);
    });

    txt_answer.addClass('txt-user-answer');
    txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_type_your_answer);

    txt_answer.addEventListener('keyup', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    txt_answer.addEventListener('blur', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    holder.appendChild(txt_answer);
    setTimeout(function () {
        SP.SOD.loadMultiple(['sp.core.js', 'sp.runtime.js', 'sp.js', 'autofill.js', 'clientpeoplepicker.js', 'clientforms.js', 'clienttemplates.js'], function () {
            var schema = {};
            if (self.question_model.people_values.is_people_and_group == true) {
                schema['PrincipalAccountType'] = 'User,SecGroup,SPGroup';
            } else {
                schema['PrincipalAccountType'] = 'User';
            }
         
            schema['SearchPrincipalSource'] = 15;
            schema['ResolvePrincipalSource'] = 15;
            schema['AllowMultipleValues'] = self.question_model.sp_original_data.AllowMultipleValues;
            schema['MaximumEntitySuggestions'] = 50;
            schema['Width'] = '100%';
            SPClientPeoplePicker_InitStandaloneControlWrapper(holder_id, null, schema);
            var picker = SPClientPeoplePicker.SPClientPeoplePickerDict[holder_id + "_TopSpan"];
            var _waiter = null;
            picker.OnValueChangedClientScript = function (elementId, userInfo) {
                clearTimeout(_waiter);
                _waiter = setTimeout(function () {
                    if (userInfo.length > 0 && self.question_model.sp_original_data.AllowMultipleValues == false) {
                        // Always take first user
                        if (cache_user_ids[userInfo[0].Key] != undefined) {
                            self.question_model.user_answer = cache_user_ids[userInfo[0].Key];
                        } else {
                            holder.showLoading();
                            AceSVDbInstance
                                .getUserIdByLoginName(userInfo[0].Key)
                                .onSuccess(function (user_id) {
                                    self.question_model.user_answer = user_id;
                                    cache_user_ids[userInfo[0].Key] = user_id;
                                    holder.hideLoading();
                                }).onFailed(function (mess) {
                                    alert(mess);
                                    holder.hideLoading();
                                });
                        }

                    } else if (userInfo.length > 0) {
                        /// Put answer to array if the Answer allow multiple values
                        self.question_model.user_answer = [];
                        var is_dupplicate_user = [];
                        userInfo.forEach(function (item) {
                            if (is_dupplicate_user.indexOf(item.Key) == -1) {
                                is_dupplicate_user.push(item.Key);
                                if (cache_user_ids[item.Key] != undefined) {
                                    self.question_model.user_answer.push(cache_user_ids[item.Key]);
                                } else {
                                    holder.showLoading();
                                    AceSVDbInstance
                                        .getUserIdByLoginName(item.Key)
                                        .onSuccess(function (user_id) {
                                            holder.hideLoading();
                                            self.question_model.user_answer.push(user_id);
                                            cache_user_ids[item.Key] = user_id;
                                        }).onFailed(function (message) {
                                            holder.hideLoading();
                                        });
                                }
                            }
                        })
                    }
                }, 600)
            };

            setTimeout(function () {
                $('#ace-search-people-holder').css('visibility', 'visible');
                $('#ace-search-people-holder').find('.sp-peoplepicker-topLevel').css({
                    'box-sizing': 'border-box',
                    'border': 'none'
                });
                $('#ace-search-people-holder').find('.ms-helperText').html('Enter recipient(s) here')
            }, 100);
        });
    }, 2000)
    return holder;
}

