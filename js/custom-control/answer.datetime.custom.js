

/**
 * 
 * @param {QuestionModel} question_model 
 * @param {*} render_mode 
 */
var AceDateTimeAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AceDateTimeAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceDateTimeAnswer.prototype.constructor = AceDateTimeAnswer;

AceDateTimeAnswer.prototype.templateEditMode = function () {

    var self = this,
        holder = new AceSurveyPanel();

    return holder;
}

AceDateTimeAnswer.prototype.templateViewMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        txt_answer = new AceSurveyEdittext();

    holder.addClass("ace-text-answer-holder");

    txt_answer.addClass('txt-user-answer');
    txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_pick_date_answer);

    txt_answer.addEventListener('keyup', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    txt_answer.addEventListener('blur', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    $(txt_answer.element)
        .datepicker()
        .on('pick.datepicker', function (e) {
            console.log(e.date);
            self.question_model.user_answer = e.date;
        });;

    holder.appendChild(txt_answer);
    return holder;
}


AceDateTimeAnswer.prototype.getQuestionModel = function () {
    return this.question_model;
}

