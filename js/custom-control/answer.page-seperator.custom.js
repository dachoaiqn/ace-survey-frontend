

var AcePageSeparatorAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AcePageSeparatorAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AcePageSeparatorAnswer.prototype.constructor = AcePageSeparatorAnswer;

AcePageSeparatorAnswer.prototype.templateEditMode = function () {
    var self = this,
        holder = new AceSurveyPanel();

    return holder;
}

AcePageSeparatorAnswer.prototype.templateViewMode = function () {
    var self = this,
        holder = new AceSurveyPanel(),
        lb_title = new AceSurveyLabel(this.question_model.title);

    holder.addClass('ace-page-separator-holder');

    holder.appendChild(lb_title);

    return holder;
}
