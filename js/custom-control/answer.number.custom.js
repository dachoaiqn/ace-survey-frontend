

var AceNumberAnswer = function (question_model, render_mode) {

    // Override to render graph if the question is Number type
    if (question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER) {
        this.templateGraphMode = this.templateGraphMode2;
    }
    AceBaseAnswer.call(this, question_model, render_mode);

}

// Inherit all AceBaseAnswer method 
AceNumberAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceNumberAnswer.prototype.constructor = AceNumberAnswer;

AceNumberAnswer.prototype.templateEditMode = function () {
    var self = this,
        holder = new AceSurveyPanel();
    holder.addClass("ace-number-answer-holder");
    if (self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY) {
        var row_currency_format = new AceSurveyPanel();
        row_currency_format.addClass('row-option');
        var dropdown_currency_format = new AceSurveyDropdown(ACE_SURVEY_CONSTANT.CURRENCY_FORMAT);
        //   row_currency_format.appendChild(dropdown_currency_format);
        holder.appendChild(row_currency_format);

        row_currency_format.addEventListener("change", function (e) {
            self.question_model.currency_values.location_id = dropdown_range.getSelectedValue();
        });
    } else {

    }
    return holder;
}



AceNumberAnswer.prototype.templateGraphMode2 = function () {

    var self = this;
    // Convert poll data to chartjs lib
    // graph data is array have 2 items. [0,0] => [0] Yes answer, [1] = No anser
    var graph_data = this.question_model.toGraphData();

    var labels = [], values = [];
    for (var i = 0; i < graph_data.length; i++) {
        labels.push(graph_data[i][0]);
        values.push(graph_data[i][1]);
    }

    var chart_option = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                data: values,
                backgroundColor: ACE_CHART_COLOR.backgroundColor,
                borderColor: ACE_CHART_COLOR.borderColor,
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                position: 'left'
            },
        }
    }

    var holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel(),
        wrapper_graph = new AceSurveyPanel(),
        lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', self.question_model.totalAnswer())),
        view_graph = new AceSurveyChart(chart_option);


    wrapper_choice.addClass('editor-choice-group');
    wrapper_graph.addClass('pn-graph');
    holder.addClass("ace-choice-quest-holder");
    lb_total_answer.addClass("lb-total-answer");

    wrapper_graph.appendChild(lb_total_answer);
    wrapper_graph.appendChild(view_graph);
    holder.appendChild(wrapper_choice);
    holder.appendChild(wrapper_graph);


    lb_total_answer.addEventListener('click', function () {
        new AceAnswerListDialog(self.question_model);
    });

    return holder;
}

AceNumberAnswer.prototype.templateViewMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        txt_answer = new AceSurveyEdittext();

    holder.addClass("ace-text-answer-holder");

    txt_answer.addClass('txt-user-answer');
    txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_type_your_answer);
    txt_answer.setAttribute("type", 'number');

    txt_answer.addEventListener('keyup', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    txt_answer.addEventListener('blur', function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    holder.appendChild(txt_answer);
    return holder;
}