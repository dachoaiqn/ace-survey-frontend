


var AceSurveyQuestionViewer = function (survey_list_title, question_model) {
    if (survey_list_title == undefined) {
        throw 'Make sure pass the Survey List Title to constructor - AceSurveyQuestionViewer';
    }
    var self = this;
    this.question_model = question_model || new QuestionModel();
    this.survey_list_title = survey_list_title;
    this.choice_values = this.question_model.answer_values || [];
    BaseAceSurveyWidget.call(this);

    // Run after rendered;
    this.switchQuestionType(this.question_model.question_type);
}

AceSurveyQuestionViewer.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyQuestionViewer.prototype.constructor = AceSurveyQuestionViewer;

AceSurveyQuestionViewer.prototype.template = function () {
    var self = this;
    // var wrapper = new AceSurveyPanel();
    // wrapper.addClass('ace-question-holder');

    var pn_quest_section = new AceSurveyPanel();
    pn_quest_section.addClass('question-section');
    pn_quest_section.addClass('sv-view-mode');

    // if this question is Page Separator type then only return the holder without label of question copy
    if (self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
        pn_quest_section.addClass('sv-page-separator');

        var row_answer_section = this.pn_answer_section = new AceSurveyPanel();
        row_answer_section.addClass('answer-section');
        pn_quest_section.appendChild(row_answer_section);
        return pn_quest_section;
    }

    var row_question_and_type = new AceSurveyPanel();
    row_question_and_type.addClass('ace-question-edt-row');

    var row_answer_section = this.pn_answer_section = new AceSurveyPanel();
    row_answer_section.addClass('answer-section');

    var lb_question = new AceSurveyLabel(ACE_SURVEY_STRINGS.label_question);
    lb_question.addClass('lb-question');

    if (self.question_model.is_require) {
        var lb_flag_require = new AceSurveyLabel(ACE_SURVEY_STRINGS.flag_require_answer);
        lb_flag_require.addClass('ace-lb-flag flag-require-answer');
        pn_quest_section.appendChild(lb_flag_require);
    }

    var txt_question = new AceSurveyLabel('<span class="lb-question-no">' + this.question_model.question_no + '</span>' + this.question_model.title + "?");
    txt_question.element.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_question);
    txt_question.addClass("txt-question-copy");
    this.txt_question = txt_question;

    row_question_and_type.appendChild(lb_question);
    row_question_and_type.appendChild(txt_question);

    pn_quest_section.appendChild(row_question_and_type);
    pn_quest_section.appendChild(row_answer_section);

    return pn_quest_section;
}

AceSurveyQuestionViewer.prototype.setError = function (flag) {
    if (flag) {
        this.addClass('response-error');
    } else {
        this.removeClass('response-error');
    }
}


AceSurveyQuestionViewer.prototype.switchQuestionType = function (type_key) {
    this.pn_answer_section.clearHtml();
    switch (type_key) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT:
            this.renderTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
            this.renderNumberAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME:
            this.renderDateTimeAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            this.renderChoiceAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            this.renderMatrixAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR:
            this.renderPageSeparator();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
            this.renderYesNoAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
            this.renderUserAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            this.renderMatrixTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
            this.renderMatrixMultipleChoice();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            this.renderMatrixChoiceTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
            this.renderMatrixChoiceMuitlpleTextAnswer();
            break;
    }
}


AceSurveyQuestionViewer.prototype.renderTextAnswer = function () {
    var view_text = new AceTextAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_text);
}

AceSurveyQuestionViewer.prototype.renderNumberAnswer = function () {
    var view_number = new AceNumberAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_number);
}

AceSurveyQuestionViewer.prototype.renderDateTimeAnswer = function () {
    var view_text = new AceDateTimeAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_text);
}

AceSurveyQuestionViewer.prototype.renderChoiceAnswer = function () {
    var view_choice = new AceChoiceAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_choice);
}

AceSurveyQuestionViewer.prototype.renderMatrixAnswer = function () {
    var view_matrix = new AceMatrixAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_matrix);
}


AceSurveyQuestionViewer.prototype.renderMatrixMultipleChoice = function () {
    var view_matrix = new AceMatrixMultipleChoice(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_matrix);
}

AceSurveyQuestionViewer.prototype.renderMatrixTextAnswer = function () {
    var view_matrix = new AceMatrixTextAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_matrix);
}

AceSurveyQuestionViewer.prototype.renderMatrixChoiceTextAnswer = function () {
    var view_matrix = new AceMatrixChoiceTextAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_matrix);
}

// AceSurveyQuestionViewer.prototype.renderMatrixChoiceMuitlpleTextAnswer = function () {
//     var view_matrix = new AceMatrixMultiSubQuestion(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
//     this.pn_answer_section.appendChild(view_matrix);
// }

AceSurveyQuestionViewer.prototype.renderUserAnswer = function () {
    var view_user = new AceUserAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_user);
}

AceSurveyQuestionViewer.prototype.renderPageSeparator = function () {
    var view_separator = new AcePageSeparatorAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_separator);
}


AceSurveyQuestionViewer.prototype.renderYesNoAnswer = function () {
    var view_yes_no = new AceYesNoAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE);
    this.pn_answer_section.appendChild(view_yes_no);
}
