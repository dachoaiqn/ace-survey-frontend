

var AceMatrixMultipleChoice = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all BaseAceSurveyWidget method 
AceMatrixMultipleChoice.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceMatrixMultipleChoice.prototype.constructor = AceMatrixMultipleChoice;


AceMatrixMultipleChoice.prototype.templateEditMode = function () {
    var self = this,
        array_range = [],
        array_min_answer = [], array_max_answer = [], array_min_rate = [], array_max_rate = [];

    for (var i = 2; i <= 50; i++) {
        array_range.push({
            text: i,
            value: i
        });
    }

    var dropdown_limit_binder = {
        minAnswer: function () {
            array_min_answer = [{
                text: "Không",
                value: 0
            }];

            var max = self.question_model.answer_values.filter(function (item) {
                return item != null;
            }).length;
            for (var i = 1; i <= max; i++) {
                array_min_answer.push({
                    text: i + ' câu',
                    value: i
                });
            }

            var selected_value = self.question_model.rating_answer.another_config.min_answer || dropdown_min_answer.getSelectedIndex();
            dropdown_min_answer.setOptions(array_min_answer);

            if (selected_value < max && selected_value > -1) {
                dropdown_min_answer.setSelectedValue(selected_value);
                //   self.question_model.rating_answer.another_config.min_answer = dropdown_min_answer.getSelectedValue();
            }
        },
        maxAnswer: function () {
            array_max_answer = [{
                text: "Không",
                value: 0
            }];
            var selected_value = self.question_model.rating_answer.another_config.max_answer || dropdown_max_answer.getSelectedIndex();

            var min = dropdown_min_answer.getSelectedIndex() > -1 ? dropdown_min_answer.getSelectedIndex() : 1;
            var max = self.question_model.answer_values.length;
            for (var i = min; i <= max; i++) {
                array_max_answer.push({
                    text: i + ' câu',
                    value: i
                });
            }

            dropdown_max_answer.setOptions(array_max_answer);
            dropdown_max_answer.setSelectedValue(selected_value);
        },
        minRate: function () {
            array_min_rate = [{
                text: "Không",
                value: 0
            }];

            var min = 1;
            var max = dropdown_range.getSelectedValue();

            var selected_value = self.question_model.rating_answer.another_config.min_rate || dropdown_min_rate.getSelectedIndex();

            for (var i = min; i <= max; i++) {
                array_min_rate.push({
                    text: i + ' đáp án',
                    value: i
                });
            }
            dropdown_min_rate.setOptions(array_min_rate);
            dropdown_min_rate.setSelectedValue(selected_value);
        },
        maxRate: function () {
            array_max_rate = [{
                text: "Không",
                value: 0
            }];
            var min = dropdown_min_rate.getSelectedValue() >= 0 ? dropdown_min_rate.getSelectedValue() : 0;
            var max = dropdown_range.getSelectedValue();

            var selected_value = self.question_model.rating_answer.another_config.max_rate || dropdown_max_rate.getSelectedIndex();
            for (var i = min; i <= max; i++) {
                array_max_rate.push({
                    text: i + ' đáp án',
                    value: i
                });
            }
            dropdown_max_rate.setOptions(array_max_rate);
            dropdown_max_rate.setSelectedValue(selected_value);
        },
        bindAll: function () {
            this.minAnswer();
            this.maxAnswer();
            this.minRate();
            this.maxRate();
        },
        bindRate: function () {
            this.minRate();
            this.maxRate();
        }
    }

    // Filter the empty option
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length > 0);
    });

    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder");

    var wrapper_choice = new AceSurveyPanel(),
        row_text_range = new AceSurveyPanel(),
        btn_add_option = new AceSurveyMaterialButton('add', ACE_SURVEY_STRINGS.add_answer),
        row_range_picker = new AceSurveyPanel(),
        label_range = new AceSurveyLabel(ACE_SURVEY_STRINGS.range),
        label_min_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.min_answer),
        label_max_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.max_answer),
        label_min_rate = new AceSurveyLabel("Chọn tối thiểu"),
        label_max_rate = new AceSurveyLabel(",tối đa"),
        dropdown_range = new AceSurveyDropdown(array_range),
        dropdown_min_answer = new AceSurveyDropdown(),
        dropdown_max_answer = new AceSurveyDropdown(),
        dropdown_min_rate = new AceSurveyDropdown(),
        dropdown_max_rate = new AceSurveyDropdown();

    var row_rate_limit = new AceSurveyPanel();
    row_rate_limit.appendChild(label_min_rate);
    row_rate_limit.appendChild(dropdown_min_rate);
    row_rate_limit.appendChild(label_max_rate);
    row_rate_limit.appendChild(dropdown_max_rate);

    row_rate_limit.addClass('pn-rating-limit')


    row_range_picker.addClass("row-range");
    row_text_range.addClass('row-text-range');

    btn_add_option.addClass('btn-add-option');
    wrapper_choice.addClass('editor-choice-group');

    row_range_picker.appendChild(label_range);
    row_range_picker.appendChild(dropdown_range);
    row_range_picker.appendChild(label_min_answer);
    row_range_picker.appendChild(dropdown_min_answer);
    row_range_picker.appendChild(label_max_answer);
    row_range_picker.appendChild(dropdown_max_answer);
    row_range_picker.appendChild(row_rate_limit);

    matrix_holder.appendChild(row_range_picker);
    matrix_holder.appendChild(wrapper_choice);
    matrix_holder.appendChild(btn_add_option);

    var addHeader = function () {
        var row_header = new AceSurveyPanel();
        row_header.addClass('row-option');
        row_header.addClass('row-header');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = dropdown_range.getSelectedValue();
        var percent_width = 100 / max;
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel(),
                txt_second_label = new AceSurveyEdittext();
            var second_value = self.question_model.rating_answer.second_labels[i - 1];
            if (second_value == undefined) {
                self.question_model.rating_answer.second_labels.push('' + i);
                txt_second_label.setValue(i);
            } else if (typeof second_value === 'string' && second_value.length == 0) {
                txt_second_label.setValue(i);
            } else {
                txt_second_label.setValue(second_value);
            }
            sub_col.addCss({
                width: percent_width + "%"
            });
            sub_col.appendChild(txt_second_label);

            txt_second_label.setAttribute("ace-rating-val", i);
            txt_second_label.addOnTextChanged(function (e) {
                var rating_value = parseInt(e.currentTarget.getAttribute("ace-rating-val"));
                self.question_model.rating_answer.second_labels[rating_value - 1] = e.currentTarget.value.trim();
            });
            txt_second_label.addEventListener("blur", function (e) {
                var rating_value = parseInt(e.currentTarget.getAttribute("ace-rating-val"));
                if (self.question_model.rating_answer.second_labels[rating_value - 1].trim().length === 0) {
                    self.question_model.rating_answer.second_labels[rating_value - 1] = rating_value;
                    e.currentTarget.value = rating_value;
                }
            });
            col_choice.appendChild(sub_col);
        }

        row_header.appendChild(col_choice);
        wrapper_choice.appendChild(row_header);

    }

    var addOption = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option second-question');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = dropdown_range.getSelectedValue();
        var percent_width = 100 / max;
        for (var i = 0; i < max; i++) {
            (function (index) {
                if (self.question_model.answer_values[choice_values_index].second_questions == null) {
                    self.question_model.answer_values[choice_values_index].second_questions = [];
                }
                if (self.question_model.answer_values[choice_values_index].second_questions.length - 1 < index) {
                    self.question_model.answer_values[choice_values_index].second_questions.push("");
                }
                var sub_col = new AceSurveyPanel();
                sub_col.addCss({
                    width: percent_width + "%"
                });

                // var lb_second_question = new AceSurveyLabel('Câu hỏi phụ');
                // lb_second_question.addClass('lb-second-question');

                // sub_col.appendChild(lb_second_question);
                var checkbox = new AceSurveyCheckbox();
                checkbox.setAttribute('disabled', 'disabled');
                sub_col.appendChild(checkbox);

                col_choice.appendChild(sub_col);
            })(i);
        }

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var btn_remove_image = new AceSurveyMaterialButton('clear');

        var txt_answer = new AceSurveyTextArea();
        txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);
        txt_answer.setValue(choice_item.text);



        var btn_add_image = new AceSurveyMaterialButton('add_a_photo');
        btn_add_image.addClass('btn-add-photo');

        var btn_remove = new AceSurveyMaterialButton('clear');
        btn_remove.addClass('btn-remove');
        txt_answer.addClass('txt-answer-option');

        pn_image_answer.appendChild(btn_remove_image);

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(btn_add_image);
        row_option.appendChild(col_choice);
        row_option.appendChild(btn_remove);

        wrapper_choice.appendChild(row_option);

        txt_answer.focus();

        btn_remove.addEventListener("click", function () {
            row_option.destroy();
            self.question_model.answer_values[choice_values_index] = null;
            dropdown_limit_binder.minAnswer();
        });

        txt_answer.addEventListener('keyup', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });

        txt_answer.addEventListener('blur', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });

        btn_add_image.addEventListener('click', function (e) {
            var img_url = self.question_model.answer_values[choice_values_index].image_url;
            new AceImagePickerDialog(img_url)
                .onImagePicker(function (image_url) {
                    pn_image_answer.addClass('show-imv-answer');
                    pn_image_answer.addCss({
                        'background-image': 'url("' + image_url + '")'
                    });
                    self.question_model.answer_values[choice_values_index].image_url = image_url;
                    self.question_model.answer_values[choice_values_index].image_name = ACE_SURVEY_STRINGS.prefix_image_no.replace("##", moment().format('hhmmss_MMDD'));
                });
            return false;
        });

        btn_remove_image.addEventListener('click', function (e) {
            pn_image_answer.removeClass('show-imv-answer');
            self.question_model.answer_values[choice_values_index].image_url = '';
            self.question_model.answer_values[choice_values_index].image_name = '';
            return false;
        });
    }

    dropdown_range.setSelectedValue(self.question_model.rating_answer.end_number);
    dropdown_min_answer.setSelectedValue(self.question_model.rating_answer.another_config.min_answer);

    addHeader();

    if (self.question_model.answer_values.length == 0) {
        self.question_model.answer_values.push({
            text: '',
            image_url: '',
            image_name: '',
            second_questions: []
        });
        addOption(0);
    } else {
        // Restore old value if existed
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        })
    }

    dropdown_limit_binder.bindAll();

    btn_add_option.addEventListener('click', function () {
        self.question_model.answer_values.push({
            text: '',
            image_url: '',
            image_name: '',
            second_questions: []
        });
        var idx = self.question_model.answer_values.length - 1;
        addOption(idx);

        dropdown_limit_binder.minAnswer();
        dropdown_limit_binder.maxAnswer();

        return false;
    });

    function reRenderMatrixUI() {
        wrapper_choice.setHtml("");
        addHeader();
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        });
    }

    dropdown_range.addEventListener("change", function () {
        wrapper_choice.setHtml("");
        self.question_model.rating_answer.start_number = 2;
        self.question_model.rating_answer.end_number = dropdown_range.getSelectedValue();

        if (self.question_model.answer_values[0].second_questions.length > self.question_model.rating_answer.end_number) {
            self.question_model.answer_values.forEach(function (item) {
                item.second_questions.splice(self.question_model.rating_answer.end_number);
            });
        }
        reRenderMatrixUI();

        dropdown_limit_binder.bindRate();
    });

    dropdown_min_answer.addEventListener('change', function (e) {
        self.question_model.rating_answer.another_config.min_answer = dropdown_min_answer.getSelectedValue();

    });

    dropdown_max_answer.addEventListener('change', function (e) {
        self.question_model.rating_answer.another_config.max_answer = dropdown_max_answer.getSelectedValue();
    });

    dropdown_min_rate.addEventListener('change', function (e) {
        self.question_model.rating_answer.another_config.min_rate = dropdown_min_rate.getSelectedValue();
        dropdown_limit_binder.maxRate();
    });

    dropdown_max_rate.addEventListener('change', function (e) {
        self.question_model.rating_answer.another_config.max_rate = dropdown_max_rate.getSelectedValue();
    });

    return matrix_holder;
}

AceMatrixMultipleChoice.prototype.templateViewMode = function () {
    var self = this, header_answer_texts = [];
    // set default answer as Object
    this.question_model.user_answer = {};
    // Filter the empty option { text: 'string', image_url: 'string'}
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length);
    })

    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder");

    var wrapper_choice = new AceSurveyPanel(),
        row_text_range = new AceSurveyPanel(),
        txt_text_low = new AceSurveyLabel(self.question_model.rating_answer.text_range_low),
        txt_text_medium = new AceSurveyLabel(self.question_model.rating_answer.text_range_medium),
        txt_text_high = new AceSurveyLabel(self.question_model.rating_answer.text_range_high),
        lb_require_answer = new AceSurveyLabel('');

    lb_require_answer.addClass('lb-min-answer');

    var text_require = '';
    if (self.question_model.rating_answer.another_config.min_answer > 0) {
        text_require = 'Trả lời tối thiếu ' + self.question_model.rating_answer.another_config.min_answer + ' câu';
    }

    /*if (self.question_model.rating_answer.another_config.max_answer > 0 && text_require.length == 0) {
        text_require += 'Trả lời tối đa ' + self.question_model.rating_answer.another_config.max_answer + ' câu.';
    } else */if (self.question_model.rating_answer.another_config.max_answer > 0) {
        text_require += ', tối đa ' + self.question_model.rating_answer.another_config.max_answer + ' câu.';
    }

    if (self.question_model.rating_answer.another_config.min_rate > 0) {
        text_require += '<br/>Chọn tối thiểu ' + self.question_model.rating_answer.another_config.min_rate;
    } else {
        text_require += "<br/>Chọn tối thiểu: 0 "
    }

    if (self.question_model.rating_answer.another_config.max_rate > 0) {
        text_require += ', tối đa ' + self.question_model.rating_answer.another_config.max_rate;
    }

    if (text_require.length > 0) {
        lb_require_answer.setText(text_require);
        lb_require_answer.show();
    } else {
        lb_require_answer.setText('');
        lb_require_answer.hide();
    }

    row_text_range.addClass('row-text-range');
    wrapper_choice.addClass('editor-choice-group');

    txt_text_low.addCss({
        'float': 'left'
    });

    txt_text_high.addCss({
        'float': 'right'
    });

    row_text_range.appendChild(lb_require_answer);

    matrix_holder.appendChild(row_text_range);
    matrix_holder.appendChild(wrapper_choice);

    var track_na_text = this.question_model.rating_answer.na_text;

    var addHeader = function () {
        header_answer_texts = [];
        var row_header = new AceSurveyPanel();
        row_header.addClass('row-option');
        row_header.addClass('row-header');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = self.question_model.rating_answer.end_number;
        var percent_width = 100 / max;
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel();
            sub_col.setHtml(self.question_model.rating_answer.second_labels[i - 1] || i);
            sub_col.addCss({
                "width": percent_width + "%",
                "min-width": "100px"
            })
            col_choice.appendChild(sub_col);
            header_answer_texts.push(i);
        }

        row_header.appendChild(col_choice);
        wrapper_choice.appendChild(row_header);
    }

    var addOption = function (choice_values_index) {
        var tracking_pn_second_question_showing = null;
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option second-question');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = self.question_model.rating_answer.end_number;
        var percent_width = 100 / max;

        // Init Default answer

        var user_answer_item;
        if (self.question_model.user_answer["k_" + choice_values_index] == undefined) {
            var rating_item = self.question_model.answer_values[choice_values_index];
            var value = rating_item.text;
            if (rating_item && rating_item.image_url && rating_item.image_url.length > 0) {
                // If the image is existing in answer then get the image name to append to the answer 
                value += ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + rating_item.image_name;
            }
            user_answer_item = {
                Answer: -1,
                Question: value,
                SecondAnswer: []
            }
            self.question_model.user_answer["k_" + choice_values_index] = user_answer_item;
        } else {
            user_answer_item = self.question_model.user_answer["k_" + choice_values_index];
        }


        for (var i = 0; i < max; i++) {
            (function (col_index) {

                // Put empty answer by default - It same to Rating question (header)
                user_answer_item.SecondAnswer.push('');
                var sub_col = new AceSurveyPanel();


                var second_question_text = self
                    .question_model
                    .answer_values[choice_values_index]
                    .second_questions[col_index];

                var pn_second_question = new AceSurveyPanel(),
                    lb_second_question = new AceSurveyLabel(second_question_text),
                    checkbox_second_answer = new AceSurveyCheckbox();

                pn_second_question.addClass('pn-second-question');
                lb_second_question.addClass('lb-second-answer');


                pn_second_question.appendChild(lb_second_question);
                pn_second_question.appendChild(checkbox_second_answer);

                sub_col.appendChild(pn_second_question);

                sub_col.addCss({
                    "width": percent_width + "%",
                    "min-width": "100px"
                });

                col_choice.appendChild(sub_col);

                checkbox_second_answer.setAttribute('data-answer-index', i);

                checkbox_second_answer.addEventListener('click', function (e) {
                    var target = e.currentTarget;
                    //Slit to parent
                    var dv = $(target).parent().parent().parent();
                    var totalChecked = 0;
                    $(dv).find("input[type='checkbox']").each(function() {
                        if (this.checked)
                            totalChecked++;
                    });
                    //Validate row and column
                    if (totalChecked > self.question_model.rating_answer.another_config.max_rate) {
                        alert("Bạn không thể chọn nhiều hơn " + totalChecked + " loại");
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                    //get all checkboxes already check
                    //var chkAlrChecked = $(dv).find("input[type='checkbox']").is(":checked");
                    var rating_index = parseInt(target.getAttribute('data-answer-index'))

                    var allItems = $(dv).parent().parent();
                    var totalChecked = 0;
                    $(allItems).find("input[data-answer-index='" + rating_index + "']").each(function() {
                        if (this.checked)
                            totalChecked++;
                    });

                    if (totalChecked > self.question_model.rating_answer.another_config.max_answer) {
                        alert("Bạn không thể trả lời nhiều hơn " + totalChecked + " câu");
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }

                    var answer_text = self.question_model.rating_answer.second_labels[rating_index];
                    if (checkbox_second_answer.isChecked()) {
                        user_answer_item.SecondAnswer[col_index] = answer_text;
                    } else {
                        user_answer_item.SecondAnswer[col_index] = '';
                    }
                });
            })(i);
        }


        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var txt_answer = new AceSurveyLabel(choice_item.text);
        txt_answer.addClass('txt-answer-option');
        if (choice_item.text.length == 0) {
            txt_answer.hide();
        }

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(col_choice);
        wrapper_choice.appendChild(row_option);
    }

    addHeader();

    self.question_model.answer_values.forEach(function (value, index) {
        addOption(index);
    });

    return matrix_holder;
}

AceMatrixMultipleChoice.prototype.templateGraphMode = function () {
    var self = this;
    // set default answer as Object
    this.question_model.user_answer = {};
    // Filter the empty option { text: 'string', image_url: 'string'}
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length);
    });

    var graph_data = this.question_model.toGraphData();
    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder graph-mode");

    var wrapper_choice = new AceSurveyPanel();
    var lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', this.question_model.totalAnswer()));
    lb_total_answer.addClass("lb-total-answer");

    wrapper_choice.addClass('editor-choice-group');

    matrix_holder.appendChild(lb_total_answer);
    matrix_holder.appendChild(wrapper_choice);
    var track_na_text = this.question_model.rating_answer.na_text;

    var graph_labels = [], datasets = [];
    for (var i = 0; i < self.question_model.answer_values.length; i++) {
        graph_labels.push(self.question_model.answer_values[i].text);
    }


    var _total = {};

    for (var i = 1; i <= self.question_model.rating_answer.end_number; i++) {
        var data_item = {
            data: [],
            label: self.question_model.rating_answer.second_labels[i - 1] || i,
            backgroundColor: ACE_CHART_COLOR.backgroundColor[i - 1],
            borderColor: ACE_CHART_COLOR.borderColor[i - 1],
            borderWidth: 1
        }
        for (var j = 0; j < self.question_model.answer_values.length; j++) {
            if (graph_data[j] != undefined) {
                if (_total['idx_' + j] == undefined) {
                    _total['idx_' + j] = graph_data[j].slice(1).reduce(function (out, item) {
                        return out + item;
                    }, 0)
                }
                data_item.data.push(parseFloat((graph_data[j][i] * 100 / _total['idx_' + j]).toFixed(2)));
            } else {
                data_item.data.push('');
            }

        }
        datasets.push(data_item);
    }

    // for (var i = 0; i < 2; i++) {
    //     var data_item = {
    //         label: i == 0 ? 'Thích' : 'ko thích',
    //         data: [2, 1, 3],
    //         backgroundColor: ACE_CHART_COLOR.backgroundColor[i],
    //         borderColor: ACE_CHART_COLOR.borderColor[i],
    //         borderWidth: 1
    //     }
    //     datasets.push(data_item);
    // }

    var main_chart_option = {
        type: 'bar',
        data: {
            labels: graph_labels,
            datasets: datasets
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true.options,
                        stepSize: 10,
                        min: 0,
                        max: 100
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true.options,
                        stepSize: 10,
                        min: 0,
                        max: 100
                    }
                }]
            }
        }
    }
    var chart_type_option = [{
        text: 'Biểu đồ cột',
        value: 'bar'
    },
    {
        text: 'Biểu đồ cột ngang',
        value: 'horizontalBar'
    }]
    var view_main_chart = new AceSurveyChart(main_chart_option, chart_type_option);
    view_main_chart.setUnitPickerHidden(true);
    wrapper_choice.appendChild(view_main_chart);

    var addOption = function (choice_values_index) {
        // ["ANSWER_STRING", number,number,number...] - number is count of user choice
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');

        var max = self.question_model.rating_answer.end_number;
        //    max = (track_na_text.length > 0 ? (max + 1) : max)
        var graph_labels = [], graph_values = graph_data[choice_values_index];
        graph_values.shift();
        for (var i = 1; i <= max; i++) {
            graph_labels.push(self.question_model.rating_answer.second_labels[i - 1] || i);
        }
        if (track_na_text.length > 0) {
            graph_labels.push(track_na_text);
        }
        var chart_option = {
            type: 'bar',
            data: {
                labels: graph_labels,
                datasets: [{
                    data: graph_values,
                    backgroundColor: ACE_CHART_COLOR.backgroundColor,
                    borderColor: ACE_CHART_COLOR.borderColor,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
            }
        }
        var view_chart = new AceSurveyChart(chart_option);

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var txt_answer = new AceSurveyLabel(choice_item.text);
        txt_answer.addClass('txt-answer-option');
        if (choice_item.text.length == 0) {
            txt_answer.hide();
        }

        var btn_view_comment = new AceSurveyButton("Xem góp ý");
        var btn_view_chart = new AceSurveyButton("Xem biểu đồ");
        var table_comment_container = new AceSurveyPanel();
        btn_view_comment.addClass('btn-view-comment');
        btn_view_chart.addClass('btn-view-comment');
        table_comment_container.addClass('tbl-comment-viewer');
        // Render all user comment but not show Username
        table_comment_container.hide();
        var toggleCommentViewer = function (flag) {
            if (table_comment_container.element.childElementCount > 0 || flag == false) {
                table_comment_container.setHtml("");
                table_comment_container.hide();
                return;
            }
            var html_table_result = "<table class='ace-tbl-user-comment' valign='top' >";
            var user_polls = self.question_model.toUserPollList();
            console.log("user_polls", user_polls);
            // Display user name in response list
            var col_rating = [];
            var width = 100 / self.question_model.rating_answer.end_number + "%";
            for (var i = 0; i < self.question_model.rating_answer.end_number; i++) {
                col_rating.push("");
                var second_question_text = self.question_model.rating_answer.second_labels[i] || (i + 1);
                html_table_result += "<th style='width: " + width + "'>" + second_question_text + "</th>";
            }
            var col_length = col_rating.length;
            col_rating = user_polls.reduce(function (output, item) {
                var answer_part = item.answer[choice_values_index];
                console.log("answer_part", answer_part);
                if (answer_part != null &&
                    answer_part.SecondAnswer.length > 0) {
                    for (var i = 0; i < answer_part.SecondAnswer.length; i++) {
                        if (AceSurveyUtility.isEmptyString(answer_part.SecondAnswer[i]) == false) {
                            output[i] += "<p>" + answer_part.SecondAnswer[i] + "</p>";
                        }
                    }
                }
                return output;
            }, col_rating)

            html_table_result += "<tr>";
            col_rating.forEach(function (item) {
                html_table_result += "<td>" + item + "</td>";
            });

            html_table_result += "</tr><table>";

            table_comment_container.setHtml(html_table_result);
            table_comment_container.show();
        }
        col_choice.hide();

        col_choice.appendChild(view_chart);
        col_choice.appendChild(table_comment_container);
        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(btn_view_chart);
        row_option.appendChild(col_choice);
        //row_option.appendChild(btn_view_comment);
        wrapper_choice.appendChild(row_option);

        btn_view_comment.addEventListener("click", function () {
            try {
                toggleCommentViewer();
            } catch (e) {
                console.error(e);
            }
            return false;
        })

        btn_view_chart.addEventListener("click", function () {
            if (col_choice.isShowing()) {
                toggleCommentViewer(false);
                col_choice.hide();
            } else {
                toggleCommentViewer(true);
                col_choice.show();
            }

            return false;
        })
    }

    graph_data.forEach(function (value, index) {
        addOption(index);
    })

    lb_total_answer.addEventListener('click', function () {
        new AceAnswerListDialog(self.question_model);
    });

    return matrix_holder;
}


