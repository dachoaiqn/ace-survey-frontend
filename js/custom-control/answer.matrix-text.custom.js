

var AceMatrixTextAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all BaseAceSurveyWidget method 
AceMatrixTextAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceMatrixTextAnswer.prototype.constructor = AceMatrixTextAnswer;

AceMatrixTextAnswer.prototype.templateViewMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel();

    self.question_model.user_answer = self.question_model.answer_values.map(function (item) {
        return {
            Question: item != null ? item.text : "",
            Answer: ""
        };
    });

    holder.addClass("ace-matrix-text-answer-holder");
    wrapper_choice.addClass('editor-choice-group');

    holder.appendChild(wrapper_choice);

    var addOption = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        if (choice_item == null) {
            return;
        }
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var lb_sub_question = new AceSurveyLabel(choice_item.text);
        lb_sub_question.addClass('lb-sub-question');

        var txt_answer = new AceSurveyTextArea();
        txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);


        row_option.appendChild(lb_sub_question);
        row_option.appendChild(txt_answer);

        wrapper_choice.appendChild(row_option);


        txt_answer.addOnTextChanged(function (e) {
            self.question_model.user_answer[choice_values_index].Answer = txt_answer.getValue();
        });

    }
    if (self.question_model.answer_values != null) {
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        })
    }
    return holder;
}


AceMatrixTextAnswer.prototype.templateEditMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel(),
        btn_add_option = new AceSurveyMaterialButton('add', ACE_SURVEY_STRINGS.add_answer);

    holder.addClass("ace-matrix-text-answer-holder");
    btn_add_option.addClass('btn-add-option');
    wrapper_choice.addClass('editor-choice-group');

    holder.appendChild(wrapper_choice);
    holder.appendChild(btn_add_option);

    var addOption = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');


        var txt_sub_question = new AceSurveyTextArea();
        txt_sub_question.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);

        var btn_remove = new AceSurveyMaterialButton('clear');
        btn_remove.addClass('btn-remove-option');

        txt_sub_question.addClass('txt-answer-option');
        txt_sub_question.setValue(choice_item.text);

        row_option.appendChild(txt_sub_question);

        row_option.appendChild(btn_remove);
        wrapper_choice.appendChild(row_option);

        txt_sub_question.focus();

        btn_remove.addEventListener("click", function () {
            row_option.destroy();
            self.question_model.answer_values[choice_values_index] = null;
        });
        txt_sub_question.addEventListener('keyup', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_sub_question.getValue();
        });
        txt_sub_question.addEventListener('blur', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_sub_question.getValue();
        });

    }


    btn_add_option.addEventListener('click', function () {
        self.question_model.answer_values.push({
            text: '',
            image_url: ''
        });
        var idx = self.question_model.answer_values.length - 1;
        addOption(idx);
        return false;
    })

    if (self.question_model.answer_values == null || typeof self.question_model.answer_values === 'string') {
        self.question_model.answer_values = [];
    }

    if (self.question_model.answer_values.length == 0) {
        self.question_model.answer_values.push({
            text: '',
            image_url: ''
        });
        addOption(0);
    } else {
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        })
    }

    return holder;
}

