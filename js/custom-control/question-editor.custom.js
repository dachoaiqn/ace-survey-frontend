

var AceSurveyQuestionEditor = function (survey_list_title, question_model, all_questions) {
    if (survey_list_title == undefined) {
        throw 'Make sure pass the Survey List Title to constructor - AceSurveyQuestionEditor';
    }

    var self = this;
    this.question_model = question_model || new QuestionModel();
    this.survey_list_title = survey_list_title;
    this.choice_values = this.question_model.answer_values || [];
    this._all_questions = all_questions || [];
    BaseAceSurveyWidget.call(this);

    // Run after rendered;
    if (this.question_model.isNewModel() == false) {
        this.switchQuestionType(this.question_model.question_type);
    }
}

AceSurveyQuestionEditor.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyQuestionEditor.prototype.constructor = AceSurveyQuestionEditor;

AceSurveyQuestionEditor.prototype.template = function () {
    var self = this;
    // var wrapper = new AceSurveyPanel();
    // wrapper.addClass('ace-question-holder');

    var pn_quest_section = new AceSurveyPanel();
    pn_quest_section.addClass('question-section');

    var row_question_and_type = new AceSurveyPanel();
    row_question_and_type.addClass('ace-question-edt-row');

    var row_answer_section = this.pn_answer_section = new AceSurveyPanel();
    row_answer_section.addClass('answer-section');

    var lb_flag_new, lb_flag_not_save;
    if (self.question_model.isNewModel()) {
        lb_flag_new = new AceSurveyLabel(ACE_SURVEY_STRINGS.flag_new_question);
        lb_flag_new.addClass('ace-lb-flag flag-new-question');
        pn_quest_section.appendChild(lb_flag_new);
        pn_quest_section.addAnimationClass('anim-show');
    }

    lb_flag_not_save = new AceSurveyLabel(ACE_SURVEY_STRINGS.flag_not_save);
    lb_flag_not_save.addClass('ace-lb-flag flag-not-save');
    pn_quest_section.appendChild(lb_flag_not_save);
    // pn_quest_section.addClass('anim-show');
    lb_flag_not_save.hide();

    var tv_check_editted = setInterval(function () {
        try {
            if (self.has_destroy) {
                clearInterval(tv_check_editted);
                return;
            }
            if (self.question_model.hasSaved() == false) {
                pn_quest_section.removeClass("quest-saved")
                pn_quest_section.addClass("quest-not-save");
                lb_flag_not_save.show();
            } else if (pn_quest_section.hasClass("quest-not-save")) {
                pn_quest_section.removeClass("quest-not-save");
                pn_quest_section.addClass("quest-saved")
                lb_flag_not_save.hide();
                if (lb_flag_new) {
                    lb_flag_new.hide();
                }
            }
        } catch (err) {
            console.warn(err)
        }
    }, 2000);


    var lb_question = new AceSurveyLabel(ACE_SURVEY_STRINGS.label_question);
    lb_question.addClass('lb-question');

    var lb_hint_save = new AceSurveyLabel(ACE_SURVEY_STRINGS.hint_save_question_and_answer);
    lb_hint_save.addClass("lb-hint-save");

    var txt_question = new AceSurveyEdittext();
    txt_question.element.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_question);
    txt_question.addClass("txt-question-copy");
    this.txt_question = txt_question;
    txt_question.setValue(this.question_model.title);
    var btn_question_type = new AceSurveyQuestionTypePicker(this.question_model.question_type);

    row_question_and_type.appendChild(lb_question);
    row_question_and_type.appendChild(txt_question);
    row_question_and_type.appendChild(btn_question_type);

    var row_toolbar = new AceSurveyPanel();
    row_toolbar.addClass('ace-question-toolbar');

    var btn_remove = new AceSurveyMaterialButton("delete", ACE_SURVEY_STRINGS.remove_question);
    var btn_save = new AceSurveyMaterialButton("save", ACE_SURVEY_STRINGS.save_survey);
    var btn_refresh = new AceSurveyMaterialButton("refresh", ACE_SURVEY_STRINGS.question_refresh);

    var btn_require = new AceSurveyMaterialButton("check_box_outline_blank", ACE_SURVEY_STRINGS.require);
    btn_require.addCss({
        'color': '#888888',
        'float': 'left'
    });

    btn_remove.addCss({
        'float': 'left'
    });

    row_toolbar.appendChild(btn_require);
    row_toolbar.appendChild(btn_remove);
    row_toolbar.appendChild(btn_save);
    row_toolbar.appendChild(btn_refresh);

    pn_quest_section.appendChild(row_question_and_type);
    pn_quest_section.appendChild(row_answer_section);
    pn_quest_section.appendChild(row_toolbar);
    pn_quest_section.appendChild(lb_hint_save);

    // wrapper.appendChild(pn_quest_section);

    btn_remove.addEventListener('click', function () {
        self.deleteQuestion();
        return false;
    });
    var is_require = this.question_model.is_require;
    if (is_require) {
        btn_require.setIcon("check_box");
        btn_require.addCss({
            'color': '#fa0001'
        });
    } else {
        btn_require.setIcon("check_box_outline_blank");
        btn_require.addCss({
            'color': '#888888'
        })
    }
    btn_question_type.setOnTypeSelected(function (question_type) {
        if (self.question_model.isNewModel() == false && confirm(ACE_SURVEY_STRINGS.notice_change_type_loss_data) == false) {
            btn_question_type.setSelectedByQuestionKey(self.question_model.question_type)
        } else if (self.question_model.isNewModel() == false) {
            var old_type = self.question_model.question_type;
            self.question_model.question_type = question_type.key;
            self.saveQuestion(function () {
                self.switchQuestionType(question_type.key);
            }, function () {
                // Restore old kind 
                self.question_model.question_type = old_type;
                btn_question_type.setSelectedByQuestionKey(old_type)
            })
        } else {
            self.question_model.question_type = question_type.key;
            self.switchQuestionType(question_type.key);
        }
    });

    btn_save.addEventListener("click", function () {
        self.question_model.title = txt_question.getValue();

        self.question_model.is_require = is_require;
        self.question_model.question_type = btn_question_type.getTypeSelected().key;
        self.saveQuestion(function () {
            if (lb_flag_new) {
                lb_flag_new.destroy();
            }
            pn_quest_section.addClass('quest-saved');
        });
        return false;
    })

    btn_require.addEventListener('click', function () {
        is_require = !is_require;
        if (is_require) {
            btn_require.setIcon("check_box");
            btn_require.addCss({
                'color': '#fa0001'
            });
        } else {
            btn_require.setIcon("check_box_outline_blank");
            btn_require.addCss({
                'color': '#888888'
            })
        }
        self.question_model.is_require = is_require;
        if (self.question_model.isNewModel() == false) {
            self.saveQuestion();
        }
        return false;
    });

    txt_question.addOnTextChanged(function (e) {
        self.question_model.title = txt_question.getValue();

    })

    btn_refresh.addEventListener('click', function () {
        // Call restore state to rollback the backup version that create by saveState() method - BaseModel
        self.question_model.restoreState();
        self.switchQuestionType(self.question_model.question_type);
        txt_question.setValue(self.question_model.title);
        btn_question_type.setSelectedByQuestionKey(self.question_model.question_type);

        lb_flag_not_save.hide();
        pn_quest_section.removeClass("quest-not-save");

        return false;
    });

    return pn_quest_section;
}


AceSurveyQuestionEditor.prototype.switchQuestionType = function (type_key) {
    this.pn_answer_section.clearHtml();
    switch (type_key) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT:
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT:
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            this.renderChoiceAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            this.renderMatrixAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            this.renderMatrixTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            this.renderMatrixChoiceTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
            this.renderMatrixChoiceMuitlpleTextAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
            this.renderMatrixMultipleChoice();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
            this.renderUserAnswer();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
            this.renderNumberAnswer();
            break;

    }

    if (this.question_model.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
        this.renderJumpToConfig()
    }
}


AceSurveyQuestionEditor.prototype.renderChoiceAnswer = function () {
    var view_choice = new AceChoiceAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    // view_choice.setAllQuestions(this.all_questions);
    this.pn_answer_section.appendChild(view_choice);
}

AceSurveyQuestionEditor.prototype.renderMatrixAnswer = function () {
    var view_matrix = new AceMatrixAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_matrix);
}

AceSurveyQuestionEditor.prototype.renderMatrixTextAnswer = function () {
    var view_matrix = new AceMatrixTextAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_matrix);
}

AceSurveyQuestionEditor.prototype.renderMatrixChoiceTextAnswer = function () {
    var view_matrix = new AceMatrixChoiceTextAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_matrix);
}


AceSurveyQuestionEditor.prototype.renderMatrixMultipleChoice = function () {
    var view_matrix = new AceMatrixMultipleChoice(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_matrix);
}
// AceSurveyQuestionEditor.prototype.renderMatrixChoiceMuitlpleTextAnswer = function () {
//     var view_matrix = new AceMatrixMultiSubQuestion(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
//     this.pn_answer_section.appendChild(view_matrix);
// }


AceSurveyQuestionEditor.prototype.renderUserAnswer = function () {
    var view_user = new AceUserAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_user);
}

AceSurveyQuestionEditor.prototype.renderNumberAnswer = function () {
    var view_number = new AceNumberAnswer(this.question_model, ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT);
    this.pn_answer_section.appendChild(view_number);
}

AceSurveyQuestionEditor.prototype.saveQuestion = function (onSuccess, onError) {
    this.showLoading();
    var self = this;
    var success_callback = function () {
        // Save state of question
        self.question_model.saveState();
        toastr.success(ACE_SURVEY_STRINGS.update_success, { timeOut: 2000 });
        switch (self.question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
                self.choice_values = self.question_model.answer_values;
                break;
            default:
                self.question_model.answer_values = [];
                self.choice_values = [];
                break;
        }
        if (onSuccess) {
            onSuccess();
        }
        self.hideLoading();
    }
    if (self.question_model.answer_values && Array.isArray(self.question_model.answer_values)) {
        self.question_model.answer_values = self.question_model.answer_values.filter(function (item) {
            if (typeof item === 'object') {
                return item != null && (item.text.length > 0 || item.image_url.length > 0);
            } else {
                return item != null && item.length > 0;
            }
        });
    }
    if (self.question_model.isNewModel()) {
        AceSVDbInstance.createSurveyQuestion(self.survey_list_title, self.question_model)
            .onSuccess(success_callback)
            .onFailed(function (message) {
                alert(ACE_SURVEY_STRINGS.update_error + "\n" + message);
                if (onError) {
                    onError();
                }
            }).onDone(function () {
                self.hideLoading();
            });
    } else {
        AceSVDbInstance.updateSurveyQuestion(self.survey_list_title, self.question_model)
            .onSuccess(success_callback)
            .onFailed(function (message) {
                alert(ACE_SURVEY_STRINGS.update_error + "\n" + message);
                if (onError) {
                    onError();
                }
            }).onDone(function () {
                self.hideLoading();
            });
    }
}

AceSurveyQuestionEditor.prototype.deleteQuestion = function () {
    var self = this;
    if (confirm(ACE_SURVEY_STRINGS.confirm_remove)) {
        this.showLoading();
        if (self.question_model.isNewModel() == false) {
            AceSVDbInstance.deleteSurveyQuestion(self.survey_list_title, self.question_model)
                .onSuccess(function () {
                    self.destroy('anim-destroy');
                    self.question_model.is_deleted = true;
                }).onFailed(function (message) {
                    alert(message);
                }).onDone(function () {
                    self.hideLoading();
                });
        } else {
            self.question_model.is_deleted = true;
            self.destroy('anim-destroy');
        }
    }
}

AceSurveyQuestionEditor.prototype.getQuestionModel = function () {
    return this.question_model;
}


AceSurveyQuestionEditor.prototype.renderJumpToConfig = function () {
    if (this.question_model.isNewModel() == true || this._all_questions.length == 0) {
        return;
    }
    var self = this;

    var question_index = AceSurveyUtility.Array(self._all_questions).findIndex(function (quest) {
        return quest.id == self.question_model.id;
    });

    var questions_can_jump_to = self._all_questions.slice(question_index + 1);


    var section_filter = [], next_section, question_same_section = [], has_next_section = false;

    for (var i = 0; i < questions_can_jump_to.length; i++) {
        var q = questions_can_jump_to[i];
        if (has_next_section == false && q.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
            question_same_section.push(q);
        }
        if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
            has_next_section = true;
            if (next_section != null) {
                section_filter.push(next_section);
            }
            next_section = {
                question: q,
                index: i,
                questions_child: [],
            }
        } else if (q.is_require == false && next_section) {
            next_section.questions_child.push(q);
        } else if (q.is_require == true) {
            // Clear next question if have any child that require answer
            next_section = null;
            break;
        } else if (q.IsRatingRequire()) {
            next_section = null;
            break;
        }

    }
    if (next_section) {
        section_filter.push(next_section);
    }

    // Convert Section (Page separator) to option
    var options_jumpto = section_filter.map(function (item) {
        return {
            text: item.question.title,
            // Default jump to first question in a section
            value: item.questions_child[0].length > 0 ? item.questions_child[0].id : item.question.id
        }
    });

    // Merger the question that same section to this
    options_jumpto = question_same_section
        .map(function (item) {
            return {
                text: item.title,
                value: item.id
            }
        }).concat(options_jumpto);

    options_jumpto.unshift({
        text: "Không",
        value: ""
    })

    // Not in question list
    if (question_index == -1) {
        return;
    }

    var jumpto_holder = new AceSurveyPanel();
    this.pn_answer_section.appendChild(jumpto_holder);

    var lb_jumto = new AceSurveyLabel("Jump to");
    lb_jumto.addClass("lb-jumpto");

    jumpto_holder.appendChild(lb_jumto);
    jumpto_holder.addClass('pn-jumpto-config');

    var addOption = function (answer_option, index) {

        // "answer_option" structure: { text: '',  image_url: '',  image_name: ''}
        var row_jumpto_option = new AceSurveyPanel();
        row_jumpto_option.addClass('row-jumpto');

        var lb_answer = new AceSurveyLabel(answer_option.text);
        lb_answer.addClass('lb-answer-need-jumpto');

        var dropdown_jumpto = new AceSurveyDropdown(options_jumpto);
        dropdown_jumpto.addClass('dropdown-jumpto-question');
        dropdown_jumpto.setSelectedValue(
            self.question_model.jumpto_config[index].jumpto_question_id
        )

        row_jumpto_option.appendChild(lb_answer);
        row_jumpto_option.appendChild(dropdown_jumpto);

        row_jumpto_option.addEventListener('change', function () {
            self.question_model.jumpto_config[index].jumpto_question_id = dropdown_jumpto.getSelectedValue();
        });

        jumpto_holder.appendChild(row_jumpto_option);
    }.bind(this);


    // Used to render the option for each question type
    var option_binder = {
        renderOptionForChoice: function () {
            for (var i = 0; i < self.question_model.answer_values.length; i++) {
                var option = self.question_model.answer_values[i];
                if (self.question_model.jumpto_config[i] == null) {
                    self.question_model.jumpto_config.push({
                        answer_index: i,
                        answer_value: option.text,
                        jumpto_question_id: ""
                    });
                }
                // Push default jump to option, it same size to answer_values
                addOption(option, i);
            }
        },
        renderOptionForYesNo: function () {

            var options = [{
                text: "Có",
            }, option_no = {
                text: "Không"
            }];

            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (self.question_model.jumpto_config[i] == null) {
                    self.question_model.jumpto_config.push({
                        answer_index: i,
                        answer_value: i == 0,
                        jumpto_question_id: ""
                    });
                }
                // Push default jump to option, it same size to answer_values
                addOption(option, i);
            }
        },
        renderOptionForOthers: function () {
            var option = {
                text: "Nếu có trả lời"
            };
            if (self.question_model.jumpto_config[0] == undefined) {
                self.question_model.jumpto_config.push({
                    answer_index: i,
                    answer_value: 'any',
                    jumpto_question_id: ""
                });
            }
            // Push default jump to option, it same size to answer_values
            addOption(option, 0);

        }
    }

    switch (this.question_model.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            option_binder.renderOptionForChoice();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
            option_binder.renderOptionForYesNo();
            break;
        default:
            option_binder.renderOptionForOthers();
            break;
    }

    return jumpto_holder;
}
