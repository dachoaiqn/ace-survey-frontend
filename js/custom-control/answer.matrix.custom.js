

var AceMatrixAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AceMatrixAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceMatrixAnswer.prototype.constructor = AceMatrixAnswer;

AceMatrixAnswer.prototype.templateEditMode = function () {
    var self = this,
        array_range = [],
        array_min_answer = [], array_max_answer = [];

    for (var i = 2; i <= 50; i++) {
        array_range.push({
            text: i,
            value: i
        });
    }

    function bindDropdownMinAnswer() {
        array_min_answer = [{
            text: 0,
            value: 0
        }];

        var max = self.question_model.answer_values.filter(function (item) {
            return item != null;
        }).length;
        for (var i = 1; i <= max; i++) {
            array_min_answer.push({
                text: i + ' đáp án',
                value: i
            });
        }

        var latest_index = self.question_model.rating_answer.another_config.min_answer || dropdown_min_answer.getSelectedIndex();
        dropdown_min_answer.setOptions(array_min_answer);

        if (latest_index < max && latest_index > -1) {
            dropdown_min_answer.setSelectedValue(latest_index);
            //   self.question_model.rating_answer.another_config.min_answer = dropdown_min_answer.getSelectedValue();
        }

    }

    function bindDropdownMaxAnswer() {
        // array_max_answer = [{
        //     text: 0,
        //     value: 0
        // }];
        // var latest_max_index = self.question_model.rating_answer.another_config.max_answer || dropdown_max_answer.getSelectedIndex();

        // var min = dropdown_min_answer.getSelectedIndex() > -1 ? dropdown_min_answer.getSelectedIndex() : 1;
        // var max = self.question_model.answer_values.filter(function (item) {
        //     return item != null;
        // }).length;
        // for (var i = min; i <= max; i++) {
        //     array_max_answer.push({
        //         text: i + ' đáp án',
        //         value: i
        //     });
        // }

        // dropdown_max_answer.setOptions(array_max_answer);

        // if (latest_max_index < max && latest_max_index > -1) {
        //     dropdown_max_answer.setSelectedValue(latest_max_index);
        // }
    }


    // Filter the empty option
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length > 0);
    });

    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder");

    var wrapper_choice = new AceSurveyPanel(),
        row_text_range = new AceSurveyPanel(),
        btn_add_option = new AceSurveyMaterialButton('add', ACE_SURVEY_STRINGS.add_answer),
        row_range_picker = new AceSurveyPanel(),
        label_range = new AceSurveyLabel(ACE_SURVEY_STRINGS.range),
        label_min_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.min_answer),
        label_max_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.max_answer),
        dropdown_range = new AceSurveyDropdown(array_range),
        dropdown_min_answer = new AceSurveyDropdown(array_min_answer),
        dropdown_max_answer = new AceSurveyDropdown(array_min_answer),
        txt_text_low = new AceSurveyEdittext(),
        txt_text_medium = new AceSurveyEdittext(),
        txt_text_high = new AceSurveyEdittext(),
        txt_na_text = new AceSurveyEdittext();


    txt_na_text.addClass("txt-na-text");

    txt_text_low.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_text_range_low);
    txt_text_medium.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_text_range_medium);
    txt_text_high.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_text_range_high);
    txt_na_text.setAttribute("placeholder", '...');

    row_range_picker.addClass("row-range");
    row_text_range.addClass('row-text-range');

    btn_add_option.addClass('btn-add-option');
    wrapper_choice.addClass('editor-choice-group');

    row_range_picker.appendChild(label_range);
    row_range_picker.appendChild(dropdown_range);
    row_range_picker.appendChild(label_min_answer);
    row_range_picker.appendChild(dropdown_min_answer);
    // row_range_picker.appendChild(label_max_answer);
    // row_range_picker.appendChild(dropdown_max_answer);

    txt_text_low.addCss({
        'float': 'left'
    });

    txt_text_high.addCss({
        'float': 'right'
    });

    // Disable input the text range 
    // row_text_range.appendChild(txt_text_low);
    // row_text_range.appendChild(txt_text_medium);
    // row_text_range.appendChild(txt_text_high);
    // row_text_range.appendChild(txt_na_text);

    matrix_holder.appendChild(row_range_picker);
    // matrix_holder.appendChild(row_text_range);
    matrix_holder.appendChild(wrapper_choice);
    matrix_holder.appendChild(btn_add_option);
    var track_na_text = this.question_model.rating_answer.na_text;

    var addHeader = function () {
        var row_header = new AceSurveyPanel();
        row_header.addClass('row-option');
        row_header.addClass('row-header');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = dropdown_range.getSelectedValue();
        // var percent_width = 100 / (track_na_text.length > 0 ? (max + 1) : max);
        var percent_width = 100 / (max + 1);
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel(),
                txt_second_label = new AceSurveyEdittext();
            // sub_col.setHtml(i);
            var second_value = self.question_model.rating_answer.second_labels[i - 1];
            if (second_value == undefined) {
                self.question_model.rating_answer.second_labels.push('' + i);
                txt_second_label.setValue(i);
            } else if (typeof second_value === 'string' && second_value.length == 0) {
                txt_second_label.setValue(i);
            } else {
                txt_second_label.setValue(second_value);
            }
            sub_col.addCss({
                width: percent_width + "%"
            })
            sub_col.appendChild(txt_second_label);

            txt_second_label.setAttribute("ace-rating-val", i);
            txt_second_label.addOnTextChanged(function (e) {
                var rating_value = parseInt(e.currentTarget.getAttribute("ace-rating-val"));
                self.question_model.rating_answer.second_labels[rating_value - 1] = e.currentTarget.value.trim();
            });
            txt_second_label.addEventListener("blur", function (e) {
                var rating_value = parseInt(e.currentTarget.getAttribute("ace-rating-val"));
                if (self.question_model.rating_answer.second_labels[rating_value - 1].trim().length === 0) {
                    self.question_model.rating_answer.second_labels[rating_value - 1] = rating_value;
                    e.currentTarget.value = rating_value;
                }
            });

            col_choice.appendChild(sub_col);
        }
        // if (track_na_text.length > 0) {
        var sub_col_na = new AceSurveyPanel(),
            lb_na_text = new AceSurveyLabel(ACE_SURVEY_STRINGS.label_na_text),
            txt_second_na = new AceSurveyEdittext();

        txt_second_na.setAttribute("placeholder", '...');
        lb_na_text.addClass('lb-matrix-na');
        txt_second_na.setValue(track_na_text);
        // txt_second_na.setAttribute('disabled', 'disabled');

        sub_col_na.addCss({
            width: percent_width + "%"
        });
        sub_col_na.appendChild(lb_na_text);
        sub_col_na.appendChild(txt_second_na);
        col_choice.appendChild(sub_col_na);
        // }
        row_header.appendChild(col_choice);
        wrapper_choice.appendChild(row_header);

        txt_second_na.addOnTextChanged(function () {
            self.question_model.rating_answer.na_text = txt_second_na.getValue();
        });
    }

    var addOption = function (choice_values_index) {
    
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = dropdown_range.getSelectedValue() + 1;
        // max = (track_na_text.length > 0 ? (max + 1) : max)
        var percent_width = 100 / max;
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel();
            sub_col.setHtml("<input type='radio' disabled='disabled' name='ace-sv-matrix-grp-" + choice_values_index + "' />");
            sub_col.addCss({
                width: percent_width + "%"
            })
            col_choice.appendChild(sub_col);
        }

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var btn_remove_image = new AceSurveyMaterialButton('clear');

        var txt_answer = new AceSurveyTextArea();
        txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);
        txt_answer.setValue(choice_item.text);

        var btn_add_image = new AceSurveyMaterialButton('add_a_photo');
        btn_add_image.addClass('btn-add-photo');

        var btn_remove = new AceSurveyMaterialButton('clear');
        btn_remove.addClass('btn-remove');
        txt_answer.addClass('txt-answer-option');

        pn_image_answer.appendChild(btn_remove_image);

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(btn_add_image);
        row_option.appendChild(col_choice);
        row_option.appendChild(btn_remove);

        wrapper_choice.appendChild(row_option);

        txt_answer.focus();

        btn_remove.addEventListener("click", function () {
            row_option.destroy();
            self.question_model.answer_values[choice_values_index] = null;
            bindDropdownMinAnswer();
            bindDropdownMaxAnswer();
        });

        txt_answer.addEventListener('keyup', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });

        txt_answer.addEventListener('blur', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });

        btn_add_image.addEventListener('click', function (e) {
            var img_url = self.question_model.answer_values[choice_values_index].image_url;
            new AceImagePickerDialog(img_url)
                .onImagePicker(function (image_url) {
                    pn_image_answer.addClass('show-imv-answer');
                    pn_image_answer.addCss({
                        'background-image': 'url("' + image_url + '")'
                    })
                    self.question_model.answer_values[choice_values_index].image_url = image_url;
                    self.question_model.answer_values[choice_values_index].image_name = ACE_SURVEY_STRINGS.prefix_image_no.replace("##", moment().format('hhmmss_MMDD'));
                });
            return false;
        });

        btn_remove_image.addEventListener('click', function (e) {
            pn_image_answer.removeClass('show-imv-answer');
            self.question_model.answer_values[choice_values_index].image_url = '';
            self.question_model.answer_values[choice_values_index].image_name = '';
            return false;
        });
    }

    dropdown_range.setSelectedValue(self.question_model.rating_answer.end_number);
    dropdown_min_answer.setSelectedValue(self.question_model.rating_answer.another_config.min_answer);


    txt_text_low.setValue(self.question_model.rating_answer.text_range_low);
    txt_text_medium.setValue(self.question_model.rating_answer.text_range_medium);
    txt_text_high.setValue(self.question_model.rating_answer.text_range_high);
    txt_na_text.setValue(self.question_model.rating_answer.na_text);

    addHeader();
    if (self.question_model.answer_values.length == 0) {
        self.question_model.answer_values.push({
            text: '',
            image_url: '',
            image_name: ''
        });
        addOption(0);
    } else {
        // Restore old value if existed
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        })
    }

    bindDropdownMinAnswer();
    bindDropdownMaxAnswer();

    btn_add_option.addEventListener('click', function () {
        self.question_model.answer_values.push({
            text: '',
            image_url: ''
        });
        var idx = self.question_model.answer_values.length - 1;
        addOption(idx);

        bindDropdownMinAnswer();
        bindDropdownMaxAnswer();

        return false;
    });

    txt_text_low.addEventListener('keyup', function () {
        self.question_model.rating_answer.text_range_low = txt_text_low.getValue();
    });

    txt_text_low.addEventListener('blur', function () {
        self.question_model.rating_answer.text_range_low = txt_text_low.getValue();
    });

    txt_text_medium.addEventListener('keyup', function () {
        self.question_model.rating_answer.text_range_medium = txt_text_medium.getValue();
    });

    txt_text_medium.addEventListener('blur', function () {
        self.question_model.rating_answer.text_range_medium = txt_text_medium.getValue();
    });

    txt_text_high.addEventListener('keyup', function () {
        self.question_model.rating_answer.text_range_high = txt_text_high.getValue();
    });

    txt_text_high.addEventListener('blur', function () {
        self.question_model.rating_answer.text_range_high = txt_text_high.getValue();
    });

    function reRenderMatrixUI() {
        wrapper_choice.setHtml("");
        addHeader();
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        });
    }

    txt_na_text.addEventListener('blur', function () {
        if (track_na_text == txt_na_text.getValue()) {
            return;
        }
        track_na_text = txt_na_text.getValue();
        self.question_model.rating_answer.na_text = track_na_text;
        reRenderMatrixUI();
    });

    dropdown_range.addEventListener("change", function () {
        // Reset min answer

        wrapper_choice.setHtml("");
        self.question_model.rating_answer.start_number = 2;
        self.question_model.rating_answer.end_number = dropdown_range.getSelectedValue();
        reRenderMatrixUI();
    });

    dropdown_min_answer.addEventListener('change', function (e) {
        self.question_model.rating_answer.another_config.min_answer = dropdown_min_answer.getSelectedValue();
        bindDropdownMaxAnswer();
    });

    // dropdown_max_answer.addEventListener('change', function (e) {
    //     self.question_model.rating_answer.another_config.max_answer = dropdown_max_answer.getSelectedValue();
    // });

    return matrix_holder;
}

AceMatrixAnswer.prototype.templateViewMode = function () {
    var self = this, header_answer_texts = [];
    // set default answer as Object
    this.question_model.user_answer = {};
    // Filter the empty option { text: 'string', image_url: 'string'}
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length);
    })

    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder");

    var wrapper_choice = new AceSurveyPanel(),
        row_text_range = new AceSurveyPanel(),
        txt_text_low = new AceSurveyLabel(self.question_model.rating_answer.text_range_low),
        txt_text_medium = new AceSurveyLabel(self.question_model.rating_answer.text_range_medium),
        txt_text_high = new AceSurveyLabel(self.question_model.rating_answer.text_range_high),
        lb_require_answer = new AceSurveyLabel('');

    lb_require_answer.addClass('lb-min-answer');

    var text_require = '';
    if (self.question_model.rating_answer.another_config.min_answer > 0) {
        text_require = 'Trả lời tối thiếu ' + self.question_model.rating_answer.another_config.min_answer + ' đáp án';
    }

    // if (self.question_model.rating_answer.another_config.max_answer > 0 && text_require.length == 0) {
    //     text_require = 'Trả lời tối đa ' + self.question_model.rating_answer.another_config.max_answer + ' đáp án';
    // } else if (self.question_model.rating_answer.another_config.max_answer > 0) {
    //     text_require += ', tối đa ' + self.question_model.rating_answer.another_config.max_answer + ' đáp án';
    // }

    if (text_require.length > 0) {
        lb_require_answer.setText(text_require);
        lb_require_answer.show();
    } else {
        lb_require_answer.setText('');
        lb_require_answer.hide();
    }

    row_text_range.addClass('row-text-range');

    wrapper_choice.addClass('editor-choice-group');

    txt_text_low.addCss({
        'float': 'left'
    });

    txt_text_high.addCss({
        'float': 'right'
    });

    row_text_range.appendChild(txt_text_low);
    row_text_range.appendChild(txt_text_medium);
    row_text_range.appendChild(txt_text_high);
    row_text_range.appendChild(lb_require_answer);

    matrix_holder.appendChild(row_text_range);
    matrix_holder.appendChild(wrapper_choice);
    var track_na_text = this.question_model.rating_answer.na_text;

    var addHeader = function () {
        header_answer_texts = [];
        var row_header = new AceSurveyPanel();
        row_header.addClass('row-option');
        row_header.addClass('row-header');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = self.question_model.rating_answer.end_number;
        var percent_width = 100 / (track_na_text.length > 0 ? (max + 1) : max);
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel();
            sub_col.setHtml(self.question_model.rating_answer.second_labels[i - 1] || i);
            sub_col.addCss({
                width: percent_width + "%"
            })
            col_choice.appendChild(sub_col);
            header_answer_texts.push(i);
        }
        if (track_na_text.length > 0) {
            var sub_col = new AceSurveyPanel()
            sub_col.setHtml(track_na_text);
            sub_col.addCss({
                width: percent_width + "%",
                'padding': '5px',
            })
            col_choice.appendChild(sub_col);
            header_answer_texts.push(track_na_text);

        }
        row_header.appendChild(col_choice);
        wrapper_choice.appendChild(row_header);
    }

    var onRadioChanged = function (e) {
        var answer_index = parseInt(e.currentTarget.getAttribute('choice-row-index')),
            col_index = parseInt(e.currentTarget.getAttribute('choice-col-index'));
        var choice_item = self.question_model.answer_values[answer_index];
        var value = choice_item.text;
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            // If the image is existing in answer then get the image name to append to the answer 
            value += ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + choice_item.image_name;
        }


        self.question_model.user_answer["k_" + answer_index] = {
            Answer: col_index + 1,
            Question: value
        }
    }

    var addOption = function (choice_values_index) {
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');
        var max = self.question_model.rating_answer.end_number;
        max = (track_na_text.length > 0 ? (max + 1) : max)
        var percent_width = 100 / max;
        for (var i = 1; i <= max; i++) {
            var sub_col = new AceSurveyPanel();
            var radio = new AceSurveyRadio();
            radio.setAttribute('name', "sv-" + self.question_model.id + "-" + choice_values_index);
            radio.setAttribute('choice-row-index', choice_values_index);
            radio.setAttribute('choice-col-index', i - 1);
            sub_col.appendChild(radio);
            sub_col.addCss({
                width: percent_width + "%"
            })
            col_choice.appendChild(sub_col);

            var latest_radio = null;

            radio.addEventListener("change", function (e) {
                onRadioChanged(e);
                latest_radio = e.currentTarget;
            });
            radio.addEventListener("click", function (e) {
                if (e.currentTarget.checked && e.currentTarget == latest_radio) {
                    e.currentTarget.checked = false;
                    var answer_index = parseInt(e.currentTarget.getAttribute('choice-row-index'));
                    delete self.question_model.user_answer["k_" + answer_index];
                }
            });
        }


        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var txt_answer = new AceSurveyLabel(choice_item.text);
        txt_answer.addClass('txt-answer-option');
        if (choice_item.text.length == 0) {
            txt_answer.hide();
        }

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(col_choice);
        wrapper_choice.appendChild(row_option);
    }

    addHeader();

    self.question_model.answer_values.forEach(function (value, index) {
        addOption(index);
    });

    return matrix_holder;
}

AceMatrixAnswer.prototype.templateGraphMode = function () {
    var self = this;
    // set default answer as Object
    this.question_model.user_answer = {};
    // Filter the empty option { text: 'string', image_url: 'string'}
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length);
    });

    var graph_data = this.question_model.toGraphData();

    var matrix_holder = new AceSurveyPanel();
    matrix_holder.addClass("ace-matrix-quest-holder graph-mode");

    var wrapper_choice = new AceSurveyPanel();
    var lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', this.question_model.totalAnswer()));
    lb_total_answer.addClass("lb-total-answer");

    wrapper_choice.addClass('editor-choice-group');

    matrix_holder.appendChild(lb_total_answer);
    matrix_holder.appendChild(wrapper_choice);
    var track_na_text = this.question_model.rating_answer.na_text;


    var addOption = function (choice_values_index) {
        // ["ANSWER_STRING", number,number,number...] - number is count of user choice
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var col_choice = new AceSurveyPanel();
        col_choice.addClass('col-choice');

        var max = self.question_model.rating_answer.end_number;
        //    max = (track_na_text.length > 0 ? (max + 1) : max)
        var graph_labels = [], graph_values = graph_data[choice_values_index];
        graph_values.shift();
        for (var i = 1; i <= max; i++) {
            graph_labels.push(self.question_model.rating_answer.second_labels[i - 1] || i);
        }
        if (track_na_text.length > 0) {
            graph_labels.push(track_na_text);
        }
        var chart_option = {
            type: 'bar',
            data: {
                labels: graph_labels,
                datasets: [{
                    data: graph_values,
                    backgroundColor: ACE_CHART_COLOR.backgroundColor,
                    borderColor: ACE_CHART_COLOR.borderColor,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
            }
        }
        var view_chart = new AceSurveyChart(chart_option);

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item && choice_item.image_url && choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var txt_answer = new AceSurveyLabel(choice_item.text);
        txt_answer.addClass('txt-answer-option');
        if (choice_item.text.length == 0) {
            txt_answer.hide();
        }

        col_choice.appendChild(view_chart);
        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        row_option.appendChild(col_choice);
        wrapper_choice.appendChild(row_option);

    }

    graph_data.forEach(function (value, index) {
        addOption(index);
    })

    lb_total_answer.addEventListener('click', function () {
        new AceAnswerListDialog(self.question_model);
    });

    return matrix_holder;
}