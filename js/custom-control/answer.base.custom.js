



/**
 * 
 * @param {QuestionModel} question_model 
 * @param {*} render_mode 
 */
var AceBaseAnswer = function (question_model, render_mode) {
    this.render_mode = render_mode != undefined ? render_mode : false;
    this.question_model = question_model;
    this._all_questions = [];
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceBaseAnswer.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceBaseAnswer.prototype.constructor = AceBaseAnswer;


AceBaseAnswer.prototype.isNewQuestion = function () {
    return this.question_model.isNewModel();
}

AceBaseAnswer.prototype.template = function () {
    var self = this;
    switch (this.render_mode) {
        case ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.EDIT:
            var tempate = this.templateEditMode();
            return tempate;
        case ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.RESPONSE:
            return this.templateViewMode();
        case ACE_SURVEY_CONSTANT.QUESTION_RENDER_MODE.GRAPH:
            var tempate = this.templateGraphMode();
            if (tempate == null) {
                return this.templateGraphModeDefault();
            }
            return tempate;
    }
}

AceBaseAnswer.prototype.templateEditMode = function () {

}

AceBaseAnswer.prototype.templateViewMode = function () {

}


/**
 * Using on Jump to feature, filter all question that have same section, require question.
 * @param {[QuestionModel]} all_questions 
//  */
// AceBaseAnswer.prototype.setAllQuestions = function (all_questions) {
//     this._all_questions = all_questions;
//     console.log("_all_questions", this._all_questions);
//     if (this.isNewQuestion() == false && this._all_questions.length > 0) {
//         // If not new question show jump to feature
//         var temp_jumpto = this.templateJumpTo();
//         console.log(temp_jumpto);
//         if (temp_jumpto != null) {
//             this.appendChild(temp_jumpto);
//         }
//     }
// }

AceBaseAnswer.prototype.templateGraphMode = function () {

}


AceBaseAnswer.prototype.templateJumpTo = function () {



    var self = this;

    var question_index = AceSurveyUtility.Array(self._all_questions).findIndex(function (quest) {
        return quest.id == self.question_model.id;
    });

    var questions_can_jump_to = self._all_questions.slice(question_index + 1);


    var section_filter = [], next_section, question_same_section = [], has_next_section = false;

    for (var i = 0; i < questions_can_jump_to.length; i++) {
        var q = questions_can_jump_to[i];
        if (has_next_section == false && q.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
            question_same_section.push(q);
        }
        if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
            has_next_section = true;
            if (next_section != null) {
                section_filter.push(next_section);
            }
            next_section = {
                question: q,
                index: i,
                questions_child: [],
            }
        } else if (q.is_require == false && next_section) {
            next_section.questions_child.push(q);
        } else if (q.is_require == true) {
            // Clear next question if have any child that require answer
            next_section = null;
            break;
        }

    }
    if (next_section) {
        section_filter.push(next_section);
    }

    // Convert Section (Page separator) to option
    var options_jumpto = section_filter.map(function (item) {
        return {
            text: item.question.title,
            value: item.question.id
        }
    });

    // Merger the question that same section to this
    options_jumpto = question_same_section
        .map(function (item) {
            return {
                text: item.title,
                value: item.id
            }
        }).concat(options_jumpto);

    options_jumpto.unshift({
        text: "Không",
        value: ""
    })

    // Not in question list
    if (question_index == -1) {
        return;
    }

    var jumpto_holder = new AceSurveyPanel();

    var lb_jumto = new AceSurveyLabel("Jump to");
    lb_jumto.addClass("lb-jumpto");

    jumpto_holder.appendChild(lb_jumto);
    jumpto_holder.addClass('pn-jumpto-config');

    var addOption = function (answer_option, index) {

        // "answer_option" structure: { text: '',  image_url: '',  image_name: ''}
        var row_jumpto_option = new AceSurveyPanel();
        row_jumpto_option.addClass('row-jumpto');

        var lb_answer = new AceSurveyLabel(answer_option.text);
        lb_answer.addClass('lb-answer-need-jumpto');

        var dropdown_jumpto = new AceSurveyDropdown(options_jumpto);
        dropdown_jumpto.addClass('dropdown-jumpto-question');
        dropdown_jumpto.setSelectedValue(
            self.question_model.jumpto_config[index].jumpto_question_id
        )

        row_jumpto_option.appendChild(lb_answer);
        row_jumpto_option.appendChild(dropdown_jumpto);

        row_jumpto_option.addEventListener('change', function () {
            self.question_model.jumpto_config[index].jumpto_question_id = dropdown_jumpto.getSelectedValue();
        });

        jumpto_holder.appendChild(row_jumpto_option);
    }.bind(this);


    // Used to render the option for each question type
    var option_binder = {
        renderOptionForChoice: function () {
            for (var i = 0; i < this.question_model.answer_values.length; i++) {
                var option = this.question_model.answer_values[i];
                if (this.question_model.jumpto_config[i] == null) {
                    this.question_model.jumpto_config.push({
                        answer_index: i,
                        answer_value: option.text,
                        jumpto_question_id: ""
                    });
                }
                // Push default jump to option, it same size to answer_values
                addOption(option, i);
            }
        },
        renderOptionForYesNo: function () {

            var options = [{
                text: "Có",
            }, option_no = {
                text: "Không"
            }];

            for (var i = 0; i < options.length; i++) {
                var option = options[i];
                if (this.question_model.jumpto_config[i] == null) {
                    this.question_model.jumpto_config.push({
                        answer_index: i,
                        answer_value: option.text,
                        jumpto_question_id: ""
                    });
                }
                // Push default jump to option, it same size to answer_values
                addOption(option, i);
            }
        }
    }



    switch (this.question_model.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            option_binder.renderOptionForChoice();
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
            
            option_binder.renderOptionForYesNo();
            break;


    }


    return jumpto_holder;
}

AceBaseAnswer.prototype.templateGraphModeDefault = function () {
    var self = this;
    var holder = new AceSurveyPanel();
    holder.addClass("pn-graph");

    var lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', this.question_model.totalAnswer()));
    lb_total_answer.addClass("lb-total-answer");

    holder.appendChild(lb_total_answer);
    var is_showing = false;
    lb_total_answer.addEventListener('click', function () {
        // new AceAnswerListDialog(self.question_model);
        is_showing = !is_showing;
        self.toggleUserAnswer(is_showing);
    });

    return holder;
}

AceBaseAnswer.prototype.toggleUserAnswer = function (flag) {
    if (flag == false) {
        this.table_container.destroy();
        this.table_container = null;
        return;
    }
    var self = this;
    var table_container = new AceSurveyPanel();
    this.table_container = table_container;
    table_container.addClass('tbl-container');

    var html_table_result = "<table class='ace-tbl-user-poll' >";
    var user_polls = self.question_model.toUserPollList();
    // Display user name in response list
    if (ACE_SV_GLOBAL_VALUE.survey_basic_info.ShowUser) {
        html_table_result = user_polls.reduce(function (output, item) {
            var html_answer = AceSurveyUtility.convertUserAnswerToHtml(self.question_model, item.answer, ACE_SURVEY_STRINGS.empty_answer);
            output += '<tr><td>' + item.user.full_name + '</td><td>' + html_answer + '</td></tr>';
            return output;
        }, html_table_result);
    } else {
        html_table_result = user_polls.reduce(function (output, item) {
            var html_answer = AceSurveyUtility.convertUserAnswerToHtml(self.question_model, item.answer, ACE_SURVEY_STRINGS.empty_answer);
            output += '<tr><td>' + html_answer + '</td></tr>';
            return output;
        }, html_table_result);
    }

    html_table_result += '</table>';
    table_container.setHtml(html_table_result);

    this.appendChild(table_container);

}

AceBaseAnswer.prototype.validate = function () {

}

AceBaseAnswer.prototype.getQuestionModel = function () {
    return this.question_model;
}


