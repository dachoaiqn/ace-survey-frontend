

/**
 * 
 * @param {QuestionModel} question_model 
 * @param {*} render_mode 
 */
var AceChoiceAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AceChoiceAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceChoiceAnswer.prototype.constructor = AceChoiceAnswer;

AceChoiceAnswer.prototype.templateEditMode = function () {

    // Filter the empty option
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length > 0);
    });

    var self = this,
        holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel(),
        btn_add_option = new AceSurveyMaterialButton('add', ACE_SURVEY_STRINGS.add_answer);
    btn_add_option.addClass('btn-add-option');
    wrapper_choice.addClass('editor-choice-group');
    holder.addClass("ace-choice-quest-holder");
    //  self.question_model.answer_values = self.question_model.answer_values;
    self.question_model.answer_values = self.question_model.answer_values.filter(function (item) {
        return item != null;
    });

    holder.appendChild(wrapper_choice);
    holder.appendChild(btn_add_option);

    var addOption = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");
        if (choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        var btn_remove_image = new AceSurveyMaterialButton('clear');

        var txt_answer = new AceSurveyEdittext();
        txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);

        var btn_add_image = new AceSurveyMaterialButton('add_a_photo');
        btn_add_image.addClass('btn-add-photo');

        var btn_remove = new AceSurveyMaterialButton('clear');
        txt_answer.addClass('txt-answer-option');
        txt_answer.setValue(choice_item.text);

        pn_image_answer.appendChild(btn_remove_image);

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(txt_answer);
        // Add button to choose image if the question type is not drop-down type
        if (self.question_model.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN) {
            row_option.appendChild(btn_add_image);
        }
        row_option.appendChild(btn_remove);
        wrapper_choice.appendChild(row_option);

        txt_answer.focus();

        btn_remove.addEventListener("click", function () {
            row_option.destroy();
            self.question_model.answer_values[choice_values_index] = null;
        });
        txt_answer.addEventListener('keyup', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });
        txt_answer.addEventListener('blur', function (e) {
            self.question_model.answer_values[choice_values_index].text = txt_answer.getValue();
        });

        btn_add_image.addEventListener('click', function (e) {
            var img_url = self.question_model.answer_values[choice_values_index].image_url;
            new AceImagePickerDialog(img_url)
                .onImagePicker(function (image_url) {
                    pn_image_answer.addClass('show-imv-answer');
                    pn_image_answer.addCss({
                        'background-image': 'url("' + image_url + '")'
                    })
                    self.question_model.answer_values[choice_values_index].image_url = image_url;
                    // Random a name of image
                    self.question_model.answer_values[choice_values_index].image_name = ACE_SURVEY_STRINGS.prefix_image_no.replace("##", moment().format('hhmmss_MMDD'));
                    console.log(self.question_model.answer_values);
                });
            return false;
        });

        btn_remove_image.addEventListener('click', function (e) {
            pn_image_answer.removeClass('show-imv-answer');
            self.question_model.answer_values[choice_values_index].image_url = '';
            self.question_model.answer_values[choice_values_index].image_name = '';
            return false;
        });

    }

    if (self.question_model.answer_values.length == 0) {
        self.question_model.answer_values.push({
            text: '',
            image_url: '',
            image_name: ''
        });
        addOption(0);
    } else {
        // Restore old value if existed
        self.question_model.answer_values.forEach(function (value, index) {
            addOption(index);
        })
    }

    btn_add_option.addEventListener('click', function () {
        self.question_model.answer_values.push({
            text: '',
            image_url: ''
        });
        var idx = self.question_model.answer_values.length - 1;
        addOption(idx);
        return false;
    })
    return holder;
}

AceChoiceAnswer.prototype.templateViewMode = function () {
    // Filter the empty option
    this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
        return item != null && (item.text.length > 0 || item.image_url.length > 0);
    });

    if (this.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE) {
        this.question_model.user_answer = [];
    }
    var self = this,
        holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel();

    wrapper_choice.addClass('editor-choice-group');
    holder.addClass("ace-choice-quest-holder");


    holder.appendChild(wrapper_choice);

    var addOptionForRadioAndCheckbox = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');
        var choice_button;
        switch (self.question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
                choice_button = new AceSurveyRadio();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                choice_button = new AceSurveyCheckbox();
                break;

        }

        choice_button.setAttribute("name", 'group_' + self.question_model.id);

        row_option.appendChild(choice_button);

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");

        var lb_answer = new AceSurveyLabel(choice_item.text);
        lb_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);
        lb_answer.addClass('txt-answer-option');

        if (choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(lb_answer);

        wrapper_choice.appendChild(row_option);
        choice_button.addEventListener('change', function (e) {
            var value = choice_item.text;
            if (choice_item.image_url.length > 0) {
                // If the image is existing in answer then get the image name to append to the answer 
                value += ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + choice_item.image_name;
            }
            if (e.currentTarget.checked == true && self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE) {
                self.question_model.user_answer = value;
            } else if (e.currentTarget.checked == true && self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE) {
                self.question_model.user_answer.push(value)
            } else if (e.currentTarget.checked == false && self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE) {
                var idx = self.question_model.user_answer.indexOf(value);
                self.question_model.user_answer.splice(idx, 1);
            }
        });
    }

    // IF QUESTION TYPE IS DROPDOWN OR CHECKBOX
    if (self.question_model.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN) {
        self.question_model.answer_values.forEach(function (value, index) {
            addOptionForRadioAndCheckbox(index);
        })
    } else {
        // IF QUESTION TYPE IS DROPDOWN
        var option_sources = self.question_model.answer_values.map(function (item) {
            return {
                text: item.text,
                value: item.text
            }
        });
        var choice_button = new AceSurveyDropdown(option_sources);
        choice_button.addEventListener('change', function (e) {
            self.question_model.user_answer = choice_button.getSelectedValue();
        });
        wrapper_choice.appendChild(choice_button);
    }
    return holder;
}

AceChoiceAnswer.prototype.templateGraphMode = function () {
    // Filter the empty option
    // this.question_model.answer_values = this.question_model.answer_values.filter(function (item) {
    //     return item != null && (item.text.length > 0 || item.image_url.length > 0);
    // });
    var self = this;
    // Convert poll data to chartjs lib
    var graph_data = this.question_model.toGraphData();
    var graph_labels = [], graph_values = [];
    var image_count = 0;
    graph_data.reduce(function (output, array_values, i) {
        var label = array_values.shift();
        if (self.question_model.answer_values[i].text.length > 0 && label.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
            label = label.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME)[0];
        } else if (self.question_model.answer_values[i].text.length == 0 && label.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
            // If the option is only image then generate a image name to add to label in chart
            image_count++;
            label = self.question_model.answer_values[i].image_name;
            self.question_model.answer_values[i].text = label;
        }
        output[0].push(label);
        output[1].push(array_values[0]);
        return output;
    }, [graph_labels, graph_values]);

    var chart_option = {
        type: 'doughnut',
        data: {
            labels: graph_labels,
            datasets: [{
                data: graph_values,
                backgroundColor: ACE_CHART_COLOR.backgroundColor,
                borderColor: ACE_CHART_COLOR.borderColor,
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                position: 'left'
            },
        }
    }

    var holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel(),
        wrapper_graph = new AceSurveyPanel(),
        lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', self.question_model.totalAnswer())),
        view_graph = new AceSurveyChart(chart_option);


    wrapper_choice.addClass('editor-choice-group');
    wrapper_graph.addClass('pn-graph');
    holder.addClass("ace-choice-quest-holder");
    lb_total_answer.addClass("lb-total-answer");

    self.question_model.answer_values = self.question_model.answer_values.filter(function (item) {
        return item != null;
    });

    wrapper_graph.appendChild(lb_total_answer);
    wrapper_graph.appendChild(view_graph);
    holder.appendChild(wrapper_choice);
    holder.appendChild(wrapper_graph);


    lb_total_answer.addEventListener('click', function () {
        new AceAnswerListDialog(self.question_model);
    });

    var addOptionForRadioAndCheckbox = function (choice_values_index) {
        // {text: ANSWER_AS_TEXT, image_url: ANSWRER_AS_IMAGE}
        var choice_item = self.question_model.answer_values[choice_values_index];
        var row_option = new AceSurveyPanel();
        row_option.addClass('row-option');
        var choice_button;

        switch (self.question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
                choice_button = new AceSurveyRadio();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                choice_button = new AceSurveyCheckbox();
                break;
        }

        choice_button.setAttribute("name", 'group_' + self.question_model.id);
        choice_button.setAttribute("disabled", 'disabled');
        row_option.appendChild(choice_button);

        var pn_image_answer = new AceSurveyPanel();
        pn_image_answer.addClass("imv-image-answer");

        var lb_answer = new AceSurveyLabel(choice_item.text);
        lb_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_enter_answer);
        lb_answer.addClass('txt-answer-option');

        if (choice_item.image_url.length > 0) {
            pn_image_answer.addClass('show-imv-answer');
            pn_image_answer.addCss({
                'background-image': 'url("' + choice_item.image_url + '")'
            })
        }

        row_option.appendChild(pn_image_answer);
        row_option.appendChild(lb_answer);

        wrapper_choice.appendChild(row_option);
    }

    // IF QUESTION TYPE IS DROPDOWN OR CHECKBOX
    if (self.question_model.question_type != ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN) {
        self.question_model.answer_values.forEach(function (value, index) {
            addOptionForRadioAndCheckbox(index);
        })
    } else {
        // IF QUESTION TYPE IS DROPDOWN
        var option_sources = self.question_model.answer_values.map(function (item) {
            return {
                text: item.text,
                value: item.text
            }
        });
        var choice_button = new AceSurveyDropdown(option_sources);
        choice_button.addEventListener('change', function (e) {
            self.question_model.user_answer = value;
        });
        wrapper_choice.appendChild(choice_button);
    }

    return holder;
}
