

var AceTextAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all BaseAceSurveyWidget method 
AceTextAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceTextAnswer.prototype.constructor = AceTextAnswer;

AceTextAnswer.prototype.templateEditMode = function () {

    var self = this,
        holder = new AceSurveyPanel();

    return holder;
}

AceTextAnswer.prototype.templateViewMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        txt_answer;

    holder.addClass("ace-text-answer-holder");

    if (self.question_model.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT) {
        txt_answer = new AceSurveyTextArea();
    } else {
        txt_answer = new AceSurveyEdittext();
    }
    txt_answer.addClass('txt-user-answer');
    txt_answer.setAttribute("placeholder", ACE_SURVEY_STRINGS.hint_type_your_answer);

    txt_answer.addOnTextChanged(function (e) {
        self.question_model.user_answer = e.currentTarget.value.trim();
    });

    holder.appendChild(txt_answer);
    return holder;
}

