

var AceYesNoAnswer = function (question_model, render_mode) {
    AceBaseAnswer.call(this, question_model, render_mode);
}

// Inherit all AceBaseAnswer method 
AceYesNoAnswer.prototype = Object.create(AceBaseAnswer.prototype);
// Override constructor not use Parent
AceYesNoAnswer.prototype.constructor = AceYesNoAnswer;

AceYesNoAnswer.prototype.templateEditMode = function () {
    var self = this,
        holder = new AceSurveyPanel();

    return holder;
}

AceYesNoAnswer.prototype.templateViewMode = function () {

    var self = this,
        holder = new AceSurveyPanel(),
        row_yes = new AceSurveyPanel(),
        row_no = new AceSurveyPanel(),
        radio_yes = new AceSurveyRadio(),
        label_yes = new AceSurveyLabel(ACE_SURVEY_STRINGS.label_yes_answer),
        radio_no = new AceSurveyRadio(),
        label_no = new AceSurveyLabel(ACE_SURVEY_STRINGS.label_no_answer);

    radio_yes.setAttribute('name', this.question_model.id);
    radio_no.setAttribute('name', this.question_model.id);

    holder.addClass("ace-yes-no-answer-holder");

    row_yes.addClass('row-option');
    row_no.addClass('row-option');

    row_yes.appendChild(radio_yes);
    row_yes.appendChild(label_yes);

    row_no.appendChild(radio_no);
    row_no.appendChild(label_no);

    holder.appendChild(row_yes);
    holder.appendChild(row_no);

    radio_yes.addEventListener('change', function () {
        self.question_model.user_answer = true;
    });

    radio_no.addEventListener('change', function () {
        self.question_model.user_answer = false;
    });

    return holder;
}


AceYesNoAnswer.prototype.templateGraphMode = function () {

    var self = this;
    // Convert poll data to chartjs lib
    // graph data is array have 2 items. [0,0] => [0] Yes answer, [1] = No anser
    var graph_data = this.question_model.toGraphData();


    var chart_option = {
        type: 'doughnut',
        data: {
            labels: [ACE_SURVEY_STRINGS.label_yes_answer, ACE_SURVEY_STRINGS.label_no_answer],
            datasets: [{
                data: graph_data,
                backgroundColor: ACE_CHART_COLOR.backgroundColor,
                borderColor: ACE_CHART_COLOR.borderColor,
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                position: 'left'
            },
        }
    }

    var holder = new AceSurveyPanel(),
        wrapper_choice = new AceSurveyPanel(),
        wrapper_graph = new AceSurveyPanel(),
        lb_total_answer = new AceSurveyLabel(ACE_SURVEY_STRINGS.total_answer.replace('##', self.question_model.totalAnswer())),
        view_graph = new AceSurveyChart(chart_option);


    wrapper_choice.addClass('editor-choice-group');
    wrapper_graph.addClass('pn-graph');
    holder.addClass("ace-choice-quest-holder");
    lb_total_answer.addClass("lb-total-answer");

    wrapper_graph.appendChild(lb_total_answer);
    wrapper_graph.appendChild(view_graph);
    holder.appendChild(wrapper_choice);
    holder.appendChild(wrapper_graph);


    lb_total_answer.addEventListener('click', function () {
        new AceAnswerListDialog(self.question_model);
    });

    return holder;
}
