

var QuestionModel = function () {
    BaseModel.call(this);
    this.title = "";
    this.question_no = 1;
    this.internal_name = "";
    this.sp_original_data = null;
    this.schema_xml = null;
    this.answer_values = [];
    this.jumpto_config = [];
    this.rating_answer = {
        start_number: 2,
        end_number: 2,
        text_range_low: "",
        text_range_medium: "",
        text_range_high: "",
        na_text: "",
        second_labels: [],
        another_config: {
            min_answer: 0,
            max_answer: 0,
            min_rate: 0,
            max_rate: 0
        }
    }
    this.people_values = {
        is_people_and_group: false
    }
    this.currency_values = {
        location_id: 1066 // Default vietnam format
    }
    this.total_answer = 0;
    this.user_answer = null;
    // Default Single line
    this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT;
    this.is_require = false;

    this.poll_result = [];
    this.user_polls = [];
}

// Override all BaseModel method 
QuestionModel.prototype = Object.create(BaseModel.prototype);
QuestionModel.prototype.constructor = QuestionModel;

QuestionModel.prototype.fromJSON = function (rest_model, index) {
    this.sp_original_data = rest_model;
    this.question_no = index != undefined ? (index + 1) : 0;
    this.title = rest_model.Title;
    this.question_type = rest_model.FieldTypeKind;
    this.id = rest_model.Id;
    this.is_require = rest_model.Required;
    this.internal_name = rest_model.EntityPropertyName;

    /// JUMP TO PARSER
    if (typeof rest_model.Description === 'string' && rest_model.Description.length > 0) {
        switch (this.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
                this.jumpto_config = JSON.parse(rest_model.Description);

                break;
            default:
                var split = rest_model.Description.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_JUMP_TO_CONFIG);
                if (split.length >= 2) {
                    this.jumpto_config = JSON.parse(split[1]);
                    // Update Description after trim jumpto config
                    rest_model.Description = split[0];
                }
                break;
        }
    }

    if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT) {
        // Check is matrix text type;
        if (rest_model.Description && rest_model.Description.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_TEXT_OPTIONS) > -1) {
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT;
            var split_options = rest_model.Description.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_TEXT_OPTIONS);
            this.answer_values = JSON.parse(split_options[1]);
        }
        else if (rest_model.Description && rest_model.Description.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_TEXT_OPTIONS) > -1) {
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT;
            // Format
            // var _meta = {
            //     rating_answer: this.rating_answer,
            //     answer_values: this.answer_values
            // }
            var split_options = rest_model.Description.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_TEXT_OPTIONS);
            var _meta = JSON.parse(split_options[1]);
            this.rating_answer = _meta.rating_answer;
            this.answer_values = _meta.answer_values;

            // Init default min answer if not exist
            if (this.rating_answer.another_config == undefined) {
                this.rating_answer.another_config = {
                    min_answer: 0,
                    min_answer: 0,
                    min_rate: 0,
                    max_rate: 0
                }
            }
        }
        else if (rest_model.Description && rest_model.Description.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_MULTIPLE_CHOICE_OPTIONS) > -1) {
            // MATRIX CHOICE MULTIPLE SUB QUESTION
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE;
            // Format
            // var _meta = {
            //     rating_answer: this.rating_answer,
            //     answer_values: this.answer_values
            // }
            var split_options = rest_model.Description.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_MULTIPLE_CHOICE_OPTIONS);
            var _meta = JSON.parse(split_options[1]);
            this.rating_answer = _meta.rating_answer;
            this.answer_values = _meta.answer_values;
            // console.log("this.answer_values", this.answer_values);

            // this.answer_values.forEach(function (value) {
            //     value.second_questions = value.second_questions.map(function (item, index) {
            //         if (item.trim() === "") {
            //             return (index + 1) + "";
            //         }
            //         return item
            //     })
            // })

        }
        else if (rest_model.Description && rest_model.Description.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_MULTILE_TEXT_OPTIONS) > -1) {
            // MATRIX CHOICE MULTIPLE SUB QUESTION
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT;
            // Format
            // var _meta = {
            //     rating_answer: this.rating_answer,
            //     answer_values: this.answer_values
            // }
            var split_options = rest_model.Description.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_MULTILE_TEXT_OPTIONS);
            var _meta = JSON.parse(split_options[1]);
            this.rating_answer = _meta.rating_answer;
            this.answer_values = _meta.answer_values;
        }
    } else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER) {
        this.internal_name = rest_model.EntityPropertyName;
        if (rest_model.SelectionMode == 1) {
            this.people_values.is_people_and_group = true;
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP;
        } else {
            this.people_values.is_people_and_group = false;
            this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER;
        }
    } else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY) {
        this.currency_values.location_id = rest_model.CurrencyLocaleId;
    } else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX) {
        this.rating_answer.end_number = rest_model.GridEndNumber;
        this.rating_answer.start_number = rest_model.GridStartNumber;
        this.rating_answer.text_range_medium = rest_model.GridTextRangeAverage;
        this.rating_answer.text_range_high = rest_model.GridTextRangeHigh;
        this.rating_answer.text_range_low = rest_model.GridTextRangeLow;

        if (typeof rest_model.Description && rest_model.Description.length > 0) {
            try {
                this.rating_answer.another_config = JSON.parse(rest_model.Description);
            } catch (err) {

            }
        }

        var str_convert_rating = rest_model.GridNAOptionText;
        if (str_convert_rating != null && str_convert_rating.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_NA_AND_SECOND_VALUE) > -1) {
            // Encode second rating value
            str_convert_rating = str_convert_rating.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_NA_AND_SECOND_VALUE)
            this.rating_answer.na_text = str_convert_rating[0];
            this.rating_answer.second_labels = str_convert_rating[1].split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_SECOND_RATING_VALUE);
        } else {
            this.rating_answer.na_text = str_convert_rating === null ? "" : str_convert_rating;
            this.rating_answer.second_labels = [];
        }
    }


    if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE && rest_model.EditFormat == 0) {
        this.question_type = ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN;
        if (typeof rest_model.Description == 'string' && rest_model.Description.length > 0)
            this.jumpto_config = JSON.parse(rest_model.Description);
    }


    if (rest_model.Choices) {
        // Convert Answer in SP to in app choice 
        this.answer_values = rest_model.Choices.results.map(function (str_answer) {
            /// this object is valid in Choice answer and Matix answer - it allow user choose an image to show in answer
            return AceSurveyUtility.convertChoiceAnswerStringToObject(str_answer);
        });
    }



    this.saveState();
    return this;
}

QuestionModel.prototype.IsRatingRequire = function () {
    return this.rating_answer.another_config.min_rate > 0
        || this.rating_answer.another_config.min_answer > 0;
}

QuestionModel.prototype.toJSON = function () {
    var results = {
        FieldTypeKind: this.question_type,
        Title: this.title,
        Required: this.is_require
    }
    switch (this.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT:
            results.__metadata = { type: 'SP.FieldText' };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT:
            results.__metadata = { type: 'SP.FieldMultiLineText' };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
            results.__metadata = { type: 'SP.FieldNumber' };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
            results.__metadata = { type: 'SP.FieldUser' };
            results.SelectionMode = 0;
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
            // Set FieldTypeKind = 20 as People field 
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER;
            results.__metadata = { type: 'SP.FieldUser' };
            results.SelectionMode = 1;
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY:
            results.__metadata = { type: 'SP.FieldCurrency' };
            results.CurrencyLocaleId = this.currency_values.location_id;
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            results.__metadata = { type: 'SP.FieldChoice' };
            results.Choices = {
                results: this.answer_values.map(AceSurveyUtility.convertChoiceAnswerObjectToString)
            };
            results.EditFormat = 1;
            results.Description = JSON.stringify(this.jumpto_config);
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE;
            results.__metadata = { type: 'SP.FieldChoice' };
            results.Choices = {
                results: this.answer_values.map(function (item) {
                    return item.text;
                })
            };
            results.EditFormat = 0;
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
            results.__metadata = { type: 'SP.FieldMultiChoice' };
            results.Choices = {
                results: this.answer_values.map(AceSurveyUtility.convertChoiceAnswerObjectToString)
            };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME:
            results.__metadata = { type: 'SP.FieldDateTime' };
            break
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            results.__metadata = { type: 'SP.FieldRatingScale' };
            results.GridEndNumber = this.rating_answer.end_number;
            // Convert string value to string and add it to GridNAOptionText to store
            results.GridNAOptionText = this.rating_answer.na_text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_NA_AND_SECOND_VALUE + this.rating_answer.second_labels.join(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_SECOND_RATING_VALUE);
            results.GridStartNumber = 1;
            results.GridTextRangeAverage = this.rating_answer.text_range_medium;
            results.GridTextRangeHigh = this.rating_answer.text_range_high;
            results.GridTextRangeLow = this.rating_answer.text_range_low;
            results.Choices = {
                results: this.answer_values.map(AceSurveyUtility.convertChoiceAnswerObjectToString)
            }
            results.Description = JSON.stringify(this.rating_answer.another_config);
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR:
            results.__metadata = { type: 'SP.Field' };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
            results.__metadata = { type: 'SP.Field' };
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            results.__metadata = { type: 'SP.FieldMultiLineText' };
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT;
            results.Description = ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_TEXT_OPTIONS + JSON.stringify(this.answer_values);
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            results.__metadata = { type: 'SP.FieldMultiLineText' };
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT;
            var _meta = {
                rating_answer: this.rating_answer,
                answer_values: this.answer_values,
            }
            results.Description = ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_TEXT_OPTIONS + JSON.stringify(_meta);
            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
            results.__metadata = { type: 'SP.FieldMultiLineText' };
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT;
            var _meta = {
                rating_answer: this.rating_answer,
                answer_values: this.answer_values,
            }

            for (var i = 0; i < _meta.answer_values.length; i++) {
                for (var j = 0; j < _meta.answer_values[i].second_questions.length; j++) {
                    _meta.answer_values[i].second_questions[j] = _meta.answer_values[i].second_questions[j].filter(function (item) {
                        return typeof item === 'string' && item.length > 0;
                    })
                }
            }
            results.Description = ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_CHOICE_MULTILE_TEXT_OPTIONS + JSON.stringify(_meta);

            break;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
            results.__metadata = { type: 'SP.FieldMultiLineText' };
            results.FieldTypeKind = ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT;
            var _meta = {
                rating_answer: this.rating_answer,
                answer_values: this.answer_values,
            }
            results.Description = ACE_SURVEY_CONSTANT.PATTERN.SPLIT_MATRIX_MULTIPLE_CHOICE_OPTIONS + JSON.stringify(_meta);
            break;
    }


    /// JUMP TO PARSER
    switch (this.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
            results.Description = JSON.stringify(this.jumpto_config);
            break;
        default:
            if (results.Description == null) {
                results.Description = "";
            }
            results.Description += ACE_SURVEY_CONSTANT.PATTERN.SPLIT_JUMP_TO_CONFIG + JSON.stringify(this.jumpto_config);
            break;
    }

    return results;
}

QuestionModel.prototype.toSPAnswer = function () {
    switch (this.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME:
            var date_as_string = "";
            if (typeof this.user_answer == 'string') {
                date_as_string = this.user_answer;
            } else if (this.user_answer.toISOString != undefined) {
                // This is DateTime object
                date_as_string = this.user_answer.toISOString();
            }
            return date_as_string;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
            return { results: this.user_answer };
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            var array_answers = [];
            for (var i in this.user_answer) {
                array_answers.push(this.user_answer[i]);
            }
            return { results: array_answers };
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR:
            return null;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
            if (this.sp_original_data.AllowMultipleValues) {
                return { results: this.user_answer };
            }
            return this.user_answer;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            var has_answered = true;
            if (Array.isArray(this.user_answer)) {
                has_answered = this.user_answer.filter(function (item) {
                    return item.Answer && item.Answer.trim().length > 0;
                }).length == this.answer_values.length;
            }
            return has_answered ? JSON.stringify(this.user_answer) : null;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            if (typeof this.user_answer === 'object') {
                var array_answers = [];
                for (var i in this.user_answer) {
                    array_answers.push(this.user_answer[i]);
                }
                return JSON.stringify({ results: array_answers });
            }
            return null;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
            if (typeof this.user_answer === 'object') {
                var array_answers = [];
                for (var i in this.user_answer) {
                    array_answers.push(this.user_answer[i]);
                }
                return JSON.stringify({ results: array_answers });
            }
            return null;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
            if (typeof this.user_answer === 'object') {
                var array_answers = [];
                for (var i in this.user_answer) {
                    array_answers.push(this.user_answer[i]);
                }
                return JSON.stringify({ results: array_answers });
            }
            return null;
        default:
            return this.user_answer;
    }
}

QuestionModel.prototype.toDefaultAnswer = function () {
    switch (this.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
            return [];
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            return {};
        default:
            return "";
    }
}

QuestionModel.prototype.bindDefaultAnswer = function () {
    this.user_answer = this.toDefaultAnswer();
}

QuestionModel.prototype.hasAnswered = function () {

    if (this.user_answer == null) {
        return false;
    }
    switch (this.question_type) {
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
            var keys = Object.keys(this.user_answer);
            return keys && keys.length > 0;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
            return Array.isArray(this.user_answer) && this.user_answer.length > 0;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
            var has_answered = false;
            for (var k in this.user_answer) {
                //  has_answered = false;
                for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                    if (AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j].Answer) == false) {
                        has_answered = true;
                        break;
                    }
                }

                // allow pass if user fill text to any field
                if (has_answered == true) {
                    return has_answered;
                }
                // if (has_answered == false) {
                //     return has_answered;
                // }
            }
            return has_answered;
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_TEXT:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_TEXT:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME:
        case ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY:
            return this.user_answer != null && this.user_answer.toString().length > 0

    }
    return true;
}

// Only used to MATRIX, MATRIX MULTIPLE CHOICE & MATRIX CHOICE TEXT type
QuestionModel.prototype.hasAnswerSameToMinRequire = function () {
    if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT) {
        var count_answered = 0;
        for (var k in this.user_answer) {
            //  has_answered = false;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j].Answer) == false) {
                    count_answered++;
                    break;
                }
            }
        }
        return count_answered >= this.rating_answer.another_config.min_answer;
    } else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE) {
        var count_answered = 0;
        for (var k in this.user_answer) {
            //  has_answered = false;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j]) == false) {
                    count_answered++;
                    break;
                }
            }
        }
        return count_answered >= this.rating_answer.another_config.min_answer;
    }
    else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX) {
        var keys = Object.keys(this.user_answer);
        var count_has_answered = keys.length;
        return keys && count_has_answered >= this.rating_answer.another_config.min_answer;
    }
    return true;
}

// Only used to MATRIX, MATRIX MULTIPLE CHOICE & MATRIX CHOICE TEXT type
QuestionModel.prototype.hasAnswerSameToMaxRequire = function () {
    if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT) {
        var count_answered = 0, count_max_horizontal = 0;
        for (var k in this.user_answer) {
            //  has_answered = false;
            count_max_horizontal = 0;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (count_max_horizontal == 0 && AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j].Answer) == false) {
                    count_answered++;
                    break;
                }
            }
        }
        return count_answered <= this.rating_answer.another_config.max_answer;
    }
    else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE) {
        var count_answered = 0, count_max_horizontal = 0;
        for (var k in this.user_answer) {
            //  has_answered = false;
            count_max_horizontal = 0;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (count_max_horizontal == 0 && AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j]) == false) {
                    count_answered++;
                    count_max_horizontal++;
                } else if (count_max_horizontal > 0 && AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j]) == false) {
                    count_max_horizontal++;
                }
            }
            if (count_max_horizontal > this.rating_answer.another_config.max_answer) {
                // Checkbox has checked higher max answer
                return false;
            }
        }
        return count_answered <= this.rating_answer.another_config.max_answer;
    }
    return true;
}


// Check limitation of matrix question: multiple checkbox, multible text (row rating  )
QuestionModel.prototype.checkLimitRating = function () {
    var self = this;
    var count_answer = 0, count_max_horizontal = 0;
    var array_count_empty_answer = [];
    if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE) {
        for (var k in this.user_answer) {
            //  has_answered = false;
            count_max_horizontal = 0;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j]) == false) {
                    count_max_horizontal++;
                }
            }
            array_count_empty_answer.push(count_max_horizontal);
        }

    } else if (this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT) {

        for (var k in this.user_answer) {
            count_answer++;
            //  has_answered = false;
            count_max_horizontal = 0;
            for (var j = 0; j < this.user_answer[k].SecondAnswer.length; j++) {
                if (AceSurveyUtility.isEmptyString(this.user_answer[k].SecondAnswer[j].Answer) == false) {
                    count_max_horizontal++;
                }
            }
            array_count_empty_answer.push(count_max_horizontal);
        }
    }

    if (array_count_empty_answer.length > 0) {
        console.log("array_count_empty_answer", array_count_empty_answer);
        var count_higher_max = AceSurveyUtility.Array(array_count_empty_answer).filter(function (count_item) {
            return count_item > self.rating_answer.another_config.max_rate && self.rating_answer.another_config.max_rate > 0;
        }).length;

        var count_lower_min = AceSurveyUtility.Array(array_count_empty_answer).filter(function (count_item, index) {
            return self.rating_answer.another_config.min_rate > 0 && count_item < self.rating_answer.another_config.min_rate && count_item > 0;
        }).length;

        return count_lower_min == 0 && count_higher_max == 0;
    } else {
        return true;
    }
}

QuestionModel.prototype.isAnswerValid = function () {
    // Check if the question is matrix or matrix text then validate the min answer
    if (
        (
            this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX ||
            this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT ||
            this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE
        )) {
        var valid = true;
        if (this.rating_answer.another_config.min_answer > 0) {
            valid = this.hasAnswerSameToMinRequire();
        }
        if (valid && this.rating_answer.another_config.max_answer > 0) {
            valid = this.hasAnswerSameToMaxRequire();
        }
        if (valid && this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE) {
            valid = this.checkLimitRating();
        } else if (valid && this.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT) {
            valid = this.checkLimitRating();
        }
        return valid;
    }

    // This question is require so need to fill in all questions
    if (this.is_require && this.hasAnswered()) {
        return true;
    } else if (this.is_require == false) {
        return true;
    } else {
        return false;
    }
}

QuestionModel.prototype.hasSaved = function () {
    if (this.__backup == null) {
        return false;
    }
    var clone = {}, clone2 = {};
    var backup = JSON.parse(this.__backup);
    var ignore_property = ['__backup', 'sp_original_data', 'user_answer', 'poll_result', 'user_polls']
    for (var i in this) {
        if (ignore_property.indexOf(i) > -1) {
            continue;
        }

        if (typeof this[i] != 'function') {
            clone[i] = this[i];
            clone2[i] = backup[i];
        } else if (i === 'user_answer') {
            clone[i] = null;
            clone2[i] = null;
        }
    }

    var str = JSON.stringify(clone), str2 = JSON.stringify(clone2);

    if (str2 === str) {
        return true;
    }
    return false;
}

QuestionModel.prototype.hasEditted = function () {
    var has_editted = false;

    var clone = {}
    for (var i in this) {
        if (i != '__backup' && typeof this[i] != 'function') {
            clone[i] = this[i];
        }
    }
    var str = JSON.stringify(clone);
    if (this.__backup !== str) {
        return true;
    }

    var backup_version = JSON.parse(this.__backup);

    if (backup_version == null) {
        return false;
    }

    if (backup_version && typeof this.title === 'string' && typeof backup_version.title == 'string' && this.title.trim() !== backup_version.title.trim()) {
        // console.log(this.title, "!==", backup_version.title);
        return true;
    }

    // if(this.is_require !== backup_version.is_require){
    //     return true;
    // }

    for (var i in this.answer_values) {
        // console.log(this.answer_values[i], "!==", backup_version.answer_values[i]);
        if (this.answer_values[i] == null && backup_version.answer_values[i] != undefined) {
            // User has clicked on the delete button of answer.
            return true
        } else if (this.answer_values[i].text.length > 0 && backup_version.answer_values[i] == undefined) {
            return true;
        } else if (backup_version.answer_values[i] !== undefined || this.answer_values[i].text !== backup_version.answer_values[i].text) {
            return true;
        } else if (this.answer_values[i].image_name !== backup_version.answer_values[i].image_name) {
            return true;
        }
    }

    for (var k in this.rating_answer) {
        // console.log(this.rating_answer[k], "!==", backup_version.rating_answer[k]);
        if (k === 'second_labels') {
            for (var i = 0; i < this.rating_answer[k].length; i++) {
                if (isNaN(this.rating_answer[k][i]) && this.rating_answer[k][i] == i.toString()) {
                    continue;
                }
                if (this.rating_answer[k][i] !== backup_version.rating_answer[k][i]) {
                    return true;
                }
            }
        } else if (this.rating_answer[k] !== backup_version.rating_answer[k]) {
            return true;
        }
    }

    return false;
}

QuestionModel.prototype.setPollResult = function (poll_list) {
    // reset latest result
    this.user_polls = [];
    this.total_answer = 0;

    var self = this;

    var list_filter_by_question = poll_list.reduce(function (out, item) {
        var temp = item[self.internal_name];
        /// Count result of matrix question choice
        if (self.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT && typeof temp === 'string' && temp.length > 0) {
            temp = JSON.parse(temp);
            out = out.concat(temp.results);
            // Push item to user poll list to store the answer of each user
            self.user_polls.push({
                user: {
                    user_id: item.Author.ID,
                    full_name: item.Author.Title
                },
                answer: temp.results
            });
            self.total_answer++;
        } else if (self.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE && typeof temp === 'string' && temp.length > 0) {
            temp = JSON.parse(temp);
            out = out.concat(temp.results);
            // Push item to user poll list to store the answer of each user
            self.user_polls.push({
                user: {
                    user_id: item.Author.ID,
                    full_name: item.Author.Title
                },
                answer: temp.results
            });
            self.total_answer++;
        } else if (self.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT && typeof temp === 'string' && temp.length > 0) {
            temp = JSON.parse(temp);
            out = out.concat(temp.results);
            // Push item to user poll list to store the answer of each user
            self.user_polls.push({
                user: {
                    user_id: item.Author.ID,
                    full_name: item.Author.Title
                },
                answer: temp.results
            });
            self.total_answer++;
        } else if (temp != null && typeof temp === 'object' && temp.results != undefined) {
            // Count result of question type as Dropdown, multiple choice, single choice, matrix
            out = out.concat(temp.results);
            // Push item to user poll list to store the answer of each user
            self.user_polls.push({
                user: {
                    user_id: item.Author.ID,
                    full_name: item.Author.Title
                },
                answer: temp.results
            })
            self.total_answer++;
        } else if (temp != null) {
            // Push item to user poll list to store the answer of each user
            switch (self.question_type) {
                case ACE_SURVEY_CONSTANT.QUESTION_TYPE.CURRENCY:
                    temp = AceSurveyUtility.numberToCurrency(temp);
                    break;
                case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DATE_TIME:
                    temp = AceSurveyUtility.stringToDateTime(temp, 'DD/MM/YYYY');
                    break;
                case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
                    temp = AceSurveyUtility.numberToCommaString(temp);
                    break;
                case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
                    if (temp.Title == undefined) {
                        return out;
                    }
                    break;
                case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
                    temp = JSON.parse(temp);
                    break;
                // case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
                // case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
                //     temp = temp.Title;
                //     break;
            }
            self.total_answer++;
            self.user_polls.push({
                user: {
                    user_id: item.Author.ID,
                    full_name: item.Author.Title
                },
                answer: temp
            })
            out.push(temp);
        }
        return out;
    }, []);
    this.poll_result = list_filter_by_question;
    return this;
}

QuestionModel.prototype.toUserPollList = function () {
    return this.user_polls;
}

QuestionModel.prototype.toMatrixChoiceTextGraph = function () {
    var graph_data = [], rs, self = this;
    // Create object that the keys follow the user_answer property
    if (this.poll_result != null) {
        // create graph 
        // format: 
        //  [
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //  ]
        rs = self.answer_values.reduce(function (output, item) {
            var key;
            if (item.image_name.length > 0) {
                key = item.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + item.image_name;
            } else {
                key = item.text;
            }
            // new object: format: ['answer', number,number,number] - default first item is answer object rating record - string
            output[key] = [key];
            for (var i = 0; i < self.rating_answer.end_number; i++) {
                output[key].push(0);
            }
            if (self.rating_answer.na_text.length > 0) {
                output[key].push(0);
            }
            return output;
        }, {});
        // Loop poll result and count
        rs = self.poll_result.reduce(function (output, item) {
            if (item) {
                var key = item.Question;
                console.log("key", key);
                for (var i = 0; i < item.SecondAnswer.length; i++) {
                    if (AceSurveyUtility.isEmptyString(item.SecondAnswer[i].Answer) == false) {
                        if (output[key] !== undefined) {
                            output[key][i + 1] += 1;
                        }
                    }
                }
            }
            return output;
        }, rs);
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}


QuestionModel.prototype.toMatrixGraph = function () {
    var graph_data = [], rs, self = this;
    // Create object that the keys follow the user_answer property
    if (this.poll_result != null) {
        // create graph 
        // format: 
        //  [
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //  ]
        rs = self.answer_values.reduce(function (output, item) {
            var key;
            if (item.image_name.length > 0) {
                key = item.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + item.image_name;
            } else {
                key = item.text;
            }
            // new object: format: ['answer', number,number,number] - default first item is answer object rating record - string
            output[key] = [key];
            for (var i = 0; i < self.rating_answer.end_number; i++) {
                output[key].push(0);
            }
            if (self.rating_answer.na_text.length > 0) {
                output[key].push(0);
            }
            return output;
        }, {});

        // Loop poll result and count
        rs = self.poll_result.reduce(function (output, item) {
            if (item) {
                var key = item.Question;
                if (output[key] !== undefined) {
                    output[key][item.Answer] += 1;
                }
            }
            return output;
        }, rs);
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}

QuestionModel.prototype.toMatrixMultipleChoiceGraph = function () {
    var graph_data = [], rs, self = this;
    // Create object that the keys follow the user_answer property
    if (this.poll_result != null) {
        // create graph 
        // format: 
        //  [
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //      ['answer string', number,number,number]
        //  ]
        rs = self.answer_values.reduce(function (output, item) {
            var key;
            if (item.image_name.length > 0) {
                key = item.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + item.image_name;
            } else {
                key = item.text;
            }
            // new object: format: ['answer', number,number,number] - default first item is answer object rating record - string
            output[key] = [key];
            for (var i = 0; i < self.rating_answer.end_number; i++) {
                output[key].push(0);
            }
            if (self.rating_answer.na_text.length > 0) {
                output[key].push(0);
            }
            return output;
        }, {});
        // Loop poll result and count
        rs = self.poll_result.reduce(function (output, item) {
            if (item) {
                var key = item.Question;
                for (var i = 0; i < item.SecondAnswer.length; i++) {
                    if (AceSurveyUtility.isEmptyString(item.SecondAnswer[i]) == false) {
                        if (output[key] !== undefined) {
                            output[key][i + 1] += 1;
                        }
                    }
                }
            }
            return output;
        }, rs);
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}

QuestionModel.prototype.toMatrixMultileSubQuestionGraph = function () {
    var graph_data = [], rs, self = this;
    // Create object that the keys follow the user_answer property
    if (this.poll_result != null) {

        rs = self.answer_values.reduce(function (output, item) {
            var key;
            if (item.image_name.length > 0) {
                key = item.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + item.image_name;
            } else {
                key = item.text;
            }
            output[key] = [key];
            for (var i = 0; i < self.rating_answer.end_number; i++) {
                output[key].push(0);
            }
            if (self.rating_answer.na_text.length > 0) {
                output[key].push(0);
            }
            return output;
        }, {});

        // Loop poll result and count
        rs = self.poll_result.reduce(function (output, item) {
            if (item) {
                var key = item.Question;
                if (output[key] !== undefined) {
                    output[key][item.Answer] += 1;
                }
            }
            return output;
        }, rs);
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}

QuestionModel.prototype.totalAnswer = function () {
    return this.total_answer;
}

QuestionModel.prototype.toChoiceGraph = function () {
    var graph_data = [], rs, self = this;
    if (this.poll_result != null) {

        rs = this.answer_values.reduce(function (output, item) {
            var key;
            if (item.image_name.length > 0) {
                key = item.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + item.image_name;
            } else {
                key = item.text;
            }
            output[key] = [key, 0];

            output[key][1] = self.poll_result.filter(function (item2) {
                // if (output[item2] == undefined) {
                //     console.log("add other", item2);
                //     // Add other answer that has been changed by user - ex: change the text of question or image, that not same to current answer 
                //     if(item2.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE)){

                //     }
                //     output[item2] = [item2, 0];
                //     // push more answer if it not exist in latest answer list.
                //     self.answer_values.push(AceSurveyUtility.convertChoiceAnswerStringToObject(item2));
                //     return true;
                // }
                return item2 == key;
            }).length;
            return output;
        }, {});
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}

QuestionModel.prototype.toYesNoGraph = function () {
    var graph_data = [0, 0], rs, self = this;
    if (this.poll_result != null) {
        graph_data = this.poll_result.reduce(function (output, item) {
            if (item == undefined) {

            } else if (item == true) {
                output[0] += 1;
            } else if (item == false) {
                output[1] += 1;
            }
            return output;
        }, graph_data)
    }
    return graph_data;
}


QuestionModel.prototype.toNumberGraph = function () {
    var graph_data = [], rs = {}, self = this;
    if (this.poll_result != null) {

        rs = this.poll_result.reduce(function (output, item) {
            if (output[item] == undefined) {
                output["k_" + item] = [item, 1];
            } else {
                output["k_" + item][1] += 1;
            }
            return output;
        }, rs);
        for (var k in rs) {
            graph_data.push(rs[k]);
        }
    }
    return graph_data;
}


QuestionModel.prototype.toGraphData = function () {
    // {
    //     label: '',   
    //     total: 0
    // }
    var graph_data = [], rs, self = this;
    if (this.poll_result != null) {
        switch (this.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
                graph_data = this.toMatrixGraph();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
                graph_data = this.toMatrixChoiceTextGraph();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
                graph_data = this.toMatrixMultipleChoiceGraph();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                graph_data = this.toChoiceGraph();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
                graph_data = this.toYesNoGraph();
                break;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.NUMER:
                graph_data = this.toNumberGraph();
                break;
            // case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
            //     graph_data = this.toMatrixMultileSubQuestionGraph();
            //     break;

        }
        // if (graph_data.length > 0) {
        //     rs = {
        //         label: graph_data.shift(),
        //         values: graph_data
        //     }
        // }
    }
    return graph_data;
}