

var BaseModel = function () {
    this.id = null;
    this.is_deleted = false;
    this.__backup = null;

}

BaseModel.prototype.toJSON = function () {
    throw this.constructor.name + ' not yet to override this method: toJSON()';
}

BaseModel.prototype.fromJSON = function () {
    throw this.constructor.name + ' not yet to override this method: toJSON()';
}

BaseModel.prototype.hasChanges = function () {

}

BaseModel.prototype.isNewModel = function () {
    return this.id == null;
}

BaseModel.prototype.saveState = function () {
    var clone = {}
    for (var i in this) {
        if (i != '__backup' && typeof this[i] != 'function') {
            clone[i] = this[i];
        }
    }
    this.__backup = JSON.stringify(clone);
}

BaseModel.prototype.restoreState = function () {
    if (this.__backup != null) {
        var temp = JSON.parse(this.__backup);
        for (var i in temp) {
            this[i] = temp[i];
        }
    }
}


BaseModel.prototype.hasEditted = function () {
    return false;
}