

var AceSurveyDatabase = function () {
    this.caching = {
        question_list: []
    }
}


AceSurveyDatabase.prototype.responseHandler = function () {
    return {
        _cbSuccess: null,
        _cbFailed: null,
        _cbDone: null,
        _callSuccess: function () {
            var self = this, args = arguments;
            // if (typeof this._cbSuccess === 'function') {
            setTimeout(function () {
                if (self._cbSuccess != null) {
                    self._cbSuccess.apply(null, args);
                }
                if (self._cbDone != null) {
                    self._cbDone.apply(null);
                }
            }, 60);

            // }
        },
        _callFailed: function () {
            var self = this, args = arguments;
            // if (typeof this._cbFailed === 'function') {
            setTimeout(function () {
                if (self._cbFailed) {
                    self._cbFailed.apply(null, args);
                }
                if (self._cbDone != null) {
                    self._cbDone.apply(null);
                }
            }, 50);
            // }
        },
        onSuccess: function (callback) {
            this._cbSuccess = callback;
            return this;
        },
        onFailed: function (callback) {
            this._cbFailed = callback;
            return this;
        },
        onDone: function (callback) {
            this._cbDone = callback;
        }
    }
}

AceSurveyDatabase.prototype.restConnection = function () {
    var self = this;
    var siteurl = _spPageContextInfo.webAbsoluteUrl;
    return {
        GET: function (url) {
            var responseHandler = self.responseHandler();
            $.ajax({
                url: url,
                method: "GET",
                dataType: 'json',
                headers: {
                    "Accept": "application/json; odata=verbose",
                },
                contentType: "application/json",
                success: function (data) {
                    if (data && data.d != undefined) {
                        responseHandler._callSuccess(data.d.results || data.d);
                    } else {
                        responseHandler._callSuccess(data);
                    }
                },
                error: function (error) {
                    try{

                        if (error.responseJSON.error.message && error.responseJSON.error.message.value) {
                            responseHandler._callFailed(error.responseJSON.error.message.value);
                        } else {
                            responseHandler._callFailed(error.responseJSON);
                        }
                    }catch(err){
                        console.error('erro',error)
                    }
                }
            });
            return responseHandler;
        },
        POST: function (url, data) {
            var responseHandler = self.responseHandler();
            $.ajax({
                url: url,
                method: "POST",
                data: JSON.stringify(data),
                contentType: "application/json;odata=verbose",
                headers: {
                    "Accept": "application/json; odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                },
                success: function (data) {
                    if (data && data.d != undefined) {
                        responseHandler._callSuccess(data.d.results || data.d);
                    } else {
                        responseHandler._callSuccess(data);
                    }
                },
                error: function (error) {
                    if (error.responseJSON.error.message && error.responseJSON.error.message.value) {
                        responseHandler._callFailed(error.responseJSON.error.message.value);
                    } else {
                        responseHandler._callFailed(error.responseJSON);
                    }
                }
            });
            return responseHandler;
        },
        DELETE: function (url) {
            var responseHandler = self.responseHandler();
            $.ajax({
                url: url,
                method: "POST",
                contentType: "application/json",
                // data: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json;odata=verbose",
                    "Accept": "application/json; odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "DELETE",
                    "If-Match": "*"
                },
                success: function (data) {
                    if (data && data.d != undefined) {
                        responseHandler._callSuccess(data.d.results || data.d);
                    } else {
                        responseHandler._callSuccess(data);
                    }
                },
                error: function (error) {
                    if (error.responseJSON.error.message && error.responseJSON.error.message.value) {
                        responseHandler._callFailed(error.responseJSON.error.message.value);
                    } else {
                        responseHandler._callFailed(error.responseJSON);
                    }
                }
            });
            return responseHandler;
        },
        UPDATE: function (url, data) {
            var responseHandler = self.responseHandler();
            $.ajax({
                url: url,
                method: "POST",
                contentType: "application/json",
                data: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json;odata=verbose",
                    "Accept": "application/json; odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "MERGE",
                    "If-Match": "*"
                },
                success: function (data) {
                    if (data && data.d != undefined) {
                        responseHandler._callSuccess(data.d.results || data.d);
                    } else {
                        responseHandler._callSuccess(data);
                    }
                },
                error: function (error) {
                    try {

                        if (error.responseJSON && error.responseJSON.error.message && error.responseJSON.error.message.value) {
                            responseHandler._callFailed(error.responseJSON.error.message.value);
                        } else {
                            responseHandler._callFailed(error.responseJSON);
                        }
                    } catch (ex) {
                        responseHandler._callFailed(error);
                    }

                }
            });
            return responseHandler;
        },
        POST_FORM: function (url, form_data) {
            var responseHandler = self.responseHandler();
            var request = $.ajax({
                url: url,
                method: "POST",
                data: form_data,
                processData: false,
                contentType: false,
                // contentType: 'application/x-www-form-urlencoded',
                // enctype: "application/x-www-form-urlencoded",
                headers: {
                    //    "Accept": "application/x-www-form-urlencoded",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                },

                success: function (data) {
                    if (data && data.d != undefined) {
                        responseHandler._callSuccess(data.d.results || data.d);
                    } else {
                        responseHandler._callSuccess(data);
                    }
                },
                error: function (error) {
                    try {
                        if (error.responseJSON && error.responseJSON.error.message && error.responseJSON.error.message.value) {
                            responseHandler._callFailed(error.responseJSON.error.message.value);
                        } else if (error.responseJSON) {
                            responseHandler._callFailed(error.responseJSON);
                        } else {
                            responseHandler._callFailed(error);

                        }
                    } catch (ex) {
                        responseHandler._callFailed(error);
                    }
                    console.log("error", error);
                }
            });
            return responseHandler;
        }
    }
}

AceSurveyDatabase.prototype.createNewSurveyList = function (survey_list_title, description, allow_multi_response) {
    var connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists";

    var payload = {
        '__metadata': { 'type': 'SP.List' },
        'Title': survey_list_title,
        'Description': description,
        'BaseTemplate': 102
    }

    connection.POST(url, payload)
        .onSuccess(function (data) {
            // response_handler._callSuccess(data);
            // var update = {
            //     __metadata: {
            //         type: data.ListItemEntityTypeFullName
            //     },
            //     AllowMultpleResponses: allow_multi_response
            // }
            // var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + data.Id + "')";
            response_handler._callSuccess(data);

        }).onFailed(function (message) {
            response_handler._callFailed(message);
        });

    return response_handler;

}


AceSurveyDatabase.prototype.createNewSurvey = function (survey_list_title, description, allow_multi_response, show_user_response, user_admin, user_contribute, user_read) {
    var connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/ACVMultipleShare/SurveyList.aspx?type=create&siteurl=" + encodeURIComponent(_spPageContextInfo.webAbsoluteUrl);

    var form_data = new FormData();
    form_data.append("Title", survey_list_title);
    form_data.append("Description", description);
    form_data.append("AllowMultipleResponses", allow_multi_response ? "1" : "0");
    form_data.append("ShowUser", show_user_response ? "1" : "0");
    form_data.append("Admin", user_admin || "");
    form_data.append("Contribute", user_contribute || "");
    form_data.append("Read", user_read || "");

    connection.POST_FORM(url, form_data)
        .onSuccess(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            if (response_data.Status == 'success') {
                response_handler._callSuccess(response_data.Data);
            } else if (response_data.Status == 'error') {
                response_handler._callFailed(response_data.Data.Message);

            }
        }).onFailed(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            response_handler._callFailed(response_data.Data);
        });

    return response_handler;

}

AceSurveyDatabase.prototype.updateSurvey = function (survey_id, survey_list_title, description, allow_multi_response, show_user_response, user_admin, user_contribute, user_read) {
    var connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/ACVMultipleShare/SurveyList.aspx?type=update&siteurl=" + encodeURIComponent(_spPageContextInfo.webAbsoluteUrl);

    var form_data = new FormData();
    form_data.append("Title", survey_list_title);
    form_data.append("ListId", survey_id);
    form_data.append("Description", description);
    form_data.append("AllowMultipleResponses", allow_multi_response ? "1" : "0");
    form_data.append("ShowUser", show_user_response ? "1" : "0");
    form_data.append("Admin", user_admin || "");
    form_data.append("Contribute", user_contribute || "");
    form_data.append("Read", user_read || "");

    connection.POST_FORM(url, form_data)
        .onSuccess(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            if (response_data.Status == 'success') {
                response_handler._callSuccess(response_data.Data);
            } else if (response_data.Status == 'error') {
                response_handler._callFailed(response_data.Data.Message);

            }
        }).onFailed(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            response_handler._callFailed(response_data.Data);
        });

    return response_handler;

}


AceSurveyDatabase.prototype.getSurveyListBasicInfo = function (survey_list_title) {
    var connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.siteAbsoluteUrl + "/_layouts/15/ACVMultipleShare/SurveyList.aspx?type=info&listid=" + survey_list_title + "&siteurl=" + encodeURIComponent(_spPageContextInfo.webAbsoluteUrl);

    connection.GET(url)
        .onSuccess(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            if (response_data.Status == 'success') {
                response_handler._callSuccess(response_data.Data);
            } else if (response_data.Status == 'error') {
                response_handler._callFailed(response_data.Data.Message);
            }
        }).onFailed(function (response_data) {
            // response_data = JSON.parse(response_data).Data;
            response_handler._callFailed(response_data.Data);
        });

    return response_handler;

}


AceSurveyDatabase.prototype.getAllQuestionBySurveyTitle = function (survey_list_title) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetById('" + survey_list_title + "')/fields?$filter=(CanBeDeleted eq true)";

    if (this.caching.question_list.length > 0) {
        response_handler._callSuccess(self.caching.question_list);
        return response_handler;
    }

    connection.GET(url).onSuccess(function (data) {
        // var results = data.filter(function (item) {
        //     return item.CanBeDeleted;
        // });
        var results = data.map(function (item, index) {
            return new QuestionModel().fromJSON(item, index);
        });
        self.caching.question_list = results;
        response_handler._callSuccess(results);
    }).onFailed(function (message) {
        response_handler._callFailed(message)
    })
    return response_handler;
}

AceSurveyDatabase.prototype.createSurveyQuestion = function (survey_list_title, question_model) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/fields";

    connection.POST(url, question_model.toJSON())
        .onSuccess(function (data) {
            question_model.fromJSON(data);
            response_handler._callSuccess(data);
        }).onFailed(function (message) {    
            response_handler._callFailed(message)
        })

    return response_handler;
}

AceSurveyDatabase.prototype.updateSurveyQuestion = function (survey_list_title, question_model) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/fields('" + question_model.id + "')";

    connection.UPDATE(url, question_model.toJSON())
        .onSuccess(function (data) {
            question_model.saveState();
            response_handler._callSuccess(data);
        }).onFailed(function (message) {
            response_handler._callFailed(message);
        })
    return response_handler;
}

AceSurveyDatabase.prototype.deleteSurveyQuestion = function (survey_list_title, question_model) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/fields('" + question_model.id + "')";

    connection.DELETE(url, question_model.toJSON()).onSuccess(function (data) {
        response_handler._callSuccess(data);
    }).onFailed(function (message) {
        response_handler._callFailed(message);
    })
    return response_handler;
}

AceSurveyDatabase.prototype.getSurveyPropertyByTitle = function (survey_list_title) {

    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')?$select=*&$expand=RootFolder";

    connection.GET(url)
        .onSuccess(function (data) {
            console.log(data);
            response_handler._callSuccess(data);
        }).onFailed(function (message) {
            response_handler._callFailed(message)
        });
    return response_handler;
}

AceSurveyDatabase.prototype.getAllQuestionBySurveyTitle2 = function (survey_list_title) {


    var clientContext = new SP.ClientContext.get_current();
    var oWebsite = clientContext.get_web();
    var customlist = oWebsite.get_lists().getById(survey_list_title);
    clientContext.load(customlist);
    clientContext.executeQueryAsync(function () {
        console.log(customlist);
    }, function () {

    });

}

AceSurveyDatabase.prototype.submitPollToList = function (survey_list_title, poll_data) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/items";
    this.getSurveyPropertyByTitle(survey_list_title)
        .onSuccess(function (property) {
            poll_data.__metadata = { type: property.ListItemEntityTypeFullName };

            connection.POST(url, poll_data)
                .onSuccess(function (data) {
                    response_handler._callSuccess(data);
                }).onFailed(function (message) {
                    response_handler._callFailed(message)
                });
        });
    return response_handler;
}

AceSurveyDatabase.prototype.getAllUserPolls = function (survey_list_title) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler();

    self.getAllQuestionBySurveyTitle(survey_list_title)
        .onSuccess(function (question_models) {
            var expand = [];
            var fields = question_models.map(function (q) {
                if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER || q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP) {
                    expand.push(q.sp_original_data.EntityPropertyName);
                    return q.sp_original_data.EntityPropertyName + "/ID," + q.sp_original_data.EntityPropertyName + "/Title"
                }
                return q.internal_name;
            }).join(',');
            fields += ',Author/ID,Author/Title';
            var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/items?$select=" + fields + "&$expand=Author";
            if (expand.length > 0) {
                url += ',' + expand.join(',');
            }
            // Get question list first
            self.getAllQuestionBySurveyTitle(survey_list_title)
                .onSuccess(function (question_list) {
                    // Get all poll
                    connection.GET(url)
                        .onSuccess(function (poll_list) {
                            response_handler._callSuccess(poll_list);
                        }).onFailed(function (message) {
                            response_handler._callFailed(message)
                        });
                });
        });

    return response_handler;
}

AceSurveyDatabase.prototype.getAllUserPollsAsQuestionList = function (survey_list_title) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler();

    self.getAllQuestionBySurveyTitle(survey_list_title)
        .onSuccess(function (question_models) {
            var expand = [];
            var fields = question_models.map(function (q) {
                if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER || q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP) {
                    expand.push(q.sp_original_data.EntityPropertyName);
                    return q.sp_original_data.EntityPropertyName + "/ID," + q.sp_original_data.EntityPropertyName + "/Title"
                }
                return q.internal_name;
            }).join(',');
            fields += ',Author/ID,Author/Title';
            var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')/items?$select=" + fields + "&$expand=Author";
            if (expand.length > 0) {
                url += ',' + expand.join(',');
            }
            // Get question list first
            self.getAllQuestionBySurveyTitle(survey_list_title)
                .onSuccess(function (question_list) {
                    // Get all poll
                    connection.GET(url)
                        .onSuccess(function (poll_list) {

                            // Merge poll to question list
                            question_list = question_list.map(function (question_model) {
                                return question_model.setPollResult(poll_list);
                            });
                            response_handler._callSuccess(question_list);
                        }).onFailed(function (message) {
                            response_handler._callFailed(message)
                        });
                });
        });

    return response_handler;
}

AceSurveyDatabase.prototype.getUserIdByLoginName = function (login_name) {
    var response_handler = this.responseHandler();

    var context = SP.ClientContext.get_current();
    var theUser = context.get_web().ensureUser(login_name);
    context.load(theUser);
    context.executeQueryAsync(function () {
        response_handler._callSuccess(theUser.get_id());
    }, function (sender, args) {
        response_handler._callFailed();
    });
    return response_handler;
}

AceSurveyDatabase.prototype.findUsersByLoginName = function (login_name) {
    login_name = login_name.indexOf("|") > -1 ? login_name.split("|")[1] : login_name;
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.siteAbsoluteUrl + "/_api/web/siteusers(@v)?@v='" + encodeURIComponent(login_name) + "'";
    this.getUserIdByLoginName(login_name).onSuccess(function () {
        connection.POST(url)
            .onSuccess(function (data) {
                response_handler._callSuccess(data);
            }).onFailed(function (message) {
                response_handler._callFailed(message)
            });
    })
    return response_handler;
}

AceSurveyDatabase.prototype.getAllCannotDeleteFieldsBySurveyTitle = function (survey_list_title) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler(),
        url = _spPageContextInfo.webAbsoluteUrl + "/_api/Web/Lists/GetById('" + survey_list_title + "')/fields?$filter=(CanBeDeleted eq false)";

    connection.GET(url).onSuccess(function (data) {
        // var results = data.filter(function (item) {
        //     return item.CanBeDeleted;
        // });
        var results = data.map(function (item) {
            return new QuestionModel().fromJSON(item);
        });
        response_handler._callSuccess(results);
    }).onFailed(function (message) {
        response_handler._callFailed(message)
    })
    return response_handler;
}

AceSurveyDatabase.prototype.reOrderQuestion = function (survey_list_id, list_question_model) {
    var self = this,
        connection = this.restConnection(),
        response_handler = this.responseHandler();

    self.getAllCannotDeleteFieldsBySurveyTitle(survey_list_id).onSuccess(function (results) {
        var list_all_fields = list_question_model.concat(results);
        console.log(list_all_fields);
        var url = _spPageContextInfo.webAbsoluteUrl + "/_vti_bin/owssvr.dll?CS=65001";

        var xml_reorder = list_all_fields.reduce(function (output, question_model) {
            output += '<Field Name="' + question_model.internal_name + '"/>';
            return output;
        }, '');
        xml_reorder = "<Fields>" + xml_reorder + "</Fields>";
        console.log(xml_reorder);


        $.get('http://sp2016/Survey/_layouts/15/formEdt.aspx?List=%7BAE39FDB1%2D72E5%2D4D47%2DBFC2%2D434D2C4BEA77%7D').done(function (response_text, xhr) {
            // console.log(response_text);
            // var panel = document.createElement("div");
            // panel.id = 'hidden_form_' + new Date().getTime();
            // panel.innerHTML = response_text;
            // var form = panel.querySelector("#DeltaPlaceHolderUtilityContent");
            // document.body.appendChild(form);
            // document.frmLayoutSubmit["ReorderedFields"].value = xml_reorder;
            // document.frmLayoutSubmit.action = "http://sp2016/Survey/_vti_bin/owssvr.dll?CS=65001";
            // ajaxNavigate.submit(document.frmLayoutSubmit);
            document.body.innerHTML = response_text;
        })

        var from_data = new FormData();
        from_data.append('__REQUESTDIGEST', $("#__REQUESTDIGEST").val());
        from_data.append('ReorderedFields', xml_reorder);
        from_data.append('Cmd', 'REORDERFIELDS');
        from_data.append('List', survey_list_id.toUpperCase());
        from_data.append('owshiddenversion', 619);
        from_data.append('NextUsing', 'http://sp2016/Survey/_layouts/15/SurvEdit.aspx');

        connection.POST_FORM(url, from_data)
    });
}

AceSurveyDatabase.prototype.isAuthorOrSurveyManager = function (survey_list_title) {
    var _PERMISSION_SURVEY_MANAGER_NAME = 'Survey Managers';
    var self = this,
        response_handler = this.responseHandler(),
        connection = this.restConnection();

    connection.GET(_spPageContextInfo.webAbsoluteUrl + "/_api/web/currentuser/?$expand=groups").onSuccess(function (data) {
        var is_survey_manager = data.Groups.results.filter(function (item) {
            return item.Title === _PERMISSION_SURVEY_MANAGER_NAME;
        }).length > 0;
        if (is_survey_manager) {
            response_handler._callSuccess(true);
        } else {
            // Check is author
            self.getSurveyPropertyByTitle(survey_list_title)
                .onSuccess(function (list_info) {
                    var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getfilebyserverrelativeurl('" + list_info.RootFolder.ServerRelativeUrl + "/overview.aspx')?$expand=Author";
                    connection.GET(url).onSuccess(function (author) {
                        if (author.Author.Id == _spPageContextInfo.userId) {
                            response_handler._callSuccess(true);
                        } else {
                            response_handler._callSuccess(false);
                        }
                    });
                }).onFailed(function (message) {
                    response_handler._callFailed(message);
                });

        }
    }).onFailed(function (message) {
        response_handler._callFailed(message);
    });
    return response_handler;
}


AceSurveyDatabase.prototype.executeQuery = function (url) {
    var self = this,
        response_handler = this.responseHandler(),
        connection = this.restConnection();
    //  var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getById('" + survey_list_title + "')" + query;
    connection.GET(url).onSuccess(function (data) {
        response_handler._callSuccess(data);
    }).onFailed(function (message) {
        response_handler._callFailed(message);
    })

    return response_handler;
}

var AceSVDbInstance = new AceSurveyDatabase();
