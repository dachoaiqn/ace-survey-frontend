





var SurveyEditorPage = function (survey_list_title) {
    this.survey_list_title = survey_list_title;
    this.listener = {
        onPageRenderred: null
    }
    this.question_model_list = [];

    this.root_elem = null;
}

SurveyEditorPage.prototype.renderToElement = function (element) {
    this.root_elem = element;
    this.listener.onPageRenderred = arguments[1];
    this.renderUI();
    return this;
}

SurveyEditorPage.prototype.renderUI = function () {
    var self = this;
    this.panel_survey_holder = new AceSurveyPanel();
    this.panel_survey_holder.setElementId();
    this.panel_survey_holder.addClass('survey-holder');

    this.toolbar_holder = new AceSurveyPanel();
    this.toolbar_holder.addClass('survey-toolbar-holder');

    this.btn_add_question = new AceSurveyMaterialButton("add", ACE_SURVEY_STRINGS.add_question);
    this.btn_reorder_question = new AceSurveyMaterialButton('sort_by_alpha', ACE_SURVEY_STRINGS.reorder_question);
    this.btn_preview = new AceSurveyMaterialButton('fullscreen', ACE_SURVEY_STRINGS.preview);
    this.btn_role = new AceSurveyMaterialButton('security', ACE_SURVEY_STRINGS.role);
    this.btn_save = new AceSurveyMaterialButton("save", ACE_SURVEY_STRINGS.save_question);
    this.btn_cancel = new AceSurveyMaterialButton("cancel", ACE_SURVEY_STRINGS.cancel_question);

    this.btn_save.addClass("btn-save");

    this.toolbar_holder.appendChild(this.btn_add_question);
    this.toolbar_holder.appendChild(this.btn_role);
    this.toolbar_holder.appendChild(this.btn_reorder_question);
    this.toolbar_holder.appendChild(this.btn_preview);
    this.toolbar_holder.appendChild(this.btn_save);
    // this.toolbar_holder.appendChild(this.btn_cancel);

    this.pn_question_list = new AceSurveyPanel();
    this.pn_question_list.addClass('survey-question-list');

    this.panel_survey_holder.appendChild(this.toolbar_holder);
    this.panel_survey_holder.appendChild(this.pn_question_list);
    this.panel_survey_holder.showLoading();

    AceSVDbInstance
        .getAllQuestionBySurveyTitle(self.survey_list_title)
        .onSuccess(function (question_model_list) {
            if (question_model_list.length == 0) {
                var question_editor_item = new AceSurveyQuestionEditor(self.survey_list_title);
                self.pn_question_list.appendChild(question_editor_item);
                self.question_model_list.push(question_editor_item.getQuestionModel());
                // Scroll to bottom
                var s4_workspace = document.getElementById('s4-workspace');
                $(s4_workspace).scrollTop(s4_workspace.scrollHeight);
                return;
            }
            self.question_model_list = question_model_list;
            self.question_model_list.forEach(function (question_model) {
                var question_editor_item = new AceSurveyQuestionEditor(self.survey_list_title, question_model, self.question_model_list);
                self.pn_question_list.appendChild(question_editor_item);
                question_editor_item.addAnimationClass("ace-sv-anim-fade-in");
            })
        }).onFailed(function (message) {
            alert(message);
        }).onDone(function () {
            self.panel_survey_holder.hideLoading();
            setTimeout(function () {
                if (self.listener.onPageRenderred != null) {
                    self.listener.onPageRenderred.call(self);
                }
            }, 100);

        });

    var is_previewing = false, view_survey_response;
    this.btn_preview.addEventListener("click", function () {
        is_previewing = !is_previewing;
        try {
            if (is_previewing) {
                self.btn_preview.setIcon('fullscreen_exit');
                self.pn_question_list.hide();
                var valid_question_list = self.question_model_list.filter(function (question) {
                    return question.is_deleted == false && question.title.length > 0;
                });
                view_survey_response = new SurveyReponsePage(self.survey_list_title, valid_question_list);
                view_survey_response.renderToElement(self.root_elem);
                view_survey_response.removeToolbar();
                self.btn_add_question.disable();
            } else {
                self.btn_preview.setIcon('fullscreen');
                view_survey_response.destroy();
                view_survey_response = null;
                self.pn_question_list.show();
                self.btn_add_question.enable();
            }
        } catch (ex) {
            console.log(ex);
        }
        return false;
    })

    var latest_question_editor = null;
    this.btn_add_question.addEventListener("click", function () {
        if (latest_question_editor != null && latest_question_editor.has_destroy == false) {
            latest_question_editor.saveQuestion();
            latest_question_editor = null;
        }
        var question_editor_item = latest_question_editor = new AceSurveyQuestionEditor(self.survey_list_title);
        self.pn_question_list.appendChild(question_editor_item);
        self.question_model_list.push(question_editor_item.getQuestionModel());
        // Scroll to bottom
        var s4_workspace = document.getElementById('s4-workspace');
        $(s4_workspace).scrollTop(s4_workspace.scrollHeight);
        return false;
    });

    this.btn_role.addEventListener("click", function () {
        if (confirm(ACE_SURVEY_STRINGS.confirm_move_to_role_page)) {
            window.location.href = _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/user.aspx?obj=' + self.survey_list_title + ',list&List=' + self.survey_list_title;
        }
        return false;
    });

    this.btn_reorder_question.addEventListener('click', function () {
        var url = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/formEdt.aspx?List=" + self.survey_list_title;
        window.open(url, '_blank');
        // new AceReOrderQuestionDialog(self.question_model_list)
        //     .onOrderred(function (new_question_list) {
        //         self.question_model_list = new_question_list;
        //         AceSVDbInstance.reOrderQuestion(self.survey_list_title, self.question_model_list);
        //         self.pn_question_list.setHtml('');
        //         // Rerender question after order;
        //         self.question_model_list.forEach(function (question_model) {
        //             var question_editor_item = new AceSurveyQuestionEditor(self.survey_list_title, question_model);
        //             self.pn_question_list.appendChild(question_editor_item);
        //             question_editor_item.addAnimationClass("ace-sv-anim-fade-in")
        //         })
        //     });
        return false;
    });

    this.btn_save.addEventListener('click', function () {
        try {
            var save_list = self.question_model_list.filter(function (question) {
                return question && question.is_deleted == false && question.title.length > 0;
            });
            new AceSaveAllQuestionDialog(self.survey_list_title, save_list);
        } catch (e) {
            console.log(e);
        }
        return false;
    })

    window.onbeforeunload = function () {
        for (var i = 0; i < self.question_model_list.length; i++) {
            if (self.question_model_list[i].is_deleted == false && self.question_model_list[i].hasSaved() == false) {
                return this.ACE_SURVEY_STRINGS.confirm_exit_without_save;
            }
        }
        // return null;
    }

    this.root_elem.appendChild(this.panel_survey_holder.element);
}

SurveyEditorPage.prototype.onPageRenderred = function (callback) {
    this.listener.onPageRenderred = callback;
    return this;
}