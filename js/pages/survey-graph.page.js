

var SurveyGraphPage = function (survey_list_title, question_list) {
    this.survey_list_title = survey_list_title;
    this.root_elem = null;
    this.question_model_list = question_list || [];
    this.listener = {
        onPageRenderred: null
    }
}

SurveyGraphPage.prototype.renderToElement = function (element) {
    var self = this;
    this.root_elem = element;
    this.listener.onPageRenderred = arguments[1];
    AceSVDbInstance
        .getSurveyListBasicInfo(self.survey_list_title)
        .onSuccess(function (info) {
            ACE_SV_GLOBAL_VALUE.survey_basic_info = info;
            self.renderUI();
        });
    return this;
}

SurveyGraphPage.prototype.removeToolbar = function () {
    if (this.toolbar_holder) {
        this.toolbar_holder.destroy();
    }
}

SurveyGraphPage.prototype.destroy = function () {
    if (this.root_elem && this.panel_survey_holder) {
        this.root_elem.removeChild(this.panel_survey_holder.element);
    }
}

SurveyGraphPage.prototype.renderUI = function () {

    var self = this;
    this.panel_survey_holder = new AceSurveyPanel();
    this.panel_survey_holder.setElementId();
    this.panel_survey_holder.addClass('survey-holder');

    this.toolbar_holder = new AceSurveyPanel();
    this.toolbar_holder.addClass('survey-toolbar-holder');

    this.btn_export = new AceSurveyMaterialButton("get_app", ACE_SURVEY_STRINGS.export_excel);
    this.btn_cancel = new AceSurveyMaterialButton("cancel", ACE_SURVEY_STRINGS.cancel_survey);

    this.btn_export.hide();

    this.toolbar_holder.appendChild(this.btn_export);
    // this.toolbar_holder.appendChild(this.btn_cancel);

    this.pn_question_list = new AceSurveyPanel();
    this.pn_question_list.addClass('survey-question-list');

    this.panel_survey_holder.appendChild(this.toolbar_holder);
    this.panel_survey_holder.appendChild(this.pn_question_list);

    if (this.question_model_list.length == 0) {
        self.panel_survey_holder.showLoading();
        AceSVDbInstance
            .getAllUserPollsAsQuestionList(self.survey_list_title)
            .onSuccess(function (quest_models) {
                self.question_model_list = quest_models;
                self.question_model_list.forEach(function (question_model) {
                    var question_editor_item = new AceSurveyQuestionGraph(self.survey_list_title, question_model);
                    question_editor_item.addAnimationClass('ace-sv-anim-fade-in');
                    self.pn_question_list.appendChild(question_editor_item);
                })
            }).onDone(function () {
                self.panel_survey_holder.hideLoading();
                setTimeout(function () {
                    if (self.listener.onPageRenderred != null) {
                        self.listener.onPageRenderred.call(self);
                    }
                }, 100);
            });
    } else {
        self.question_model_list.forEach(function (question_model) {
            var question_editor_item = new AceSurveyQuestionGraph(self.survey_list_title, question_model);
            self.pn_question_list.appendChild(question_editor_item);
        });
        setTimeout(function () {
            if (self.listener.onPageRenderred != null) {
                self.listener.onPageRenderred.call(self);
            }
        }, 100);
    }

    this.root_elem.appendChild(this.panel_survey_holder.element);

    AceSVDbInstance
        .isAuthorOrSurveyManager(self.survey_list_title)
        .onSuccess(function (is_admin) {
            if (is_admin) {
                self.btn_export.show();
                self.btn_export.addEventListener('click', function () {
                    self.exportExcel();
                    return false;
                })
            }
        });

}

SurveyGraphPage.prototype.onPageRenderred = function (callback) {
    this.listener.onPageRenderred = callback;
    return this;
}

SurveyGraphPage.prototype.exportExcel = function () {
    var self = this;
    self.panel_survey_holder.showLoading();

    // var allow_show_user = ACE_SV_GLOBAL_VALUE.survey_basic_info.ShowUser;
    // CR - Always show user in excel file.
    var allow_show_user = true;

    AceSVDbInstance
        .getAllUserPolls(self.survey_list_title)
        .onSuccess(function (poll_list) {
            if (allow_show_user) {
                var list_header = [ACE_SURVEY_STRINGS.full_name].concat(self.question_model_list.map(function (item) {
                    return item.title + "?";
                }));
                var list_result = poll_list.map(function (poll_item) {
                    return [poll_item.Author.Title].concat(self.question_model_list.map(function (question_model) {
                        return AceSurveyUtility.convertUserAnswerPlainText(question_model, poll_item[question_model.internal_name], ACE_SURVEY_STRINGS.empty_answer);
                    }));
                });
            } else {
                var list_header = self.question_model_list.map(function (item) {
                    return item.title + "?";
                });
                var list_result = poll_list.map(function (poll_item) {
                    return (self.question_model_list.map(function (question_model) {
                        return AceSurveyUtility.convertUserAnswerPlainText(question_model, poll_item[question_model.internal_name], ACE_SURVEY_STRINGS.empty_answer);
                    }));
                });
            }

            list_result.splice(0, 0, list_header);


            var table_matrix_results = AceSurveyUtility.convertMatrixQuestionsToHtmlTable(self.question_model_list, poll_list)

            var work_book = XLSX.utils.book_new();
            work_book.SheetNames.push("Sheet 1");

            var work_sheet = XLSX.utils.aoa_to_sheet(list_result);
            work_book.Sheets["Sheet 1"] = work_sheet;

            for (var i = 0; i < table_matrix_results.length; i++) {
                var matrix_sheet_name = "Matrix " + (i + 1);
                work_book.SheetNames.push(matrix_sheet_name);
                var matrix_sheet = XLSX.utils.table_to_sheet(table_matrix_results[i]);
                work_book.Sheets[matrix_sheet_name] = matrix_sheet;
            }

            var exporter_xlsx = XLSX.write(work_book, { bookType: 'xlsx', type: 'binary' });
            var fileName = moment().format('hhmmss-HHDDYYYY') + ".xlsx";
            saveAs(new Blob([AceSurveyUtility.s2ab(exporter_xlsx)], { type: "application/octet-stream" }), fileName);
            self.panel_survey_holder.hideLoading();
        });

}