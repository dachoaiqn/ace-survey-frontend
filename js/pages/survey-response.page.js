

var SurveyReponsePage = function (survey_list_title, question_list) {
    this.survey_list_title = survey_list_title;
    this.listener = {
        onPageRenderred: null
    }
    this.root_elem = null;
    this.question_model_list = question_list || [];
    this.question_viewer_list = [];
    this.tracking_question_answering = 0;
    this.queue_question_index = [];
    this.queue_user_answer = {};
}

SurveyReponsePage.prototype.renderToElement = function (element) {
    this.root_elem = element;
    this.listener.onPageRenderred = arguments[1];
    this.renderUI();
    return this;
}

SurveyReponsePage.prototype.setPreviewQuestionData = function (question_list) {
    this.question_model_list = question_list;
}

SurveyReponsePage.prototype.removeToolbar = function () {
    if (this.toolbar_holder) {
        this.toolbar_holder.destroy();
    }
}

SurveyReponsePage.prototype.destroy = function () {
    if (this.root_elem && this.panel_survey_holder) {
        this.root_elem.removeChild(this.panel_survey_holder.element);
    }
}

SurveyReponsePage.prototype.renderUIAllQuestion = function () {
    var self = this;
    this.panel_survey_holder = new AceSurveyPanel();
    this.panel_survey_holder.setElementId();
    this.panel_survey_holder.addClass('survey-holder');

    this.toolbar_holder = new AceSurveyPanel();
    this.toolbar_holder.addClass('survey-toolbar-holder');

    this.btn_save = new AceSurveyMaterialButton("send", ACE_SURVEY_STRINGS.submit_survey);
    this.btn_cancel = new AceSurveyMaterialButton("cancel", ACE_SURVEY_STRINGS.cancel_survey);

    this.btn_save.addClass("btn-submit-poll");
    // this.btn_save.hide();

    this.toolbar_holder.appendChild(this.btn_save);
    this.toolbar_holder.appendChild(this.btn_cancel);

    this.pn_question_list = new AceSurveyPanel();
    this.pn_question_list.addClass('survey-question-list');

    this.panel_survey_holder.appendChild(this.toolbar_holder);
    this.panel_survey_holder.appendChild(this.pn_question_list);

    if (this.question_model_list.length == 0) {
        self.panel_survey_holder.showLoading();
        AceSVDbInstance.getAllQuestionBySurveyTitle(self.survey_list_title)
            .onSuccess(function (quest_models) {
                self.question_model_list = quest_models;
                self.question_model_list.forEach(function (question_model) {
                    var question_editor_item = new AceSurveyQuestionViewer(self.survey_list_title, question_model);
                    question_editor_item.addAnimationClass('ace-sv-anim-fade-in');
                    self.pn_question_list.appendChild(question_editor_item);
                    self.question_viewer_list.push(question_editor_item);
                })
            }).onDone(function () {
                setTimeout(function () {
                    if (self.listener.onPageRenderred != null) {
                        self.listener.onPageRenderred.call(self);
                    }
                    self.panel_survey_holder.hideLoading();
                }, 100);
            });
    } else {
        self.question_model_list.forEach(function (question_model) {
            var question_editor_item = new AceSurveyQuestionViewer(self.survey_list_title, question_model);
            self.pn_question_list.appendChild(question_editor_item);
        });
        setTimeout(function () {
            if (self.listener.onPageRenderred != null) {
                self.listener.onPageRenderred.call(self);
            }
        }, 100);
    }

    this.root_elem.appendChild(this.panel_survey_holder.element);

    this.btn_save.addEventListener('click', function () {
        try {
            self.queue_question_index.push(self.tracking_question_answering);
            self.submitPoll();

        } catch (error) {
            console.error(error);
        }
        return false;
    })


};


SurveyReponsePage.prototype.renderUI = function () {
    var self = this;
    this.panel_survey_holder = new AceSurveyPanel();
    this.panel_survey_holder.setElementId();
    this.panel_survey_holder.addClass('survey-holder');

    this.toolbar_holder = new AceSurveyPanel();
    this.toolbar_holder.addClass('survey-toolbar-holder');

    this.btn_next = new AceSurveyMaterialButton("arrow_forward", ACE_SURVEY_STRINGS.next_question);
    this.btn_back = new AceSurveyMaterialButton("arrow_back", ACE_SURVEY_STRINGS.next_question);
    this.btn_save = new AceSurveyMaterialButton("send", ACE_SURVEY_STRINGS.submit_survey);
    this.btn_cancel = new AceSurveyMaterialButton("cancel", ACE_SURVEY_STRINGS.cancel_survey);

    this.btn_save.hide();
    this.btn_save.addClass("btn-submit-poll");

    this.toolbar_holder.appendChild(this.btn_save);
    this.toolbar_holder.appendChild(this.btn_next);
    this.toolbar_holder.appendChild(this.btn_back);
    this.toolbar_holder.appendChild(this.btn_cancel);

    this.pn_question_list = new AceSurveyPanel();
    this.pn_question_list.addClass('survey-question-list');

    this.panel_survey_holder.appendChild(this.toolbar_holder);
    this.panel_survey_holder.appendChild(this.pn_question_list);

    this.question_viewer_list = [];
    if (this.question_model_list.length == 0) {
        self.panel_survey_holder.showLoading();
        AceSVDbInstance.getAllQuestionBySurveyTitle(self.survey_list_title)
            .onSuccess(function (quest_models) {
                self.question_model_list = quest_models;
                self.tracking_question_answering = 0;
                self.question_model_list.forEach(function (question_model) {
                    var question_editor_item = new AceSurveyQuestionViewer(self.survey_list_title, question_model);
                    self.pn_question_list.appendChild(question_editor_item);
                    question_editor_item.hide();
                    self.question_viewer_list.push(question_editor_item);
                });

                if (self.question_viewer_list[0]) {
                    self.question_viewer_list[0].show();
                }

            }).onDone(function () {
                setTimeout(function () {
                    if (self.listener.onPageRenderred != null) {
                        self.listener.onPageRenderred.call(self);
                    }
                    self.panel_survey_holder.hideLoading();
                }, 100);
            });
    } else if (self.question_model_list.length > 0) {
        self.tracking_question_answering = 0;
        self.question_model_list.forEach(function (question_model) {
            var question_editor_item = new AceSurveyQuestionViewer(self.survey_list_title, question_model);
            self.pn_question_list.appendChild(question_editor_item);
            question_editor_item.hide();
            self.question_viewer_list.push(question_editor_item);
        });
        if (self.question_viewer_list[0]) {
            self.question_viewer_list[0].show();
        }
        setTimeout(function () {
            if (self.listener.onPageRenderred != null) {
                self.listener.onPageRenderred.call(self);
            }
        }, 100);
    }

    this.root_elem.appendChild(this.panel_survey_holder.element);

    this.btn_save.addEventListener('click', function () {
        try {
            self.queue_question_index.push(self.tracking_question_answering);
            self.submitPoll();

        } catch (error) {
            console.error(error);
        }
        return false;
    })

    this.btn_next.addEventListener('click', function (e) {
        try {
            self.nextQuestion();
        } catch (err) {
            console.error(err);
        }
        return false;
    })

    this.btn_back.addEventListener('click', function (e) {
        try {
            self.backQuestion();
        } catch (err) {
            console.error(err);
        }
        return false;
    })
}

SurveyReponsePage.prototype.nextQuestion = function () {
    this.queue_question_index.push(this.tracking_question_answering);
    var self = this;
    var next_direction = {
        next_default: function () {
            if (self.tracking_question_answering + 1 < self.question_viewer_list.length) {
                self.question_viewer_list[self.tracking_question_answering].hide()
                self.tracking_question_answering++;

                var q = self.question_model_list[self.tracking_question_answering];
                if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
                    next_direction.next_default();
                } else {
                    self.question_viewer_list[self.tracking_question_answering]
                        .show()
                        .addAnimationClass('ace-sv-anim-scale-in');
                }
            }
        },
        next_to_index: function (idx) {
            if (idx == -1 || idx >= self.question_viewer_list[self.tracking_question_answering].length) {
                return;
            }
            self.question_viewer_list[self.tracking_question_answering].hide();
            self.tracking_question_answering = idx;
            var q = self.question_model_list[self.tracking_question_answering];
            if (q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.PAGE_SEPARATOR) {
                next_direction.next_default();
            } else {
                self.question_viewer_list[self.tracking_question_answering]
                    .show()
                    .addAnimationClass('ace-sv-anim-scale-in');
            }

        }
    }

    var question = this.question_model_list[this.tracking_question_answering];
    var user_answer;

    if (question.hasAnswered() == false && question.is_require == true) {
        toastr.error(ACE_SURVEY_STRINGS.error_not_fill_all_answer, { timeOut: 2000 });
        return;
    } else if (question.isAnswerValid() == false) {
        toastr.error(ACE_SURVEY_STRINGS.error_not_fill_all_answer, { timeOut: 2000 });
        return
    } else if (question.hasAnswered() == false && question.is_require == false) {
        next_direction.next_default();
        return;
    } else if (question.hasAnswered() == true) {
        // Find Logic
        if (question.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.DROPDOWN
            || question.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE
            || question.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO) {
            var jump_to = AceSurveyUtility.Array(question.jumpto_config).find(function (item) {
                return item.answer_value === question.user_answer;
            });
            if (jump_to != null && jump_to.jumpto_question_id.length > 0) {
                var idx = AceSurveyUtility.Array(self.question_model_list).findIndex(function (item) {
                    return item.id == jump_to.jumpto_question_id;
                });
                next_direction.next_to_index(idx);
            } else {
                next_direction.next_default();
            }
            question.user_answer = question.user_answer;
        } else {
            if (question.jumpto_config.length > 0) {
                var idx = AceSurveyUtility.Array(self.question_model_list).findIndex(function (item) {
                    return item.id == question.jumpto_config[0].jumpto_question_id;
                });
                next_direction.next_to_index(idx);
            } else {
                next_direction.next_default();
            }
            user_answer = question.hasAnswered();
        }
    }

    this.queue_user_answer[question.id] = user_answer;

    if (self.question_model_list.length <= self.tracking_question_answering + 1) {
        this.btn_save.show();
        this.btn_next.hide();
    } else {
        this.btn_save.hide();
    }
}

SurveyReponsePage.prototype.backQuestion = function () {
    var self = this;
    this.btn_next.show();
    if (self.queue_question_index.length > 0) {
        self.question_viewer_list[this.tracking_question_answering].hide();
        var idx = self.queue_question_index.pop();

        self.tracking_question_answering = idx;

        self.question_viewer_list[self.tracking_question_answering]
            .show()
            .addAnimationClass('ace-sv-anim-scale-in');

    }

}

SurveyReponsePage.prototype.validateAnswer = function () {
    var is_valid = true;
    for (var i = 0; i < this.question_model_list.length; i++) {

        if (this.question_model_list[i].isAnswerValid() == false) {
            this.question_viewer_list[i].setError(true);
            is_valid = false;
        } else {
            this.question_viewer_list[i].setError(false);
        }
    }
    

    return is_valid;
}

SurveyReponsePage.prototype.submitPoll = function () {
    var self = this;
    var poll_data = {};
    if (this.validateAnswer() == false) {
        toastr.error(ACE_SURVEY_STRINGS.error_not_fill_all_answer, { timeOut: 2000 });
        return;
    }
    for (var i = 0; i < self.queue_question_index.length; i++) {
        var index = self.queue_question_index[i];
        var q = self.question_model_list[index];
        if (q.hasAnswered() == true) {
            var internal_name = q.internal_name + ((q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER || q.question_type == ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP) ? "Id" : '');
            poll_data[internal_name] = q.toSPAnswer();
        }
    }
    AceSurveyUtility.showLoadingPage();

    AceSVDbInstance.submitPollToList(self.survey_list_title, poll_data)
        .onSuccess(function (data) {
            // toastr.success(ACE_SURVEY_STRINGS.submit_success, { timeOut: 2000 });
            AceSurveyUtility.hideLoadingPage();
            self.renderSuccessPage();
        }).onFailed(function (message) {
            AceSurveyUtility.hideLoadingPage();
            alert(message);
        });
}

SurveyReponsePage.prototype.renderSuccessPage = function () {
    this.panel_survey_holder.setHtml("<span class='ace-sv-label lb-thank-you-title'>" + ACE_SURVEY_STRINGS.submit_success_label + "</span><span class='ace-sv-label lb-thank-you-message'>" + ACE_SURVEY_STRINGS.submit_success_message + "</span>");
}

SurveyReponsePage.prototype.onPageRenderred = function (callback) {
    this.listener.onPageRenderred = callback;
    return this;
}