

var ACE_SURVEY_CONSTANT = {
    QUESTION_TYPE: {
        NUMER: 1,
        SINGLE_TEXT: 2,
        MULTI_TEXT: 3,
        DATE_TIME: 4,
        SINGLE_CHOICE: 6,
        DROPDOWN: -6, // NOT USE SHAREPOINT KIND TYPE
        YES_NO: 8,
        MULTI_CHOICE: 15,
        CURRENCY: 10,
        MATRIX: 16,
        MATRIX_TEXT: -16,
        MATRIX_CHOICE_TEXT: -26,
        MATRIX_CHOICE_MULTIPLE_TEXT: -36,
        MATRIX_MULTIPLE_CHOICE: -37,
        USER: 20,
        USER_AND_GROUP: -20, // NOT USE SHAREPOINT KIND TYPE
        PAGE_SEPARATOR: 26,

    },
    PATTERN: {
        SPLIT_IMAGE: '$##_##!',
        SPLIT_IMAGE_NAME: "__##_IMAGE_NAME_##__",
        SPLIT_MATRIX_SECOND_RATING_VALUE: "|#|",
        SPLIT_MATRIX_NA_AND_SECOND_VALUE: "|##|",
        SPLIT_MATRIX_TEXT_OPTIONS: "##_MATRIX_TEXT_##",
        SPLIT_MATRIX_CHOICE_TEXT_OPTIONS: "##_MATRIX_CHOICE_TEXT_##",
        SPLIT_MATRIX_CHOICE_MULTILE_TEXT_OPTIONS: "##_MATRIX_MULTI_CHOICE_TEXT_##",
        SPLIT_MATRIX_MULTIPLE_CHOICE_OPTIONS: "##_MATRIX_MULTIPLE_CHOICE_##",
        SPLIT_JUMP_TO_CONFIG: "##_JUMP_TO_CONFIG_##",
    },
    QUESTION_RENDER_MODE: {
        EDIT: 0,
        RESPONSE: 1,
        GRAPH: 2
    },
    CURRENCY_FORMAT: [
        {
            "text": "؋123,456.00 (Afghanistan)",
            "value": 1164
        },
        {
            "text": "123,456.00 Lek (Albania)",
            "value": 1052
        },
        {
            "text": "د.ج.‏ 123,456.00 (Algeria)",
            "value": 5121
        },
        {
            "text": "$ 123,456.00 (Argentina)",
            "value": 11274
        },
        {
            "text": "123,456.00 ֏ (Armenia)",
            "value": 1067
        },
        {
            "text": "$123,456.00 (Australia)",
            "value": 3081
        },
        {
            "text": "123,456.00 € (Austria)",
            "value": 3079
        },
        {
            "text": "123,456.00 ₼ (Azerbaijan)",
            "value": 1068
        },
        {
            "text": "123,456.00 ₼ (Azerbaijan)",
            "value": 2092
        },
        {
            "text": "د.ب.‏ 123,456.00 (Bahrain)",
            "value": 15361
        },
        {
            "text": "৳ 123,456.00 (Bangladesh)",
            "value": 2117
        },
        {
            "text": "123,456.00 ₽ (Bashkir)",
            "value": 1133
        },
        {
            "text": "123,456.00 Br (Belarus)",
            "value": 1059
        },
        {
            "text": "€ 123,456.00 (Belgium)",
            "value": 2067
        },
        {
            "text": "BZ$123,456.00 (Belize)",
            "value": 10249
        },
        {
            "text": "Bs. 123,456.00 (Bolivia)",
            "value": 16394
        },
        {
            "text": "123,456.00 КМ (Bosnia and Herzegovina)",
            "value": 8218
        },
        {
            "text": "123,456.00 KM (Bosnia and Herzegovina)",
            "value": 5146
        },
        {
            "text": "R$ 123,456.00 (Brazil)",
            "value": 1046
        },
        {
            "text": "$123,456.00 (Brunei Darussalam)",
            "value": 2110
        },
        {
            "text": "123,456.00 лв. (Bulgaria)",
            "value": 1026
        },
        {
            "text": "123,456.00៛ (Cambodia)",
            "value": 1107
        },
        {
            "text": "123,456.00 $ (Canada)",
            "value": 3084
        },
        {
            "text": "$123,456.00 (Canada)",
            "value": 4105
        },
        {
            "text": "$ 123,456.00 (Chile)",
            "value": 13322
        },
        {
            "text": "$123,456.00 (Columbia)",
            "value": 9226
        },
        {
            "text": "₡123,456.00 (Costa Rica)",
            "value": 5130
        },
        {
            "text": "123,456.00 kn (Croatia)",
            "value": 1050
        },
        {
            "text": "123,456.00 Kč (Czech Republic)",
            "value": 1029
        },
        {
            "text": "kr. 123,456.00 (Denmark)",
            "value": 1030
        },
        {
            "text": "RD$123,456.00 (Dominican Republic)",
            "value": 7178
        },
        {
            "text": "$ 123,456.00 (Ecuador)",
            "value": 12298
        },
        {
            "text": "ج.م.‏ 123,456.00 (Egypt)",
            "value": 3073
        },
        {
            "text": "$123,456.00 (El Salvador)",
            "value": 17418
        },
        {
            "text": "123,456.00 € (Estonia)",
            "value": 1061
        },
        {
            "text": "ETB123,456.00 (Ethiopia)",
            "value": 1118
        },
        {
            "text": "kr. 123,456.00 (Faroe Islands)",
            "value": 1080
        },
        {
            "text": "123,456.00 € (Finland)",
            "value": 1035
        },
        {
            "text": "123,456.00 € (France)",
            "value": 1036
        },
        {
            "text": "123,456.00 ₾ (Georgia)",
            "value": 1079
        },
        {
            "text": "123,456.00 € (Germany)",
            "value": 1031
        },
        {
            "text": "123,456.00 € (Greece)",
            "value": 1032
        },
        {
            "text": "kr. 123,456.00 (Greenland)",
            "value": 1135
        },
        {
            "text": "Q123,456.00 (Guatemala)",
            "value": 4106
        },
        {
            "text": "L. 123,456.00 (Honduras)",
            "value": 18442
        },
        {
            "text": "HK$123,456.00 (Hong Kong S.A.R.)",
            "value": 3076
        },
        {
            "text": "123,456.00 Ft (Hungary)",
            "value": 1038
        },
        {
            "text": "123,456.00 kr. (Iceland)",
            "value": 1039
        },
        {
            "text": "₹ 123,456.00 (India)",
            "value": 1081
        },
        {
            "text": "Rp123,456.00 (Indonesia)",
            "value": 1057
        },
        {
            "text": "ريال123,456.00 (Iran)",
            "value": 1065
        },
        {
            "text": "د.ع.‏ 123,456.00 (Iraq)",
            "value": 2049
        },
        {
            "text": "€123,456.00 (Ireland)",
            "value": 6153
        },
        {
            "text": "€ 123,456.00 (Italy)",
            "value": 1040
        },
        {
            "text": "₪ 123,456.00 (Israel)",
            "value": 1037
        },
        {
            "text": "J$123,456.00 (Jamaica)",
            "value": 8201
        },
        {
            "text": "¥123,456.00 (Japan)",
            "value": 1041
        },
        {
            "text": "د.ا.‏ 123,456.00 (Jordan)",
            "value": 11265
        },
        {
            "text": "₸123,456.00 (Kazakhstan)",
            "value": 1087
        },
        {
            "text": "KSh123,456.00 (Kenya)",
            "value": 1089
        },
        {
            "text": "₩123,456.00 (Korea)",
            "value": 1042
        },
        {
            "text": "د.ك.‏ 123,456.00 (Kuwait)",
            "value": 13313
        },
        {
            "text": "123,456.00 сом (Kyrgyzstan)",
            "value": 1088
        },
        {
            "text": "123,456.00 ₭ (Lao P.D.R)",
            "value": 1108
        },
        {
            "text": "€ 123,456.00 (Latvia)",
            "value": 1062
        },
        {
            "text": "ل.ل.‏‏ 123,456.00 (Lebanon)",
            "value": 12289
        },
        {
            "text": "د.ل.‏‏123,456.00 (Libya)",
            "value": 4097
        },
        {
            "text": "CHF 123,456.00 (Liechtenstein)",
            "value": 5127
        },
        {
            "text": "123,456.00 € (Lithuania)",
            "value": 1063
        },
        {
            "text": "123,456.00 € (Luxembourg)",
            "value": 5132
        },
        {
            "text": "MOP123,456.00 (Macao S.A.R.)",
            "value": 5124
        },
        {
            "text": "123,456.00 ден. (Macedonia (FYROM))",
            "value": 1071
        },
        {
            "text": "RM123,456.00 (Malaysia)",
            "value": 1086
        },
        {
            "text": "123,456.00 ރ. (Maldives)",
            "value": 1125
        },
        {
            "text": "€123,456.00 (Malta)",
            "value": 1082
        },
        {
            "text": "$123,456.00 (Mexico)",
            "value": 2058
        },
        {
            "text": "123,456.00 € (Monaco)",
            "value": 6156
        },
        {
            "text": "123,456.00 ₮ (Mongolia)",
            "value": 1104
        },
        {
            "text": "د.م.‏‏ 123,456.00 (Morocco)",
            "value": 6145
        },
        {
            "text": "रु123,456.00 (Nepal)",
            "value": 1121
        },
        {
            "text": "€ 123,456.00 (Netherlands)",
            "value": 1043
        },
        {
            "text": "$123,456.00 (New Zealand)",
            "value": 5129
        },
        {
            "text": "C$123,456.00 (Nicaragua)",
            "value": 19466
        },
        {
            "text": "₦ 123,456.00 (Nigeria)",
            "value": 1128
        },
        {
            "text": "kr 123,456.00 (Norway)",
            "value": 1044
        },
        {
            "text": "ر.ع.‏‏ 123,456.00 (Oman)",
            "value": 8193
        },
        {
            "text": "Rs123,456.00 (Pakistan)",
            "value": 1056
        },
        {
            "text": "B/. 123,456.00 (Panama)",
            "value": 6154
        },
        {
            "text": "₲ 123,456.00 (Paraguay)",
            "value": 15370
        },
        {
            "text": "¥123,456.00 (People's Republic of China)",
            "value": 2052
        },
        {
            "text": "S/ 123,456.00 (Peru)",
            "value": 10250
        },
        {
            "text": "₱123,456.00 (Philippines)",
            "value": 13321
        },
        {
            "text": "123,456.00 zł (Poland)",
            "value": 1045
        },
        {
            "text": "123,456.00 € (Portugal)",
            "value": 2070
        },
        {
            "text": "$123,456.00 (Puerto Rico)",
            "value": 20490
        },
        {
            "text": "ر.ق.‏‏ 123,456.00 (Qatar)",
            "value": 16385
        },
        {
            "text": "123,456.00 lei (Romania)",
            "value": 1048
        },
        {
            "text": "123,456.00 ₽ (Russia)",
            "value": 1049
        },
        {
            "text": "123,456.00 RWF (Rwanda)",
            "value": 1159
        },
        {
            "text": "ر.س.‏ 123,456.00 (Saudi Arabia)",
            "value": 1025
        },
        {
            "text": "123,456.00 CFA (Senegal)",
            "value": 1160
        },
        {
            "text": "123,456.00 din. (Serbia, Latin)",
            "value": 9242
        },
        {
            "text": "123,456.00 € (Montenegro)",
            "value": 12314
        },
        {
            "text": "123,456.00 дин. (Serbia, Cyrillic)",
            "value": 10266
        },
        {
            "text": "$123,456.00 (Singapore)",
            "value": 4100
        },
        {
            "text": "123,456.00 EUR (Slovakia)",
            "value": 1051
        },
        {
            "text": "123,456.00 € (Slovenia)",
            "value": 1060
        },
        {
            "text": "R 123,456.00 (South Africa)",
            "value": 7177
        },
        {
            "text": "123,456.00 € (Spain)",
            "value": 3082
        },
        {
            "text": "රු. 123,456.00 (Sri Lanka)",
            "value": 1115
        },
        {
            "text": "123,456.00 kr (Sweden)",
            "value": 1053
        },
        {
            "text": "Fr. 123,456.00 (Switzerland)",
            "value": 2055
        },
        {
            "text": "ل.س.‏‏ 123,456.00 (Syria)",
            "value": 10241
        },
        {
            "text": "NT$123,456.00 (Taiwan)",
            "value": 1028
        },
        {
            "text": "123,456.00 смн (Tajikistan)",
            "value": 1064
        },
        {
            "text": "฿123,456.00 (Thailand)",
            "value": 1054
        },
        {
            "text": "TT$123,456.00 (Trinidad and Tobago)",
            "value": 11273
        },
        {
            "text": "د.ت.‏‏ 123,456.00 (Tunisia)",
            "value": 7169
        },
        {
            "text": "123,456.00 ₺ (Turkey)",
            "value": 1055
        },
        {
            "text": "123,456.00m. (Turkmenistan)",
            "value": 1090
        },
        {
            "text": "123,456.00₴ (Ukraine)",
            "value": 1058
        },
        {
            "text": "د.إ.‏ 123,456.00 (United Arab Emirates)",
            "value": 14337
        },
        {
            "text": "£123,456.00 (United Kingdom)",
            "value": 2057
        },
        {
            "text": "$123,456.00 (United States)",
            "value": 1033
        },
        {
            "text": "$U 123,456.00 (Uruguay)",
            "value": 14346
        },
        {
            "text": "123,456.00 so'm (Uzbekistan)",
            "value": 1091
        },
        {
            "text": "123,456.00 сўм (Uzbekistan)",
            "value": 2115
        },
        {
            "text": "Bs.S 123,456.00 (Venezuela)",
            "value": 8202
        },
        {
            "text": "123,456.00 ₫ (Vietnam)",
            "value": 1066
        },
        {
            "text": "ر.ي.‏‏ 123,456.00 (Yemen)",
            "value": 9217
        },
        {
            "text": "$123,456.00 (Zimbabwe)",
            "value": 12297
        }
    ]
}