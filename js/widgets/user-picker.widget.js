

var __ace_count_people_picker__ = 0;

var AceSurveyUserPicker = function (sp_schema) {
    if (sp_schema) {
        this.sp_schema = sp_schema;
    } else {
        this.sp_schema = {};
        this.sp_schema['PrincipalAccountType'] = 'User,SecGroup,SPGroup';
        this.sp_schema['SearchPrincipalSource'] = 15;
        this.sp_schema['ResolvePrincipalSource'] = 15;
        this.sp_schema['AllowMultipleValues'] = true;
        this.sp_schema['MaximumEntitySuggestions'] = 50;
        this.sp_schema['Width'] = '100%';
    }
    this._user_info = [];
    this.listener = {
        onValueChanged: null
    }
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyUserPicker.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyUserPicker.prototype.constructor = AceSurveyUserPicker;

AceSurveyUserPicker.prototype.template = function () {
    ['sp.core.js', 'sp.runtime.js', 'sp.js', 'autofill.js', 'clientpeoplepicker.js', 'clientforms.js', 'clienttemplates.js'].forEach(function (val, idx) {
        if (!_v_dictSod[val]) RegisterSod(val, "\u002f_layouts\u002f15\u002f" + val);
    });

    var self = this;
    __ace_count_people_picker__++;

    var holder = new AceSurveyPanel();
    holder.addClass('ace-sv-user-picker-holder');
    var holder_id = 'ace_people_holder_' + new Date().getTime() + '_' + __ace_count_people_picker__;
    holder.setElementId(holder_id);
    holder.showLoading();
    setTimeout(function () {
        SP.SOD.loadMultiple(['sp.core.js', 'sp.runtime.js', 'sp.js', 'autofill.js', 'clientpeoplepicker.js', 'clientforms.js', 'clienttemplates.js'], function () {
            SPClientPeoplePicker_InitStandaloneControlWrapper(holder_id, null, self.sp_schema);
            var picker = SPClientPeoplePicker.SPClientPeoplePickerDict[holder_id + "_TopSpan"];
            var _waiter = null;
            picker.OnValueChangedClientScript = function (elementId, userInfo) {
                self._user_info = userInfo;
                console.log(userInfo);
                clearTimeout(_waiter);
                if (self.listener.onValueChanged) {
                    _waiter = setTimeout(function () {
                        self.listener.onValueChanged.call(self, userInfo);
                    }, 600);
                }
            };
            holder.hideLoading();

            self.__addUser = function(key){
                picker.AddUserKeys(key);
            }
        });
    }, 50);
    return holder;
}

AceSurveyUserPicker.prototype.setOnValueChanged = function (callback) {
    this.listener.onValueChanged = callback;
}

AceSurveyUserPicker.prototype.getUserInfos = function () {
    return this._user_info;
}

AceSurveyUserPicker.prototype.addUser = function (key) {
   this.__addUser(key);
}