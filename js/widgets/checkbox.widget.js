


var AceSurveyCheckbox = function () {
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyCheckbox.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyCheckbox.prototype.constructor = AceSurveyCheckbox;

AceSurveyCheckbox.prototype.template = function () {
    return "<input type='checkbox' class='ace-sv-radio' />";
}

AceSurveyCheckbox.prototype.isChecked = function () {
    return this.element.checked;
}

AceSurveyCheckbox.prototype.setIsChecked = function (flag) {
    return this.element.checked = flag;
}