


var AceSurveyDropdown = function (options) {
    this.options = options || [];
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyDropdown.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyDropdown.prototype.constructor = AceSurveyDropdown;

AceSurveyDropdown.prototype.template = function () {
    var html = "<select class='ace-sv-dropdown'>";
    for (var i = 0; i < this.options.length; i++) {
        var item = this.options[i];
        html += "<option value='" + item.value + "'>" + item.text + "</option>"
    }
    html += "</select>";
    return html;
}

AceSurveyDropdown.prototype.setOptions = function (options) {
    this.options = options;
    var option_html = "";
    for (var i = 0; i < this.options.length; i++) {
        var item = this.options[i];
        option_html += "<option value='" + item.value + "'>" + item.text + "</option>";
    }
    this.setHtml(option_html);
}

AceSurveyDropdown.prototype.getSelectedValue = function () {
    return this.options[this.element.selectedIndex].value;
}



AceSurveyDropdown.prototype.getSelectedIndex = function () {
    return this.element.selectedIndex;
}

AceSurveyDropdown.prototype.setSelectedValue = function (value) {
    for (var i = 0; i < this.options.length; i++) {
        if (this.options[i].value === value) {
            this.element.selectedIndex = i;
            break;
        }
    }

}