


var AceSurveyLoading = function () {
    var self = this;
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyLoading.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyLoading.prototype.constructor = AceSurveyLoading;

AceSurveyLoading.prototype.template = function () {
    return "<div class='ace-anim-loading'><div class='dot dot1'></div><div class='dot dot2'></div><div class='dot dot3'></div><div class='dot dot4'></div></div>";
}

