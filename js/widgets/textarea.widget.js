


var AceSurveyTextArea = function () {
    var self = this;
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyTextArea.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyTextArea.prototype.constructor = AceSurveyTextArea;

AceSurveyTextArea.prototype.template = function () {
    return "<textarea class='ace-sv-textarea'></textarea>";
}

AceSurveyTextArea.prototype.getValue = function(){
    return this.element.value.trim();
}

AceSurveyTextArea.prototype.setValue = function (value) {
    this.element.value = value;
}

AceSurveyTextArea.prototype.addOnTextChanged = function (callback) {
    this.addEventListener('keyup', function (e) {
        callback(e);
    });

    this.addEventListener('blur', function (e) {
        callback(e);
    });

}