

var AceSurveyPanel = function () {
    var self = this;
    BaseAceSurveyWidget.call(this);

}

AceSurveyPanel.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyPanel.prototype.constructor = AceSurveyPanel;

AceSurveyPanel.prototype.template = function () {
    return "<div class='ace-sv-panel'></div>";
}
