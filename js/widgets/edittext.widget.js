


var AceSurveyEdittext = function () {
    var self = this;
    BaseAceSurveyWidget.call(this);
}

AceSurveyEdittext.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyEdittext.prototype.constructor = AceSurveyEdittext;

AceSurveyEdittext.prototype.template = function () {
    return "<input type='text' class='ace-sv-edittext' />";
}

AceSurveyEdittext.prototype.getValue = function () {
    return this.element.value.trim();
}

AceSurveyEdittext.prototype.setValue = function (value) {
    this.element.value = value;
}

AceSurveyEdittext.prototype.addOnTextChanged = function (callback) {
    var text = '';
    var self = this;
    this.addEventListener('keyup', function (e) {
        if (text === self.getValue()) {
            return;
        }
        text = self.getValue();
        callback(e);
    });

    this.addEventListener('paste', function (e) {
        if (text === self.getValue()) {
            return;
        }
        text = self.getValue();
        callback(e);
    });

    this.addEventListener('blur', function (e) {
        if (text === self.getValue()) {
            return;
        }
        text = self.getValue();
        callback(e);
    });
}