


var TEMPLATE_NAME = function () {
    var self = this;
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
TEMPLATE_NAME.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
TEMPLATE_NAME.prototype.constructor = TEMPLATE_NAME;

TEMPLATE_NAME.prototype.template = function () {
    return "";
}

