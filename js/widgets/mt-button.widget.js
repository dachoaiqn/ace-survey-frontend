


var AceSurveyMaterialButton = function (mt_icon_name, label) {
    var self = this;
    this.icon_name = mt_icon_name;
    this.label = label;
    BaseAceSurveyWidget.call(this);
}

AceSurveyMaterialButton.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyMaterialButton.prototype.constructor = AceSurveyMaterialButton;

AceSurveyMaterialButton.prototype.template = function () {
    if (this.label && this.label.length > 0) {
        return "<button class='ace-sv-mt-button'><i class='material-icons'>" + this.icon_name + "</i><label>" + this.label + "</label></button>"
    } else {
        return "<button class='ace-sv-mt-button'><i class='material-icons'>" + this.icon_name + "</i></button>"
    }
}

AceSurveyMaterialButton.prototype.setHtml = function () {
    throw this.constructor.name + " - Cannot use this function, use setIcon() instead"
}

AceSurveyMaterialButton.prototype.setIcon = function (icon_name) {
    this.icon_name = icon_name;
    this.element.firstChild.innerHTML = icon_name;
}