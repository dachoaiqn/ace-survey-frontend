


var AceSurveyRadio = function () {
    BaseAceSurveyWidget.call(this);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyRadio.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyRadio.prototype.constructor = AceSurveyRadio;

AceSurveyRadio.prototype.template = function () {
    return "<input type='radio' class='ace-sv-radio' />";
}

