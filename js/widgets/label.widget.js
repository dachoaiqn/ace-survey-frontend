
var AceSurveyLabel = function (text) {
    var self = this;
    this.label_text = text || '';
    BaseAceSurveyWidget.call(this);
}

AceSurveyLabel.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyLabel.prototype.constructor = AceSurveyLabel;

AceSurveyLabel.prototype.template = function () {
    return "<span class='acv-sv-label'>" + this.label_text + "</span>";
}

AceSurveyLabel.prototype.setText = function (text) {
    this.label_text = text;
    this.element.innerHTML = text;
}
