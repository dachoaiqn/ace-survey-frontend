
// Reference https://www.chartjs.org/

var ACE_SV_CHART_UNIT_VIEW = [
    {
        text: "Đơn vị: mặc định",
        value: 'default'
    },
    {
        text: "Đơn vị: %",
        value: 'percent'
    }
]

var ACE_SV_CHART_TYPE_PICKER = [
    {
        text: 'Biểu đồ cột',
        value: 'bar'
    },
    {
        text: 'Biểu đồ cột ngang',
        value: 'horizontalBar'
    },
    {
        text: 'Biểu đồ tròn',
        value: 'pie'
    },
    {
        text: 'Biểu đồ vòng',
        value: 'doughnut'
    },
    {
        text: 'Biểu đồ đường',
        value: 'line'
    },
    {
        text: 'Biểu đồ vùng',
        value: 'polarArea'
    },
    // {
    //     text: 'Biểu đồ bong bóng',
    //     value: 'bubble'
    // },
    {
        text: 'Biểu đồ Radar',
        value: 'radar'
    }
]
// Chart.defaults.global.animation = 600;
// Chart.pluginService.register({
//     beforeRender: function (chart) {
//         if (chart.config.options.showAllTooltips) {
//             // create an array of tooltips
//             // we can't use the chart tooltip because there is only one tooltip per chart
//             chart.pluginTooltips = [];
//             chart.config.data.datasets.forEach(function (dataset, i) {
//                 chart.getDatasetMeta(i).data.forEach(function (sector, j) {
//                     chart.pluginTooltips.push(new Chart.Tooltip({
//                         _chart: chart.chart,
//                         _chartInstance: chart,
//                         _data: chart.data,
//                         _options: chart.options.tooltips,
//                         _active: [sector]
//                     }, chart));
//                 });
//             });

//             // turn off normal tooltips
//             chart.options.tooltips.enabled = false;
//         }
//     },
//     afterDraw: function (chart, easing) {
//         if (chart.config.options.showAllTooltips) {
//             // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
//             if (!chart.allTooltipsOnce) {
//                 if (easing !== 1)
//                     return;
//                 chart.allTooltipsOnce = true;
//             }

//             // turn on tooltips
//             chart.options.tooltips.enabled = true;
//             Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
//                 tooltip.initialize();
//                 tooltip.update();
//                 // we don't actually need this since we are not animating tooltips
//                 tooltip.pivot();
//                 tooltip.transition(easing).draw();
//             });
//             chart.options.tooltips.enabled = false;
//         }
//     }
// });

var AceSurveyChart = function (chart_options, chart_type_option) {
    var self = this;
    this.chart_type_option = chart_type_option || ACE_SV_CHART_TYPE_PICKER;
    console.log(chart_options);
    // chart_options.options.responsive = false;
    this.chart_options = JSON.parse(JSON.stringify(chart_options));
    this.show_value = false;
    BaseAceSurveyWidget.call(this);

    this.unit_type = 'default'

    // this.chart = new Chart(self.ctx, chart_options);
    this.setChartType(self.chart_options.type);
}

// Inherit all BaseAceSurveyWidget method 
AceSurveyChart.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyChart.prototype.constructor = AceSurveyChart;

AceSurveyChart.prototype.template = function () {
    var self = this;
    var wrapper = new AceSurveyPanel();
    wrapper.addClass('ace-sv-chart-holder');
    var canvas = document.createElement("canvas");
    canvas.className = 'ace-sv-chart';
    canvas.height = 80;

    var dropdown_chart_type = new AceSurveyDropdown(this.chart_type_option);
    var dropdown_unit_view = new AceSurveyDropdown(ACE_SV_CHART_UNIT_VIEW);
    var btn_show_value = new AceSurveyButton('<i class="material-icons">check_box_outline_blank</i> ' + ACE_SURVEY_STRINGS.show_chart_value)

    dropdown_chart_type.addClass('dropdown-chart-type');
    dropdown_unit_view.addClass('dropdown-unit-type');
    btn_show_value.addClass('btn-show-value');

    dropdown_chart_type.setSelectedValue(self.chart_options.type);
    this.ctx = canvas.getContext('2d');
    wrapper.appendChild(dropdown_chart_type);
    wrapper.appendChild(dropdown_unit_view);
    // wrapper.appendChild(btn_show_value);
    wrapper.appendChild(canvas);

    dropdown_chart_type.addEventListener('change', function () {
        self.setChartType(dropdown_chart_type.getSelectedValue());
    });

    dropdown_unit_view.addEventListener('change', function () {
        self.setUnitType(dropdown_unit_view.getSelectedValue());
    });



    btn_show_value.addEventListener('click', function () {
        self.show_value = !self.show_value;
        if (self.show_value) {
            btn_show_value.setText('<i class="material-icons">check_box</i> ' + ACE_SURVEY_STRINGS.show_chart_value);
        } else {
            btn_show_value.setText('<i class="material-icons">check_box_outline_blank</i> ' + ACE_SURVEY_STRINGS.show_chart_value);
        }
        self.reload();
        return false;
    })

    return wrapper;
}

AceSurveyChart.prototype.setUnitPickerHidden = function (flag) {
    var elems = this.findChildElement('.dropdown-unit-type');
    if (elems.length > 0 && flag) {
        $(elems[0]).css("display", 'none');
    } else {
        $(elems[0]).css("display", 'inline-block');
    }
}

AceSurveyChart.prototype.setUnitType = function (type) {
    this.unit_type = type;
    this.reload();
}

AceSurveyChart.prototype.reload = function () {
    if (this.chart) {
        this.chart.clear();
        this.chart.destroy();

    }
    var clone_options = JSON.parse(JSON.stringify(this.chart_options));
    switch (this.unit_type) {
        case 'percent':
            var datasets = clone_options.data.datasets;
            datasets.forEach(function (dataset_record) {
                var total = dataset_record.data.reduce(function (out, item) {
                    return out + item;
                });
                for (var i = 0; i < dataset_record.data.length; i++) {
                    dataset_record.data[i] = parseFloat((dataset_record.data[i] * 100 / total).toFixed(1));
                }
            })

            break;
    }
    switch (this.chart_options.type) {
        case 'pie':
        case 'doughnut':
            clone_options.options.legend.display = true;
            break;
        default:
            clone_options.options.legend.display = false;
            if (clone_options.options.scales == undefined) {
                clone_options.options.scales = {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true.options,
                            stepSize: this.unit_type == 'percent' ? 10 : 2,
                            min: 0
                        }
                    }]
                }
            }
            break;
    }
    // clone_options.options.showAllTooltips = this.show_value;
    // if (clone_options.options.showAllTooltips == true) {
    //     if (clone_options.options.tooltips == undefined) {
    //         clone_options.options.tooltips = {};
    //     }
    //     clone_options.options.tooltips.mode = 'dataset';
    // }
    this.chart = new Chart(this.ctx, clone_options);
}

AceSurveyChart.prototype.setChartType = function (type) {

    this.chart_options.type = type;
    this.reload();

}

AceSurveyChart.prototype.setChartData = function () {

}