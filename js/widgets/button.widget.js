


var AceSurveyButton = function (text) {
    var self = this;
    this.button_text = text || '';
    BaseAceSurveyWidget.call(this);
}

AceSurveyButton.prototype = Object.create(BaseAceSurveyWidget.prototype);
AceSurveyButton.prototype.constructor = AceSurveyButton;

AceSurveyButton.prototype.template = function () {
    return "<button class='acv-sv-button'>" + this.button_text || "" + "</button>";
}

AceSurveyButton.prototype.setText = function (text) {
    this.button_text = text;
    this.element.innerHTML = text;
}