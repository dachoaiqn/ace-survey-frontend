


var BaseAceSurveyWidget = function () {
    this.element = null;
    this.has_destroy = false;
    this._is_showing = true;
    this.render();

}

BaseAceSurveyWidget.prototype.render = function () {
    var html_template = this.template();
    if (typeof html_template === 'string' && html_template.length > 0) {
        var temp = document.createElement('div');
        temp.innerHTML = html_template;
        this.element = temp.firstChild;
        // Parse to elementent by HTML string that get from template() method
        // this.element = new DOMParser().parseFromString(html_template, 'text/xml').firstElementChild;
    } else if (typeof html_template === 'object' && html_template.element != undefined) {
        this.element = html_template.element;
    } else if (typeof html_template === 'object') {
        this.element = html_template;
    }
}

BaseAceSurveyWidget.prototype.reload = function () {
    this.render();
}

BaseAceSurveyWidget.prototype.appendTo = function (elem) {
    if (this.element != null) {
        elem.appendChild(this.element);
    } else {
        throw "seem the template() method return empty";
    }
}

BaseAceSurveyWidget.prototype.toElement = function () {
    return this.elem;
}

// Html string, HTML DOM
BaseAceSurveyWidget.prototype.appendChild = function (child) {
    if (child == null) {
        throw this.constructor.name + "Children is null";
    }
    if (typeof child === 'string') {
        this.element.insertAdjacentHTML('beforeend', child);
    } else if (typeof this.element === 'object' && child.element != undefined) {
        this.element.appendChild(child.element);
    } else if (typeof this.element === 'object') {
        this.element.appendChild(child);
    }
    this.onRendered();
}

BaseAceSurveyWidget.prototype.addAnimationClass = function (anim_class_name) {
    var self = this;
    this.addClass(anim_class_name);
    setTimeout(function () {
        self.removeClass(anim_class_name);
    }, 500);
}

BaseAceSurveyWidget.prototype.destroy = function (animation) {
    this.has_destroy = true;
    if (this.element && this.element.parentElement) {
        var self = this;
        if (animation != undefined && animation.length > 0) {
            self.addClass(animation);
            setTimeout(function () {
                try {
                    self.element.parentElement.removeChild(self.element);

                } catch (err) {
                    console.warn(err);
                }
            }, 400);
        } else {
            self.element.parentElement.removeChild(self.element);
        }
    }
}

BaseAceSurveyWidget.prototype.setElementId = function (id) {
    if (document.getElementById('id') != undefined) {
        throw "The" + id + " has been used. Please use another";
    }
    this.element.id = id;
}

BaseAceSurveyWidget.prototype.disable = function () {
    this.setAttribute('disabled', 'disabled');
}


BaseAceSurveyWidget.prototype.enable = function () {
    this.removeAttribute('disabled');
}

BaseAceSurveyWidget.prototype.addClass = function (className) {
    $(this.element).addClass(className);
}

BaseAceSurveyWidget.prototype.removeClass = function (className) {
    $(this.element).removeClass(className);
}

BaseAceSurveyWidget.prototype.hasClass = function (className) {
    return $(this.element).hasClass(className);
}

BaseAceSurveyWidget.prototype.setError = function (error) {
    alert(error);
}

// Return the HTML template
BaseAceSurveyWidget.prototype.template = function () {
    throw this.constructor.name + " The template() is empty";
}

BaseAceSurveyWidget.prototype.addEventListener = function (event, callback) {
    // this.element.addEventListener(event, callback.bind(this));
    $(this.element).on(event, callback.bind(this))
}

BaseAceSurveyWidget.prototype.removeEventListener = function (event, callback) {
    // this.element.removeEventListener(event, callback);
    $(this.element).off(event, callback);
}

BaseAceSurveyWidget.prototype.setHtml = function (html) {
    this.element.innerHTML = html;
}

BaseAceSurveyWidget.prototype.clearHtml = function () {
    this.element.innerHTML = '';
}

BaseAceSurveyWidget.prototype.getHtml = function () {
    return this.element.innerHTML;
}

BaseAceSurveyWidget.prototype.addCss = function (css) {
    $(this.element).css(css);
}

BaseAceSurveyWidget.prototype.setAttribute = function (key, value) {
    this.element.setAttribute(key, value);
}


BaseAceSurveyWidget.prototype.removeAttribute = function (key) {
    this.element.removeAttribute(key);
}

BaseAceSurveyWidget.prototype.onRendered = function () {

}

BaseAceSurveyWidget.prototype.findChildElement = function (selector) {
    return this.element.querySelectorAll(selector);
}

BaseAceSurveyWidget.prototype.show = function () {
    this._is_showing = true;
    $(this.element).show();
    return this;
}

BaseAceSurveyWidget.prototype.hide = function () {
    this._is_showing = false;
    $(this.element).hide();
    return this;
}

BaseAceSurveyWidget.prototype.isShowing = function () {
    return this._is_showing || this.element.style.display !== 'none';
}

BaseAceSurveyWidget.prototype.focus = function () {
    this.element.focus();
}

BaseAceSurveyWidget.prototype.blur = function () {
    this.element.blur();
}

BaseAceSurveyWidget.prototype.showLoading = function () {
    if (this.__pn_loading == undefined) {
        this.__pn_loading = new AceSurveyLoading();
        this.appendChild(this.__pn_loading);
    }

    // this.addClass("show-loading");
}

BaseAceSurveyWidget.prototype.hideLoading = function () {
    // this.removeClass("show-loading");
    if (this.__pn_loading != undefined) {
        this.__pn_loading.destroy();
        this.__pn_loading = null;
    }

}