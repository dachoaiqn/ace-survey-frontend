


var AceSurveyImage = function (url) {
    this.image_url = url;
    BaseAceSurveyWidget.call(this);

}

// Inherit all BaseAceSurveyWidget method 
AceSurveyImage.prototype = Object.create(BaseAceSurveyWidget.prototype);
// Override constructor not use Parent
AceSurveyImage.prototype.constructor = AceSurveyImage;

AceSurveyImage.prototype.template = function () {
    return "<img class='ace-sv-image' src='" + this.image_url + "'/>";
}

AceSurveyImage.prototype.setImageUrl = function (url) {
    this.image_url = url;
    this.element.src = url;
}