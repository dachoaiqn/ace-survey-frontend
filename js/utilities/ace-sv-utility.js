

var AceSurveyUtility = {
    __loading: null,
    __currency_formatter: null,
    __number_formatter: null,
    showLoadingPage: function () {
        if (this.__loading == null) {
            this.__loading = document.createElement('div');
            this.__loading.className = 'sv-page-loading show-loading';
            document.body.appendChild(this.__loading);
        }
    },
    hideLoadingPage: function () {
        if (this.__loading != null) {
            document.body.removeChild(this.__loading);
            this.__loading = null;
        }

    },
    getUrlParameter: function (key) {
        if (typeof key !== 'string') {
            return null;
        }
        var url = window.location.href;
        if (url.lastIndexOf("#") == (url.length - 1)) {
            url = url.substr(0, url.length - 1);
        }
        params = url.split('?');
        if (params.length < 2) {
            return null;
        }
        key = key.toLowerCase();
        params = params[1];
        params = params.split('&');
        for (var i = 0; i < params.length; i++) {
            var item = params[i], pair_key_value = item.split("=");
            if (pair_key_value && pair_key_value.length >= 2 && pair_key_value[0].toLowerCase() == key) {
                return pair_key_value[1];
            }
        }
        return null;
    },
    Array: function (array_source) {
        return {
            filter: function (fn_condition) {
                if (array_source.filter != undefined) {
                    return array_source.filter(fn_condition);
                }
                var results = [], item;
                for (var i = 0; i < array_source.length; i++) {
                    if (fn_condition(item = array_source[i]) == true) {
                        results.push(item);
                    }
                }
                return results;
            },
            forEach: function (callback) {
                if (array_source == null) {
                    return;
                }
                if (array_source.forEach != undefined) {
                    array_source.forEach(callback);
                    return;
                }
                var max = array_source.length
                for (var i = 0; i < max; i++) {
                    callback.call(array_source, array_source[i], i);
                }
            },
            reduce: function (callback /*, initialValue*/) {
                'use strict';
                if (array_source == null) {
                    throw new TypeError('Array.prototype.reduce called on null or undefined');
                }
                if (typeof callback !== 'function') {
                    throw new TypeError(callback + ' is not a function');
                }
                var t = Object(array_source), len = t.length >>> 0, k = 0, value;
                if (arguments.length == 2) {
                    value = arguments[1];
                } else {
                    while (k < len && !(k in t)) {
                        k++;
                    }
                    if (k >= len) {
                        throw new TypeError('Reduce of empty array with no initial value');
                    }
                    value = t[k++];
                }
                for (; k < len; k++) {
                    if (k in t) {
                        value = callback(value, t[k], k, t);
                    }
                }
                return value;
            },
            find: function (fn_condition) {
                if (array_source.find != undefined) {
                    return array_source.find(fn_condition);
                }
                var item;
                for (var i = 0; i < array_source.length; i++) {
                    if (fn_condition.call(null, item = array_source[i]) == true) {
                        return item;
                    }
                }
                return null;
            },
            findIndex: function (fn_condition) {
                if (array_source.findIndex != undefined) {
                    return array_source.findIndex(fn_condition);
                }
                for (var i = 0; i < array_source.length; i++) {
                    if (fn_condition.call(null, item = array_source[i]) == true) {
                        return i;
                    }
                }
                return -1;
            }
        }
    },
    Object: function (obj) {
        return {
            toArray: function () {
                var rs = [];
                for (var k in obj) {
                    rs.push(obj[k]);
                }
                return rs;
            }
        }
    },
    Xml2JSON: function (xml) {
        // Create the return object
        var obj = {};

        if (xml.nodeType == 1) {
            // element
            // do attributes
            if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) {
            // text
            obj = xml.nodeValue;
        }

        // do children
        // If all text nodes inside, get concatenated text from them.
        var textNodes = AceSurveyUtility.Array(xml.childNodes).filter(function (node) {
            return node.nodeType === 3;
        });
        if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
            obj = AceSurveyUtility.Array(xml.childNodes).reduce(function (text, node) {
                return text + node.nodeValue;
            }, "");
        } else if (xml.hasChildNodes()) {
            for (var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof obj[nodeName] == "undefined") {
                    obj[nodeName] = AceSurveyUtility.Xml2JSON(item);
                } else {
                    if (typeof obj[nodeName].push == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(AceSurveyUtility.Xml2JSON(item));
                }
            }
        }
        return obj;
    },
    convertChoiceAnswerStringToObject: function (str_answer) {
        var rs = {
            text: '',
            image_url: '',
            image_name: ''
        }
        if (str_answer.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE) > -1) {
            // If the answer exist an image inside answer 
            var temp = str_answer.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE);
            var image_split = temp[1].split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
            rs.text = temp[0];
            rs.image_name = image_split[1];
            rs.image_url = image_split[0];
        } else {
            rs.text = str_answer;
        }
        return rs;
    },
    convertChoiceAnswerObjectToString: function (object) {
        // Format: {text: 'string' image_url: 'string', image_name: 'string'};
        if (object == null) {
            return null;
        }
        if (object && object.image_url.length > 0) {
            return object.text + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE + object.image_url + ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME + object.image_name;
        } else {
            return object.text;
        }
    },
    convertUserAnswerToHtml: function (question_model, user_answer, default_text) {
        // format: {user: { user_id: 'int', full_name: 'string' }, answer: 'string|array - default of Sharepoint RestAPI /items' }
        if (user_answer == null) {
            return default_text || ACE_SURVEY_STRINGS.empty_answer;
        }

        var html = '';
        switch (question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
                var ans = user_answer;
                // If the answer is existing image name then append image name to answer to display
                if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                    var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                    var image_name = split_answer[1];
                    var max = question_model.answer_values.length;
                    for (var j = 0; j < max; j++) {
                        var choice_option = question_model.answer_values[j];
                        if (image_name == choice_option.image_name) {
                            html += "- " + split_answer[0] + '<img src="' + choice_option.image_url + '"/><br/>';
                            break;
                        }
                    }
                } else {
                    html += "- " + ans;
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                for (var i = 0; i < user_answer.length; i++) {
                    var ans = user_answer[i];
                    if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1];
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                html += "- " + split_answer[0] + '<img src="' + choice_option.image_url + '"/><br/>';
                                break;
                            }
                        }
                    } else {
                        html += "- " + ans;
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
                var html = '';
                for (var i = 0; i < user_answer.length; i++) {
                    var item = user_answer[i]; // Format: { Question: string, Answer: number - rating number}
                    if (item.Question.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = item.Question.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1];
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                if (item.Answer < question_model.rating_answer.end_number) {
                                    var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                                    html += "- " + split_answer[0] + '<img src="' + choice_option.image_url + '"/>: ' + str_anwser + '<br/>';
                                } else {
                                    html += "- " + split_answer[0] + '<img src="' + choice_option.image_url + '"/>: ' + question_model.rating_answer.na_text + '<br/>';
                                }
                                break;
                            }
                        }
                    } else {
                        if (item.Answer <= question_model.rating_answer.end_number) {
                            var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                            html += "- " + item.Question + ": " + str_anwser + "<br/>";
                        } else {
                            html += "- " + item.Question + ": " + question_model.rating_answer.na_text + "<br/>";
                        }
                        // html += "- " + ans;
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        html += '<b>' + user_answer[i].Question + ':</b>';
                        html += '<br/>' + user_answer[i].Answer;
                        html += '<br/><br/>';
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        html += '<b>' + user_answer[i].Question + ':</b>';
                        html += '<br/>' + AceSurveyUtility.Array(user_answer[i].SecondAnswer).filter(function (item) {
                            return item.length > 0;
                        }).join(", ");
                        html += '<br/><br/>';
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        html += '<b>' + user_answer[i].Question + ': </b>';
                        // html += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // html += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        html += '<br/><i>' + user_answer[i].SecondAnswer.reduce(function (out, item) {
                            return out + item.RatingText + ": " + item.Answer + '<br/>';
                        }, '') + "</i>";
                        html += '<br/><br/>';
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        html += '<b>' + user_answer[i].Question + ': </b>';
                        // html += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // html += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        html += '<i>' + user_answer[i].SecondAnswer.reduce(function (out, item) {
                            out += item.reduce(function (out2, item2) {
                                out2 += "<br/> - <b>" + item2.SubQuestion + "</b>: " + item2.SubAnswer;
                                return out2
                            }, '');
                            return out;
                        }, "") + "</i>";
                        html += '<br/><br/>';
                    }
                }
                return html;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
                return user_answer ? ACE_SURVEY_STRINGS.label_yes_answer : ACE_SURVEY_STRINGS.label_no_answer;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
                return user_answer.Title;
            default:
                return user_answer;
        }
    },
    convertUserAnswerPlainText: function (question_model, user_answer, default_text) {
        // format: {user: { user_id: 'int', full_name: 'string' }, answer: 'string|array - default of Sharepoint RestAPI /items' }
        if (user_answer == null) {
            return default_text || ACE_SURVEY_STRINGS.empty_answer;
        }

        var text = '';
        switch (question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
                var ans = user_answer;
                if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                    var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                    var image_name = split_answer[1];
                    var max = question_model.answer_values.length;
                    for (var j = 0; j < max; j++) {
                        var choice_option = question_model.answer_values[j];
                        if (image_name == choice_option.image_name) {
                            text += split_answer[0] + '(' + choice_option.image_name + ')\r\n';
                            break;
                        }
                    }
                } else {
                    text += ans;
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                user_answer = user_answer.results;
                for (var i = 0; i < user_answer.length; i++) {
                    var ans = user_answer[i];
                    if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1].replace(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME, '');
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                text += split_answer[0] + '(' + choice_option.image_name + ')\r\n';
                                break;
                            }
                        }
                    } else {
                        text += ans;
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
                user_answer = user_answer.results;
                for (var i = 0; i < user_answer.length; i++) {
                    var item = user_answer[i]; // Format: { Question: string, Answer: number - rating number}
                    if (item.Question.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = item.Question.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1];
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                if (item.Answer < question_model.rating_answer.end_number) {
                                    // detect if exist the 2nd rating value 
                                    var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                                    text += split_answer[0] + "(" + choice_option.image_name + '): ' + str_anwser + '\r\n';
                                } else {
                                    text += split_answer[0] + "(" + choice_option.image_name + '): ' + question_model.rating_answer.na_text + '\r\n';
                                }
                                break;
                            }
                        }
                    } else {
                        if (item.Answer <= question_model.rating_answer.end_number) {
                            var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                            text += item.Question + ": " + str_anwser + "\r\n";
                        } else {
                            text += item.Question + ": " + question_model.rating_answer.na_text + "\r\n";
                        }
                        // text += "- " + ans;
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
                var temp = [];
                if (user_answer != null && user_answer.length > 0) {
                    var obj_user_answer = JSON.parse(user_answer);
                    for (var i = 0; i < obj_user_answer.length; i++) {
                        temp.push(obj_user_answer[i].Question + ':\r\n' + obj_user_answer[i].Answer);
                    }
                    text = temp.join('\r\n--------\r\n');
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question + ": ";
                        // text += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // text += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        text += "\r\n" + user_answer[i].SecondAnswer.filter(function (item) {
                            return item.length > 0;
                        }).join(', ');
                        text += '\r\n--------\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question + ": ";
                        // text += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // text += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        text += "\r\n" + user_answer[i].SecondAnswer.reduce(function (out, item) {
                            return out + item.RatingText + ": " + item.Answer + "\r\n";
                        }, '');
                        text += '\r\n--------\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question;
                        text += user_answer[i].SecondAnswer.reduce(function (out, item) {
                            out += item.reduce(function (out2, item2) {
                                out2 += "\r\n - " + item2.SubQuestion + ": " + item2.SubAnswer;
                                return out2
                            }, '');
                            return out;
                        }, "");
                        text += '\r\n\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
                return user_answer ? ACE_SURVEY_STRINGS.label_yes_answer : ACE_SURVEY_STRINGS.label_no_answer;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
                return user_answer.Title;
            default:
                return user_answer;
        }
    },
    convertUserNoMatrixAnswerPlainText: function (question_model, user_answer, default_text) {
        // format: {user: { user_id: 'int', full_name: 'string' }, answer: 'string|array - default of Sharepoint RestAPI /items' }
        if (user_answer == null) {
            return default_text || ACE_SURVEY_STRINGS.empty_answer;
        }

        var text = '';
        switch (question_model.question_type) {
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.SINGLE_CHOICE:
                var ans = user_answer;
                if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                    var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                    var image_name = split_answer[1];
                    var max = question_model.answer_values.length;
                    for (var j = 0; j < max; j++) {
                        var choice_option = question_model.answer_values[j];
                        if (image_name == choice_option.image_name) {
                            text += split_answer[0] + '(' + choice_option.image_name + ')\r\n';
                            break;
                        }
                    }
                } else {
                    text += ans;
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MULTI_CHOICE:
                user_answer = user_answer.results;
                for (var i = 0; i < user_answer.length; i++) {
                    var ans = user_answer[i];
                    if (ans.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = ans.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1].replace(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME, '');
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                text += split_answer[0] + '(' + choice_option.image_name + ')\r\n';
                                break;
                            }
                        }
                    } else {
                        text += ans;
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX:
                user_answer = user_answer.results;
                for (var i = 0; i < user_answer.length; i++) {
                    var item = user_answer[i]; // Format: { Question: string, Answer: number - rating number}
                    if (item.Question.indexOf(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME) > -1) {
                        var split_answer = item.Question.split(ACE_SURVEY_CONSTANT.PATTERN.SPLIT_IMAGE_NAME);
                        var image_name = split_answer[1];
                        var max = question_model.answer_values.length;
                        for (var j = 0; j < max; j++) {
                            var choice_option = question_model.answer_values[j];
                            if (image_name == choice_option.image_name) {
                                if (item.Answer < question_model.rating_answer.end_number) {
                                    // detect if exist the 2nd rating value 
                                    var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                                    text += split_answer[0] + "(" + choice_option.image_name + '): ' + str_anwser + '\r\n';
                                } else {
                                    text += split_answer[0] + "(" + choice_option.image_name + '): ' + question_model.rating_answer.na_text + '\r\n';
                                }
                                break;
                            }
                        }
                    } else {
                        if (item.Answer <= question_model.rating_answer.end_number) {
                            var str_anwser = question_model.rating_answer.second_labels[item.Answer - 1] || item.Answer;
                            text += item.Question + ": " + str_anwser + "\r\n";
                        } else {
                            text += item.Question + ": " + question_model.rating_answer.na_text + "\r\n";
                        }
                        // text += "- " + ans;
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_TEXT:
                var temp = [];
                if (user_answer != null && user_answer.length > 0) {
                    var obj_user_answer = JSON.parse(user_answer);
                    for (var i = 0; i < obj_user_answer.length; i++) {
                        temp.push(obj_user_answer[i].Question + ':\r\n' + obj_user_answer[i].Answer);
                    }
                    text = temp.join('\r\n--------\r\n');
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question + ": ";
                        // text += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // text += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        text += "\r\n" + user_answer[i].SecondAnswer.filter(function (item) {
                            return item.length > 0;
                        }).join(', ');
                        text += '\r\n--------\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_TEXT:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question + ": ";
                        // text += question_model.rating_answer.second_labels[user_answer[i].Answer - 1] || user_answer[i].Answer;
                        // text += '<b>' + user_answer[i].SecondQuestion + ':</b>';
                        console.log("user_answer[i].SecondAnswer", user_answer[i].SecondAnswer);
                        text += "\r\n" + user_answer[i].SecondAnswer.reduce(function (out, item) {
                            return out + item.RatingText + ": " + item.Answer + "\r\n";
                        }, '');
                        text += '\r\n--------\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_CHOICE_MULTIPLE_TEXT:
                user_answer = JSON.parse(user_answer).results || [];
                if (user_answer != null && user_answer.length > 0) {
                    for (var i = 0; i < user_answer.length; i++) {
                        text += user_answer[i].Question;
                        text += user_answer[i].SecondAnswer.reduce(function (out, item) {
                            out += item.reduce(function (out2, item2) {
                                out2 += "\r\n - " + item2.SubQuestion + ": " + item2.SubAnswer;
                                return out2
                            }, '');
                            return out;
                        }, "");
                        text += '\r\n\r\n';
                    }
                }
                return text;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.YES_NO:
                return user_answer ? ACE_SURVEY_STRINGS.label_yes_answer : ACE_SURVEY_STRINGS.label_no_answer;
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER:
            case ACE_SURVEY_CONSTANT.QUESTION_TYPE.USER_AND_GROUP:
                return user_answer.Title;
            default:
                return user_answer;
        }
    },
    convertMatrixQuestionsToHtmlTable: function (question_list, poll_results, default_text) {

        var matrix_question = AceSurveyUtility.Array(question_list).filter(function (item) {
            return item.question_type === ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE
                || item.question_type === ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX
        });

        var filter_poll_matrix = AceSurveyUtility.Array(matrix_question).reduce(function (out, question) {
            if (out[question.internal_name] == undefined) {
                out[question.internal_name] = [];
            }
            var polls = poll_results.map(function (poll) {
                return poll[question.internal_name];
            }).filter(function (item) {
                return item != null;
            });
            if (polls.length > 0) {
                out[question.internal_name] = out[question.internal_name].concat(polls);
            }
            return out;
        }, {});

        console.log(matrix_question);
        console.log(filter_poll_matrix);
        var tables = [];
        matrix_question.forEach(function (question) {
            var table = document.createElement("table");

            var tr_question = document.createElement('tr');
            var td_question = document.createElement('td');
            td_question.innerText = question.title + "?";
            td_question.colSpan = 3;
            tr_question.appendChild(td_question);

            var tr_header = document.createElement("tr");

            var td_header_1 = document.createElement("th");
            td_header_1.innerText = 'Nhãn hiệu';

            var td_header_2 = document.createElement("th");
            td_header_2.innerText = 'Yếu tố phù hợp';

            var td_header_3 = document.createElement("th");
            td_header_3.innerText = 'Số mẫu';

            tr_header.appendChild(td_header_1);
            tr_header.appendChild(td_header_2);
            tr_header.appendChild(td_header_3);

            table.appendChild(tr_question);
            table.appendChild(tr_header);

            question.answer_values.forEach(function (sub_question) {
                for (var k = 0; k < question.rating_answer.end_number; k++) {
                    var tr_rating = document.createElement('tr');
                    if (k == 0) {
                        var td_sub_question = document.createElement("td");
                        td_sub_question.innerText = sub_question.text;
                        td_sub_question.rowSpan = question.rating_answer.end_number;
                        tr_rating.appendChild(td_sub_question);
                    }
                    var rating_value = question.rating_answer.second_labels[k];

                    var td_rating_value = document.createElement('td');
                    td_rating_value.innerText = rating_value;

                    var td_count_answer = document.createElement('td');

                    var answer_by_this_quest = [];
                    if (question.question_type === ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX) {
                        answer_by_this_quest = question.poll_result.filter(function (item) {
                            if (item.Question != sub_question.text) {
                                return false;
                            }
                            if (question.rating_answer.second_labels[item.Answer - 1] == rating_value) {
                                return true;
                            }
                            return false;
                        });
                    } else if (question.question_type === ACE_SURVEY_CONSTANT.QUESTION_TYPE.MATRIX_MULTIPLE_CHOICE) {
                        answer_by_this_quest = question.poll_result.filter(function (item) {
                            if (item.Question != sub_question.text) {
                                return false;
                            }
                            return item.SecondAnswer.indexOf(rating_value) > -1;
                        });
                    }
                    td_count_answer.innerText = answer_by_this_quest.length > 0 ? answer_by_this_quest.length : "";
                    tr_rating.appendChild(td_rating_value);
                    tr_rating.appendChild(td_count_answer);

                    table.appendChild(tr_rating);
                }
            });

            tables.push(table);
            console.log(table);
        });

        console.log("matrix", matrix_question);

        return tables;


    },
    numberToCommaString: function (number) {
        if (AceSurveyUtility.__number_formatter == null) {
            AceSurveyUtility.__number_formatter = new Intl.NumberFormat();
        }
        return AceSurveyUtility.__number_formatter.format(number);
    },
    numberToCurrency: function (number) {
        if (AceSurveyUtility.__currency_formatter == null) {
            AceSurveyUtility.__currency_formatter = new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
            });
        }
        return AceSurveyUtility.__currency_formatter.format(number);
    },
    stringToDateTime: function (string, format) {

        return moment(string).format(format);
    },
    s2ab: function (s) {
        var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
        var view = new Uint8Array(buf);  //create uint8array as viewer
        for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
        return buf;
    },
    isEmptyString: function (string) {
        return (typeof string === 'string' && string.length > 0) == false;
    },
    imageFile2Base64: function (file, compression) {
        var max_width = 360, max_height = 360;
        var _cb;
        var result = {
            onLoaded: function (callback) {
                _cb = callback;
            }
        }
        var reader = new FileReader();
        reader.onload = function (e) {
            var base64 = e.target.result.toString();
            // Init Canvas to compress image
            var wrapper = document.createElement("div");
            var canvas = document.createElement("canvas");
            $(wrapper).css({
                'overflow': 'hidden',
                'left': '0px',
                'top:': '0px',
                'width': '0px',
                'height': '0px',
                'position': 'fixed',
                'visibility': 'hidden'
            })
            $(canvas).css({
                'position': 'absolute',
                'top': '0px',
                'left': '0px'
            });

            wrapper.appendChild(canvas);
            document.body.appendChild(wrapper);

            var image = new Image();
            image.onload = function () {


                var scale_x = 1, scale_y = 1;
                var ctx = canvas.getContext('2d');
                if ((max_height > -1 && max_height < image.naturalHeight)
                    || (max_width > -1 && max_width < image.naturalWidth)) {
                    var scale_w = max_width / image.naturalWidth,
                        scale_h = max_height / image.naturalHeight,
                        scale = scale_w * image.naturalHeight > max_width ? scale_w : scale_h;
                    scale_x = scale, scale_y = scale;

                    var real_width = scale * image.naturalWidth, real_height = scale * image.naturalHeight;
                    canvas.width = real_width;
                    canvas.height = real_height;
                    ctx.scale(scale, scale);
                } else {
                    canvas.width = image.naturalWidth;
                    canvas.height = image.naturalHeight;
                }
                ctx.drawImage(image, 0, 0);

                var base64 = canvas.toDataURL('image/jpeg', compression || 0.5);
                console.log(new Blob([base64]).size / 1024 + ' kb');
                if (typeof _cb === 'function') {
                    _cb.call(null, base64);
                }
                document.body.removeChild(wrapper);
            }
            image.src = base64;
        }

        reader.readAsDataURL(file);
        return result;
    }
}