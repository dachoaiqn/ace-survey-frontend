
var ACE_SV_GLOBAL_VALUE = {
    survey_list_title: 'Dong gop y kien quy trinh'
}
window.addEventListener("load", function () {
    // Query url parameter
    ACE_SV_GLOBAL_VALUE.survey_list_title = AceSurveyUtility.getUrlParameter('listid');

    // Response Page
    new SurveyReponsePage(ACE_SV_GLOBAL_VALUE.survey_list_title)
        .renderToElement(document.getElementById("ace-survey-holder"));

    // Graph Page
    // new SurveyGraphPage(ACE_SV_GLOBAL_VALUE.survey_list_title)
    //     .renderToElement(document.getElementById("ace-survey-holder"));

    // Editor Page
    // new SurveyEditorPage(ACE_SV_GLOBAL_VALUE.survey_list_title)
    // .renderToElement(document.getElementById("ace-survey-holder"));
});