﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using AceSurvey.Layouts.AceSurvey.Models;
using System.Web.Script.Serialization;
using Microsoft.SharePoint.Utilities;

namespace AceSurvey.Layouts.AceSurvey
{
    public partial class SurveyList : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var type = Request.QueryString["type"];
            if (type.ToLower().Equals("create"))
            {
                CreateNewSurvey();
            }
            else if (type.ToLower().Equals("info"))
            {
                GetSurveyListInfo();
            }
            else if (type.ToLower().Equals("update"))
            {
                UpdateSurvey();
            }

        }


        private void GetSurveyListInfo()
        {
            string listID = HttpUtility.UrlDecode(Request.QueryString["listid"]);
            string siteUrl = HttpUtility.UrlDecode(Request.QueryString["siteurl"]);
            try
            {
                if (null != listID)
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate ()
                    {
                        using (SPSite spSite = new SPSite(siteUrl))
                        {
                            using (SPWeb spWeb = spSite.OpenWeb())
                            {
                                Guid guid = new Guid(listID);
                                SPList lstSurvey = spWeb.Lists.GetList(guid, true);
                                if (lstSurvey != null)
                                {

                                    SurveyInfo surveyInfo = new SurveyInfo();
                                    surveyInfo.ID = lstSurvey.ID.ToString();
                                    surveyInfo.Title = lstSurvey.Title;
                                    surveyInfo.Description = lstSurvey.Description;
                                    surveyInfo.AllowMultiResponses = lstSurvey.AllowMultiResponses;
                                    surveyInfo.ShowUser = lstSurvey.ShowUser;
                                    surveyInfo.Admin = new List<SurveyPermission>();
                                    surveyInfo.Contribute = new List<SurveyPermission>();
                                    surveyInfo.Reader = new List<SurveyPermission>();


                                    if (lstSurvey.HasUniqueRoleAssignments == true)
                                    {
                                        SPRoleDefinition adminRole = spWeb.RoleDefinitions.GetByType(SPRoleType.Administrator);
                                        SPRoleDefinition contributeRole = spWeb.RoleDefinitions.GetByType(SPRoleType.Contributor);
                                        SPRoleDefinition readerRole = spWeb.RoleDefinitions.GetByType(SPRoleType.Reader);

                                        var assignmentAdmins = lstSurvey.RoleAssignments.OfType<SPRoleAssignment>().Where(item => item.RoleDefinitionBindings.OfType<SPRoleDefinition>().FirstOrDefault(def => def.BasePermissions == adminRole.BasePermissions) != null);
                                        foreach (SPRoleAssignment assign in assignmentAdmins)
                                        {
                                            SurveyPermission p = new SurveyPermission();
                                            p.ID = assign.Member.ID;
                                            p.LoginName = assign.Member.LoginName;
                                            p.Title = assign.Member.Name;
                                            surveyInfo.Admin.Add(p);
                                        }

                                        var assignmentContributes = lstSurvey.RoleAssignments.OfType<SPRoleAssignment>().Where(item => item.RoleDefinitionBindings.OfType<SPRoleDefinition>().FirstOrDefault(def => def.BasePermissions == contributeRole.BasePermissions) != null);
                                        foreach (SPRoleAssignment assign in assignmentContributes)
                                        {
                                            SurveyPermission p = new SurveyPermission();
                                            p.ID = assign.Member.ID;
                                            p.LoginName = assign.Member.LoginName;
                                            p.Title = assign.Member.Name;
                                            surveyInfo.Contribute.Add(p);
                                        }

                                        var assignmentReaders = lstSurvey.RoleAssignments.OfType<SPRoleAssignment>().Where(item => item.RoleDefinitionBindings.OfType<SPRoleDefinition>().FirstOrDefault(def => def.BasePermissions == readerRole.BasePermissions) != null);
                                        foreach (SPRoleAssignment assign in assignmentReaders)
                                        {
                                            SurveyPermission p = new SurveyPermission();
                                            p.ID = assign.Member.ID;
                                            p.LoginName = assign.Member.LoginName;
                                            p.Title = assign.Member.Name;
                                            surveyInfo.Reader.Add(p);
                                        }
                                    }


                                    JavaScriptSerializer js = new JavaScriptSerializer();

                                    ResponseSuccess(js.Serialize(surveyInfo));
                                }
                                else
                                {
                                    ResponseFailed("List does not exist");
                                }
                            }
                        }
                    });
                }
                else
                {
                    ResponseFailed("List does not exist");
                }
            }
            catch (Exception ex)
            {
                ResponseFailed(ex.Message);
            }


        }

        private void CreateNewSurvey()
        {
            string siteUrl = HttpUtility.UrlDecode(Request.QueryString["siteurl"]);
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string allowMultipleResponse = Request.Form["AllowMultipleResponses"];
            string ShowUser = Request.Form["ShowUserRepsonse"];
            string u_read = Request.Form["Read"];
            string u_contribute = Request.Form["Contribute"];
            string u_admin = Request.Form["Admin"];

            string[] user_reads = null;
            string[] user_contributes = null;
            string[] user_admins = null;

            if (u_read != null && u_read.Length > 1)
            {
                user_reads = u_read.Split(';');
            }
            if (u_contribute != null && u_contribute.Length > 1)
            {
                user_contributes = u_contribute.Split(';');
            }
            if (u_admin != null && u_admin.Length > 1)
            {
                user_admins = u_admin.Split(';');
            }


         
            SPList newList= null;
            using (SPSite spSite = new SPSite(siteUrl))
            {
                using (SPWeb spWeb = spSite.OpenWeb())
                {
                    try
                    {

                        SPList lstSurvey = spWeb.Lists.TryGetList(title);
                        if (null == lstSurvey)
                        {
                            spWeb.AllowUnsafeUpdates = true;

                            SPListTemplateCollection lstTemp = spWeb.ListTemplates;
                            SPListTemplate template = lstTemp.OfType<SPListTemplate>().FirstOrDefault(i => i.Type == SPListTemplateType.Survey);
                            spWeb.Lists.Add(title, description, template);
                            newList = spWeb.Lists[title];
                            spWeb.AllowUnsafeUpdates = false;
                          

                        }
                        else
                        {
                            ResponseFailed("List has existed");
                        }

                    }
                    catch (Exception ex)
                    {
                        ResponseFailed(ex.Message);

                    }
                }
            }
            if(newList != null)
            {
                SetPermissionToSurvey(siteUrl, newList.ID, user_reads, user_contributes, user_admins);
                ResponseSuccess("{ \"ListId\": \"" + newList.ID + "\" }");
            }

        }

        private void SetPermissionToSurvey(string siteUrl,Guid guid, string[] user_reads, string[] user_contributes, string[] user_admins)
        {
            if (null != user_admins || null != user_reads || null != user_contributes)
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                using (SPSite spSite = new SPSite(siteUrl))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                     
                            spWeb.AllowUnsafeUpdates = true;
                            SPList lstSurvey = spWeb.Lists.GetList(guid, true);

                          

                            lstSurvey.BreakRoleInheritance(false);

                            // Add Creator as Admin by default

                            SPUser admin = spWeb.EnsureUser(lstSurvey.Author.LoginName);
                            SPRoleAssignment role_assign_admin = new SPRoleAssignment(admin);
                            var admin_role = spWeb.RoleDefinitions.GetByType(SPRoleType.Administrator);
                            role_assign_admin.RoleDefinitionBindings.Add(admin_role);
                            lstSurvey.RoleAssignments.Add(role_assign_admin);

                            if (user_admins != null)
                            {
                                AssignRoleToSurvey(spWeb, lstSurvey, user_admins, SPRoleType.Administrator);
                            }

                            if (user_contributes != null)
                            {
                                AssignRoleToSurvey(spWeb, lstSurvey, user_contributes, SPRoleType.Contributor);
                            }

                            if (user_reads != null)
                            {
                                AssignRoleToSurvey(spWeb, lstSurvey, user_reads, SPRoleType.Reader);
                            }
                            lstSurvey.Update();
                            spWeb.AllowUnsafeUpdates = false;
                        
                }
            }
                });

            }
        }


        private void UpdateSurvey()
        {
            string siteUrl = HttpUtility.UrlDecode(Request.QueryString["siteurl"]);
            string listId = Request.Form["ListId"];
            string title = Request.Form["Title"];
            string description = Request.Form["Description"];
            string allowMultipleResponse = Request.Form["AllowMultipleResponses"];
            string ShowUser = Request.Form["ShowUserRepsonse"];
            string u_read = Request.Form["Read"];
            string u_contribute = Request.Form["Contribute"];
            string u_admin = Request.Form["Admin"];

            string[] user_reads = null;
            string[] user_contributes = null;
            string[] user_admins = null;

            if (u_read != null && u_read.Length > 1)
            {
                user_reads = u_read.Split(';');
            }
            if (u_contribute != null && u_contribute.Length > 1)
            {
                user_contributes = u_contribute.Split(';');
            }
            if (u_admin != null && u_admin.Length > 1)
            {
                user_admins = u_admin.Split(';');
            }
            
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
            using (SPSite spSite = new SPSite(siteUrl))
            {
                using (SPWeb spWeb = spSite.OpenWeb())
                {
                    try
                    {
                        Guid guid = new Guid(listId);
                        SPList lstSurvey = spWeb.Lists.GetList(guid, true);
                        //SPRoleDefinition adminRole = spWeb.RoleDefinitions.GetByType(SPRoleType.Administrator);
                        if (null == lstSurvey)
                        {
                            ResponseFailed("List does not exist");
                        }
                        else
                        {
                      
                            spWeb.AllowUnsafeUpdates = true;
                            lstSurvey.Title = title;
                            lstSurvey.Description = description;
                            lstSurvey.AllowMultiResponses = allowMultipleResponse == "1" ? true : false;
                            lstSurvey.ShowUser = ShowUser == "1" ? true : false;

                          

                            if (null != user_admins || null != user_reads || null != user_contributes)
                            {
                           
                                    if (!lstSurvey.HasUniqueRoleAssignments)
                                    {
                                        lstSurvey.BreakRoleInheritance(false);
                                    }

                                    SPRoleAssignmentCollection roleAssignments = lstSurvey.RoleAssignments;

                                    for (int i = 0; i < roleAssignments.Count; i++)
                                    {
                                        roleAssignments[i].RoleDefinitionBindings.RemoveAll();
                                        roleAssignments[i].Update();
                                    }

                                    SPUser admin = spWeb.EnsureUser(lstSurvey.Author.LoginName);
                                    SPRoleAssignment role_assign_admin = new SPRoleAssignment(admin);
                                    var admin_role = spWeb.RoleDefinitions.GetByType(SPRoleType.Administrator);
                                    role_assign_admin.RoleDefinitionBindings.Add(admin_role);
                                    lstSurvey.RoleAssignments.Add(role_assign_admin);

                                    if (user_admins != null)
                                    {
                                        AssignRoleToSurvey(spWeb, lstSurvey, user_admins, SPRoleType.Administrator);
                                    }

                                    if (user_contributes != null)
                                    {
                                        AssignRoleToSurvey(spWeb, lstSurvey, user_contributes, SPRoleType.Contributor);
                                    }

                                    if (user_reads != null)
                                    {
                                        AssignRoleToSurvey(spWeb, lstSurvey, user_reads, SPRoleType.Reader);
                                    }
                                
                            }
                            else
                            {
                                    if (lstSurvey.HasUniqueRoleAssignments)
                                    {
                                        lstSurvey.ResetRoleInheritance();
                                    }
                              
                            }

                            lstSurvey.Update();
                            spWeb.AllowUnsafeUpdates = false;
                            ResponseSuccess("{ \"ListId\": \"" + lstSurvey.ID + "\" }");
                            return;
                        }


                    }
                    catch (Exception ex)
                    {
                        ResponseFailed(ex.Message);
                    }
                }
            }
            });


        }

        private void AssignRoleToSurvey(SPWeb spWeb, SPList spList, string[] user_or_group, SPRoleType roleType)
        {
            foreach (string login_name in user_or_group)
            {
                if (login_name.Length == 0)
                {
                    continue;
                }
                SPUser member = spWeb.SiteUsers.GetByLoginNoThrow(login_name);
                //      SPRoleAssignment roleAssignment = spList.RoleAssignments.OfType<SPRoleAssignment>().FirstOrDefault(item => item.)
                if (null != member)
                {
                    SPRoleAssignment roleassignment_group = new SPRoleAssignment(member);
                    var role_defineition = spWeb.RoleDefinitions.GetByType(roleType);
                    roleassignment_group.RoleDefinitionBindings.Add(role_defineition);
                    spList.RoleAssignments.Add(roleassignment_group);
                }
                else
                {
                    SPGroup mgroup = spWeb.SiteGroups.Cast<SPGroup>().Where(g => g.Name == login_name).FirstOrDefault();

                    if (null != mgroup)
                    {
                        SPRoleAssignment roleassignment_group = new SPRoleAssignment(mgroup);
                        var role_defineition = spWeb.RoleDefinitions.GetByType(roleType);
                        roleassignment_group.RoleDefinitionBindings.Add(role_defineition);
                        spList.RoleAssignments.Add(roleassignment_group);
                    }
                }
            }
        }

        private void ResponseSuccess(string message)
        {
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Clear();
            //  HttpContext.Current.Response.ContentType = "plain/text";
            HttpContext.Current.Response.ContentType = "application/json";
            HttpContext.Current.Response.Write("{ \"Status\": \"success\", \"Data\":" + message + "}");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.Close();
            HttpContext.Current.Response.End();
        }


        private void ResponseFailed(string message)
        {
            try
            {
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write("{ \"Status\": \"error\", \"Data\": { \"Message\": \"" + message + "\" } }");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Close();
                HttpContext.Current.Response.End();
            }
            catch (Exception ex) { };
        }


    }
}
